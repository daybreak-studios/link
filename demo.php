<?php
	namespace DaybreakStudios\Link\Demos;

	use DaybreakStudios\Link\Connection\Adapter\MySqlPdoAdapter;
	use DaybreakStudios\Link\Connection\Connection;
	use DaybreakStudios\Link\Entity\Entity;

	require __DIR__ . '/vendor/autoload.php';

	$conn = Connection::create(new MySqlPdoAdapter('link_demo', '127.0.0.1', 'root', ''));

	/**
	 * @method int getId()
	 * @method string getBirthplace()
	 * @method $this setBirthplace(string $birthplace)
	 * @method int getAge()
	 * @method $this setAge(int $age)
	 */
	class Biography extends Entity {}

	/**
	 * @method int getId()
	 * @method string getContent()
	 * @method $this setContent(string $content)
	 * @method \DateTime getPostedOn()
	 * @method $this setPostedOn(\DateTime $postedOn)
	 * @method bool isDeleted()
	 * @method $this setDeleted(bool $deleted)
	 * @method string getTitle()
	 * @method $this setTitle(string $title)
	 * @method User getUser()
	 * @method string getUserEmail()
	 */
	class Post extends Entity {
		public function __construct(User $user, string $title, string $content) {
			parent::__construct([
				'user' => $user,
				'userEmail' => $user->getEmail(),
				'postedOn' => new \DateTime(),
				'title' => $title,
				'content' => $content,
			]);
		}

		public function setUser(User $user = null) {
			parent::set('user', $user);
			parent::set('userEmail', $user->getEmail());

			return $this;
		}

		protected static function __init() {
			parent::hasOne(User::class, 'user');
		}
	}

	/**
	 * @method string getFirstName()
	 * @method $this setFirstName(string $firstName)
	 * @method string getLastName()
	 * @method $this setLastName(string $lastName)
	 * @method string getEmail()
	 * @method $this setEmail(string $email)
	 * @method \DateTime getCreatedOn()
	 * @method $this setCreatedOn(\DateTime $createdOn)
	 * @method bool isDeleted()
	 * @method $this setDeleted(bool $deleted)
	 * @method Post[] getPosts()
	 * @method $this setPosts(array $posts)
	 * @method Biography getBiography()
	 * @method $this setBiography(Biography $bio)
	 */
	class User extends Entity {
		protected static function __init() {
			parent::ownsMany(Post::class, 'posts', [
				'deleted' => false,
			]);

			parent::ownsOne(Biography::class, 'biography');
		}
	}

	$user = User::find(1);
	$post = new Post($user, 'Test Post (Again)', 'Please ignore!');

	$post->save();

	// protected static function hasOne(string $klass, array $conditions, string $column, string $target)
	// protected static function hasMany(string $klass, array $conditions, string $target)
	// protected static function ownsOne(string $klass, array $conditions, string $column, string $target)
	// protected static function ownsMany(string $klass, array $conditions, string $target)