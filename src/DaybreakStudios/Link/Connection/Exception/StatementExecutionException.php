<?php
	namespace DaybreakStudios\Link\Connection\Exception;

	class StatementExecutionException extends \Exception {}