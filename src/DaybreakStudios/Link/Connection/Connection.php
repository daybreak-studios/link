<?php
	namespace DaybreakStudios\Link\Connection;

	use DaybreakStudios\Link\Connection\Adapter\AdapterInterface;

	class Connection {
		private static $connections = [];

		private $adapter;

		/**
		 * Connection constructor.
		 *
		 * @param AdapterInterface $adapter
		 */
		protected function __construct(AdapterInterface $adapter) {
			$this->adapter = $adapter;
		}

		/**
		 * @return AdapterInterface
		 */
		public function getAdapter() {
			return $this->adapter;
		}

		/**
		 * @param string $sql
		 *
		 * @return array
		 */
		public function fetch($sql) {
			return $this->getAdapter()->fetch($sql);
		}

		/**
		 * @param string $sql
		 *
		 * @return int the number of rows altered, or in the case of an insert, the ID of the last inserted row
		 */
		public function alter($sql) {
			return $this->getAdapter()->alter($sql);
		}

		/**
		 * @return string
		 */
		public function getSchema() {
			return $this->getAdapter()->getSchema();
		}

		/**
		 * @return bool
		 */
		public static function hasConnections() {
			return sizeof(self::$connections) !== 0;
		}

		/**
		 * @param string|null $name
		 *
		 * @return Connection|null
		 */
		public static function getConnection($name = null) {
			if (!self::hasConnections() || !isset(self::$connections[$name]))
				return null;
			else if ($name === null)
				return current(self::$connections);

			return self::$connections[$name];
		}

		/**
		 * @param AdapterInterface $adapter
		 * @param string|null      $name
		 *
		 * @return Connection
		 */
		public static function create(AdapterInterface $adapter, $name = null) {
			$conn = new Connection($adapter);

			self::$connections[$name] = $conn;

			return $conn;
		}
	}