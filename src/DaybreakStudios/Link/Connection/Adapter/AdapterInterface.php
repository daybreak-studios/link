<?php
	namespace DaybreakStudios\Link\Connection\Adapter;

	use DaybreakStudios\Link\AST\Visitor\ReduceVisitor;

	interface AdapterInterface {
		/**
		 * @return string
		 */
		public function getName();

		/**
		 * @return ReduceVisitor
		 */
		public function getVisitor();

		/**
		 * @return string
		 */
		public function getSchema();

		/**
		 * @param string $sql
		 *
		 * @return array
		 */
		public function fetch($sql);

		/**
		 * @param string $sql
		 *
		 * @return int
		 */
		public function alter($sql);

		/**
		 * @param $value
		 *
		 * @return mixed
		 */
		public function quote($value);
	}