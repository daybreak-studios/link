<?php
	namespace DaybreakStudios\Link\Connection\Adapter;

	abstract class AbstractAdapter implements AdapterInterface {
		private $name;
		private $schema;

		/**
		 * AbstractAdapter constructor.
		 *
		 * @param string $name
		 * @param string $schema
		 */
		public function __construct($name, $schema) {
			$this->name = $name;
			$this->schema = $schema;
		}

		/**
		 * @return string
		 */
		public function getName() {
			return $this->name;
		}

		/**
		 * @return string
		 */
		public function getSchema() {
			return $this->schema;
		}
	}