<?php
	namespace DaybreakStudios\Link\Connection\Adapter;

	class PostgreSqlPdoAdapter extends AbstractPdoAdapter {
		/**
		 * PostgreSqlPdoAdapter constructor.
		 *
		 * @param string   $schema
		 * @param string   $host
		 * @param string   $user
		 * @param string   $pass
		 * @param int|null $port
		 * @param array    $options
		 */
		public function __construct($schema, $host, $user, $pass, $port = null, array $options = []) {
			$dsn = sprintf('pgsql:host=%s;dbname=%s;user=%s;pass=%s', $host, $schema, $user, $pass);

			parent::__construct('pgsql', $schema, $dsn, null, null, $port, $options);
		}
	}