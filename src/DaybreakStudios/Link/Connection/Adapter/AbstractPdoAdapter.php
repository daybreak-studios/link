<?php
	namespace DaybreakStudios\Link\Connection\Adapter;

	use DaybreakStudios\Link\AST\Visitor\ToSqlVisitor;
	use DaybreakStudios\Link\Connection\Exception\StatementExecutionException;

	abstract class AbstractPdoAdapter extends AbstractAdapter {
		private $pdo;
		private $_cache = [];

		/**
		 * AbstractPdoAdapter constructor.
		 *
		 * @param string      $name
		 * @param string      $schema
		 * @param string      $dsn
		 * @param string|null $user
		 * @param string|null $pass
		 * @param int|null    $port
		 * @param array       $options
		 */
		public function __construct($name, $schema, $dsn, $user = null, $pass = null, $port = null, array $options = []) {
			parent::__construct('pdo.' . $name, $schema);

			if ($port !== null && stripos($dsn, 'port=') === false)
				$dsn .= ';port=' . $port;

			$this->pdo = new \PDO($dsn, $user, $pass, $options);
		}

		public function fetch($sql) {
			return $this->execute($sql)->fetchAll(\PDO::FETCH_OBJ);
		}

		public function alter($sql) {
			$count = $this->execute($sql)->rowCount();

			if (stripos($sql, 'insert') === 0)
				return $this->getPdo()->lastInsertId();

			return $count;
		}

		public function quote($value) {
			$type = gettype($value);

			if ($type === 'integer' || $type === 'double')
				return $value;
			else if ($type === 'boolean')
				return (int)$value;

			return $this->getPdo()->quote($value);
		}

		/**
		 * @return ToSqlVisitor
		 */
		public function getVisitor() {
			return new ToSqlVisitor($this);
		}

		/**
		 * @return \PDO
		 */
		protected function getPdo() {
			return $this->pdo;
		}

		/**
		 * @return array
		 */
		protected function getCache() {
			return $this->_cache;
		}

		/**
		 * @param string $sql
		 *
		 * @return \PDOStatement|null
		 */
		protected function getCachedStatement($sql) {
			$hash = hash('sha256', $sql);

			if (!isset($this->_cache[$hash]))
				return null;

			return $this->_cache[$hash];
		}

		/**
		 * @param string        $sql
		 * @param \PDOStatement $stmt
		 *
		 * @return $this
		 */
		protected function cache($sql, \PDOStatement $stmt) {
			$this->_cache[hash('sha256', $sql)] = $stmt;

			return $this;
		}

		/**
		 * @return $this
		 */
		protected function clearCache() {
			$this->_cache = [];

			return $this;
		}

		/**
		 * @param string $sql
		 *
		 * @return \PDOStatement
		 * @throws StatementExecutionException
		 */
		protected function execute($sql) {
			$stmt = $this->getCachedStatement($sql);

			if ($stmt === null) {
				$stmt = $this->getPdo()->prepare($sql);

				$this->cache($sql, $stmt);
			}

			if (!$stmt->execute())
				throw $this->getStatementExecutionException($stmt);

			return $stmt;
		}

		/**
		 * @param \PDOStatement $stmt
		 *
		 * @return StatementExecutionException
		 */
		protected function getStatementExecutionException(\PDOStatement $stmt) {
			return new StatementExecutionException(sprintf('[%s] %s: %s', $stmt->errorCode(), $stmt->errorInfo()[1],
				$stmt->errorInfo()[2]));
		}
	}