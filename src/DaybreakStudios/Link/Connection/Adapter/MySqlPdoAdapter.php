<?php
	namespace DaybreakStudios\Link\Connection\Adapter;

	use DaybreakStudios\Link\AST\Visitor\MySqlVisitor;

	class MySqlPdoAdapter extends AbstractPdoAdapter {
		/**
		 * MySqlPdoAdapter constructor.
		 *
		 * @param string   $schema
		 * @param string   $host
		 * @param string   $user
		 * @param string   $pass
		 * @param int|null $port
		 * @param array    $options
		 */
		public function __construct($schema, $host, $user, $pass, $port = null, array $options = []) {
			parent::__construct('mysql', $schema, sprintf('mysql:dbname=%s;host=%s', $schema, $host), $user, $pass,
				$port, $options);
		}

		/**
		 * @return MySqlVisitor
		 */
		public function getVisitor() {
			return new MySqlVisitor($this);
		}
	}