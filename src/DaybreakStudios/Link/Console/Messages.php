<?php
	namespace DaybreakStudios\Link\Console;

	class Messages {
		const MISSING_CONFIG = 'You are missing a "cli-config.php" or "config/cli-config.php", which is required to ' .
			'load a database connection.' . PHP_EOL;

		const INVALID_CONFIG_RETURN = 'Your "cli-config.php" or "config/cli-config.php" must return a %s object, %s ' .
			'given.' . PHP_EOL;
	}