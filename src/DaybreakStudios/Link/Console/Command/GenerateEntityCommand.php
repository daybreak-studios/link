<?php
	namespace DaybreakStudios\Link\Console\Command;

	use DaybreakStudios\Link\Connection\Connection;
	use DaybreakStudios\Link\Console\Helper\ColumnTypeHelper;
	use DaybreakStudios\Link\Console\Helper\DestinationHelper;
	use DaybreakStudios\Link\Database\ColumnInterface;
	use DaybreakStudios\Link\Database\ForeignKeyConstraint;
	use DaybreakStudios\Link\Database\Reader\SqlReader;
	use DaybreakStudios\Link\Entity\Entity;
	use DaybreakStudios\Link\Utility\ClassBuilder\ClassBuilder;
	use DaybreakStudios\Link\Utility\ClassBuilder\Comment\BlockCommentBuilder;
	use DaybreakStudios\Link\Utility\StringUtil;
	use Symfony\Component\Console\Command\Command;
	use Symfony\Component\Console\Input\InputArgument;
	use Symfony\Component\Console\Input\InputInterface;
	use Symfony\Component\Console\Input\InputOption;
	use Symfony\Component\Console\Output\OutputInterface;

	class GenerateEntityCommand extends Command {
		const DEFAULT_PATH = 'src/Entity/';

		protected function configure() {
			parent
				::setName('generate:entity')
				->setDescription('Generates a Link entity from a table')
				->addArgument('entity', InputArgument::REQUIRED, 'The fully-qualified class name of the entity to ' .
					'generate (use forward slashes instead of backslashes)')
				->addArgument('destination', InputArgument::OPTIONAL, 'The directory where the generated entity ' .
					'should be saved', self::DEFAULT_PATH)
				->addOption('force', 'f', InputOption::VALUE_NONE, 'Overwrite any existing files');
		}

		protected function execute(InputInterface $input, OutputInterface $output) {
			if (!parent::getHelperSet()->has('link.connection'))
				throw new \BadMethodCallException('You MUST provide a connection helper to run this command.');

			/** @var Connection $connection */
			$connection = parent::getHelper('link.connection')->getConnection();
			$dest = parent::getHelperSet()->has('link.destination') ?
				$this->getDestinationHelper()->getDestination() :
				$input->getArgument('destination');

			$klass = str_replace('/', '\\', $input->getArgument('entity'));
			$entityName = substr($klass, strrpos($klass, '\\') + 1);
			$reader = new SqlReader(StringUtil::pluralize(strtolower($entityName)), $connection);

			$relations = [];

			foreach ($reader->getConstraints() as $constraint) {
				if (!($constraint instanceof ForeignKeyConstraint))
					continue;

				$relations = array_merge($relations, array_map(function(ColumnInterface $column) {
					return $column->getField();
				}, $constraint->getColumns()));
			}

			$classBuilder = ClassBuilder::create($entityName, null, substr($klass, 0, strrpos($klass, '\\')))
				->extend(Entity::class);

			$commentBuilder = BlockCommentBuilder::create();

			foreach ($reader->getColumns() as $column) {
				if (in_array($column->getField(), $relations))
					continue;

				$retType = ColumnTypeHelper::toPhpType($column->getType());

				$commentBuilder->addDocMethod($this->buildGetterName($column, $retType), false, [$retType]);

				if ($column->getField() !== 'id') {
					$field = StringUtil::classify($column->getField());

					$commentBuilder->addDocMethod('set' . $field, false, ['$this'], [
							lcfirst($field) => ColumnTypeHelper::toPhpType($column->getType(), null),
						]);
				}
			}

			if (sizeof($relations) > 0)
				$output->writeln([
					'<comment>This entity has foreign keys, which means it probably relates another entity (or ',
					'entities) in your application. I cannot generate them, so you will need to add them yourself. ',
					'Please see https://goo.gl/dFvGYo for information on how to do this.',
					'</>',
				]);

			$classBuilder->comment($commentBuilder->build());

			if (!file_exists($dest))
				mkdir($dest, 0755, true);

			if (file_exists($filePath = $dest . $entityName . '.php') && !$input->getOption('force'))
				throw new \InvalidArgumentException('File already exists at ' . $dest);

			$result = file_put_contents($filePath, '<?php' . PHP_EOL . $classBuilder->build()->build(1));

			if ($result)
				$output->writeln('<info>Generated class written to ' . $filePath . '</>');
			else
				$output->writeln('<error>Could not write to ' . $filePath . '</>');
		}

		/**
		 * @param ColumnInterface $column
		 * @param                 $retType
		 *
		 * @return string
		 */
		private function buildGetterName(ColumnInterface $column, $retType) {
			return ($retType === ColumnTypeHelper::TYPE_BOOLEAN ? 'is' : 'get') .
				StringUtil::classify($column->getField());
		}

		/**
		 * @return DestinationHelper
		 */
		private function getDestinationHelper() {
			return parent::getHelper('link.destination');
		}
	}