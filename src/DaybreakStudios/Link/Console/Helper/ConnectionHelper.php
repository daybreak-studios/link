<?php
	namespace DaybreakStudios\Link\Console\Helper;

	use DaybreakStudios\Link\Connection\Connection;
	use Symfony\Component\Console\Helper\Helper;

	class ConnectionHelper extends Helper {
		/**
		 * @var Connection
		 */
		private $connection;

		/**
		 * ConnectionHelper constructor.
		 *
		 * @param Connection $connection
		 */
		public function __construct(Connection $connection) {
			$this->connection = $connection;
		}

		/**
		 * @return Connection
		 */
		public function getConnection() {
			return $this->connection;
		}

		/**
		 * @return string
		 */
		public function getName() {
			return 'link.connection';
		}
	}