<?php
	namespace DaybreakStudios\Link\Console\Helper;

	use DaybreakStudios\Link\Database\Type\BooleanType;
	use DaybreakStudios\Link\Database\Type\DateTimeType;
	use DaybreakStudios\Link\Database\Type\IntegerType;
	use DaybreakStudios\Link\Database\Type\TypeInterface;

	class ColumnTypeHelper {
		const TYPE_BOOLEAN = 'bool';
		const TYPE_DATETIME = '\\DateTime';
		const TYPE_INTEGER = 'int';
		const TYPE_STRING = 'string';
		const TYPE_MIXED = 'mixed';

		const STRING_TYPES = [
			'text',
			'char',
			'varchar',
		];

		const TYPE_MAP = [
			BooleanType::class => 'bool',
			DateTimeType::class => '\\DateTime',
			IntegerType::class => 'int',
		];

		/**
		 * @param TypeInterface $type
		 * @param string        $default
		 *
		 * @return string|null
		 */
		public static function toPhpType(TypeInterface $type, $default = 'mixed') {
			if (array_key_exists(get_class($type), self::TYPE_MAP))
				return self::TYPE_MAP[get_class($type)];

			if (in_array($type->getName(), self::STRING_TYPES))
				return 'string';

			return $default;
		}
	}