<?php
	namespace DaybreakStudios\Link\Console\Helper;

	use Symfony\Component\Console\Helper\Helper;

	class DestinationHelper extends Helper {
		/**
		 * @var string
		 */
		private $destination;

		/**
		 * DestinationHelper constructor.
		 *
		 * @param string $destination
		 */
		public function __construct($destination) {
			if (strrpos($destination, '/') !== strlen($destination) - 1)
				$destination .= '/';

			$this->destination = $destination;
		}

		/**
		 * @return string
		 */
		public function getDestination() {
			return $this->destination;
		}

		/**
		 * @return string
		 */
		public function getName() {
			return 'link.destination';
		}
	}