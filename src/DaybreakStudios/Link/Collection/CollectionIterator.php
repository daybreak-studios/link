<?php
	namespace DaybreakStudios\Link\Collection;

	class CollectionIterator implements \Iterator {
		private $keys;
		private $collection;

		private $index = 0;

		public function __construct(Collection $collection) {
			$this->collection = $collection;
			$this->keys = $collection->keys();
		}

		public function current() {
			return $this->collection->get($this->keys[$this->index]);
		}

		public function key() {
			return $this->keys[$this->index];
		}

		public function next() {
			$this->index++;
		}

		public function rewind() {
			$this->index = 0;
		}

		public function valid() {
			return isset($this->keys[$this->index]);
		}
	}