<?php
	namespace DaybreakStudios\Link\Collection;

	class ArrayCollection extends AbstractCollection {
		protected $data;

		public function __construct(array $data = []) {
			$this->data = $data;
		}

		public function put($key, $value) {
			$ret = null;

			if ($this->has($key))
				$ret = $this->get($key);

			$this->data[$key] = $value;

			return $ret;
		}

		public function get($key, $default = null) {
			if (!$this->has($key))
				return $default;

			return $this->data[$key];
		}

		public function has($key) {
			return array_key_exists($key, $this->data);
		}

		public function contains($value) {
			return in_array($value, $this->data);
		}

		public function remove($key) {
			$previous = $this->get($key);

			if (!$this->has($key))
				unset($this->data[$key]);

			return $previous;
		}

		public function purge($value) {
			$remove = [];

			foreach ($this->data as $k => $v)
				if ($v === $value)
					$remove[] = $k;

			foreach ($remove as $key)
				$this->remove($key);

			return sizeof($remove) > 0;
		}

		public function toArray() {
			return $this->data;
		}

		public function keys() {
			return array_keys($this->data);
		}

		public function values() {
			return array_values($this->data);
		}
	}