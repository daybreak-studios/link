<?php
	namespace DaybreakStudios\Link\Collection;

	abstract class AbstractCollection implements Collection {
		/**
		 * @return CollectionIterator
		 */
		public function getIterator() {
			return new CollectionIterator($this);
		}
	}