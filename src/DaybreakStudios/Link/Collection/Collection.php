<?php
	namespace DaybreakStudios\Link\Collection;

	interface Collection extends \IteratorAggregate {
		/**
		 * Places a value at an index, returning the previous value, or null if there was not one.
		 *
		 * @param mixed $key
		 * @param mixed $value
		 *
		 * @return mixed
		 */
		public function put($key, $value);

		/**
		 * @param mixed $key
		 * @param mixed $default
		 *
		 * @return mixed
		 */
		public function get($key, $default = null);

		/**
		 * @param mixed $key
		 *
		 * @return bool
		 */
		public function has($key);

		/**
		 * @param mixed $value
		 *
		 * @return bool
		 */
		public function contains($value);

		/**
		 * Removes the key from the collection, returning the value at that key if there was one.
		 *
		 * @param mixed $key
		 *
		 * @return mixed
		 */
		public function remove($key);

		/**
		 * Removes all keys with the given value from the collection.
		 *
		 * @param $value
		 *
		 * @return bool true if any keys were removed, false otherwise
		 */
		public function purge($value);

		/**
		 * @return array
		 */
		public function keys();

		/**
		 * @return array
		 */
		public function values();

		/**
		 * Returns a copy of the underlying collection as an associative array
		 *
		 * @return array
		 */
		public function toArray();
	}