<?php
	namespace DaybreakStudios\Link\AST;

	use DaybreakStudios\Link\AST\Node\DeleteStatementNode;
	use DaybreakStudios\Link\AST\Node\LimitNode;
	use DaybreakStudios\Link\AST\Node\Node;
	use DaybreakStudios\Link\Connection\Connection;

	class DeleteManager extends TreeManager {
		public function __construct(Connection $connection = null) {
			$ast = new DeleteStatementNode();

			parent::__construct($ast, $ast, $connection);
		}

		/**
		 * @return DeleteStatementNode
		 */
		public function getTree() {
			return parent::getTree();
		}

		/**
		 * @param $relation
		 *
		 * @return $this
		 */
		public function from($relation) {
			$this->getTree()->setRelation($relation);

			return $this;
		}

		/**
		 * @param $limit
		 *
		 * @return $this
		 */
		public function limit($limit) {
			if ($limit)
				$this->getTree()->setLimit(new LimitNode(Node::buildQuoted($limit)));
			else
				$this->getTree()->setLimit(null);

			return $this;
		}

		/**
		 * @param mixed $expr
		 *
		 * @return $this
		 */
		public function where($expr) {
			$this->getTree()->addWhere($expr);

			return $this;
		}
	}