<?php
	namespace DaybreakStudios\Link\AST;

	use DaybreakStudios\Link\Connection\Connection;

	class DescribeManager extends SelectManager {
		private $schema;

		/**
		 * DescribeManager constructor.
		 *
		 * @param Connection|null $connection
		 */
		public function __construct(Connection $connection = null) {
			$this->schema = new Table('COLUMNS', null, 'INFORMATION_SCHEMA');

			parent::__construct($this->schema);
		}

		/**
		 * @param string $schema
		 * @param string $table
		 *
		 * @return $this
		 */
		public function describe($schema, $table) {
			return parent::select(
					$this->schema['column_name'],
					Table::call('IF')->with($this->schema['is_nullable']->eq('NO'), true, false)->named('nullable'),
					$this->schema['character_maximum_length']->named('length'),
					$this->schema['numeric_precision']->named('prec'),
					$this->schema['column_key']->named('ctype'),
					$this->schema['extra']->named('extra')
				)
				->where($this->schema['table_schema']->eq($schema)->andWhere($this->schema['table_name']->eq($table)));
		}
	}