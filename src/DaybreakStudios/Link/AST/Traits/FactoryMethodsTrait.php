<?php
	namespace DaybreakStudios\Link\AST\Traits;

	use DaybreakStudios\Link\AST\Node\AndNode;
	use DaybreakStudios\Link\AST\Node\FalseNode;
	use DaybreakStudios\Link\AST\Node\GroupingNode;
	use DaybreakStudios\Link\AST\Node\InnerJoinNode;
	use DaybreakStudios\Link\AST\Node\NamedFunctionNode;
	use DaybreakStudios\Link\AST\Node\Node;
	use DaybreakStudios\Link\AST\Node\OnNode;
	use DaybreakStudios\Link\AST\Node\SqlLiteral;
	use DaybreakStudios\Link\AST\Node\StringJoinNode;
	use DaybreakStudios\Link\AST\Node\TableAliasNode;
	use DaybreakStudios\Link\AST\Node\TrueNode;

	trait FactoryMethodsTrait {
		/**
		 * @return TrueNode
		 */
		public function createTrue() {
			return new TrueNode();
		}

		/**
		 * @return FalseNode
		 */
		public function createFalse() {
			return new FalseNode();
		}

		/**
		 * @param $relation
		 * @param $name
		 *
		 * @return TableAliasNode
		 */
		public function createTableAlias($relation, $name) {
			return new TableAliasNode($relation, $name);
		}

		/**
		 * @param      $to
		 * @param null $constraint
		 * @param      $klass
		 *
		 * @return mixed
		 */
		public function createJoin($to, $constraint = null, $klass = InnerJoinNode::class) {
			return new $klass($to, $constraint);
		}

		/**
		 * @param $to
		 *
		 * @return mixed
		 */
		public function createStringJoin($to) {
			return $this->createJoin($to, null, StringJoinNode::class);
		}

		/**
		 * @param array $clauses
		 *
		 * @return AndNode
		 */
		public function createAnd(array $clauses) {
			return new AndNode($clauses);
		}

		/**
		 * @param $expr
		 *
		 * @return OnNode
		 */
		public function createOn($expr) {
			return new OnNode($expr);
		}

		/**
		 * @param $expr
		 *
		 * @return GroupingNode
		 */
		public function grouping($expr) {
			return new GroupingNode($expr);
		}

		/**
		 * @param $raw
		 *
		 * @return SqlLiteral
		 */
		public function createRawSql($raw) {
			return new SqlLiteral($raw);
		}

		/**
		 * @param $column
		 *
		 * @return NamedFunctionNode
		 */
		public function lower($column) {
			return new NamedFunctionNode('LOWER', [Node::buildQuoted($column)]);
		}
	}