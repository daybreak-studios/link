<?php
	namespace DaybreakStudios\Link\AST\Traits;

	use DaybreakStudios\Link\AST\Node\AdditionInfixNode;
	use DaybreakStudios\Link\AST\Node\BitwiseAndInfixNode;
	use DaybreakStudios\Link\AST\Node\BitwiseNotUnaryOperandNode;
	use DaybreakStudios\Link\AST\Node\BitwiseOrInfixNode;
	use DaybreakStudios\Link\AST\Node\BitwiseShiftLeftInfixNode;
	use DaybreakStudios\Link\AST\Node\BitwiseShiftRightInfixNode;
	use DaybreakStudios\Link\AST\Node\BitwiseXorInfixNode;
	use DaybreakStudios\Link\AST\Node\DivisionInfixNode;
	use DaybreakStudios\Link\AST\Node\GroupingNode;
	use DaybreakStudios\Link\AST\Node\MultiplicationInfixNode;
	use DaybreakStudios\Link\AST\Node\Node;
	use DaybreakStudios\Link\AST\Node\SubtractionInfixNode;

	trait MathTrait {
		public function mul($other) {
			return new MultiplicationInfixNode($this, $this->_quote($other));
		}

		public function div($other) {
			return new DivisionInfixNode($this, $this->_quote($other));
		}

		public function add($other) {
			return new GroupingNode(new AdditionInfixNode($this, $this->_quote($other)));
		}

		public function sub($other) {
			return new GroupingNode(new SubtractionInfixNode($this, $this->_quote($other)));
		}

		public function band($other) {
			return new GroupingNode(new BitwiseAndInfixNode($this, $this->_quote($other)));
		}

		public function bor($other) {
			return new GroupingNode(new BitwiseOrInfixNode($this, $this->_quote($other)));
		}

		public function bxor($other) {
			return new GroupingNode(new BitwiseXorInfixNode($this, $this->_quote($other)));
		}

		public function blshift($other) {
			return new GroupingNode(new BitwiseShiftLeftInfixNode($this, $this->_quote($other)));
		}

		public function brshift($other) {
			return new GroupingNode(new BitwiseShiftRightInfixNode($this, $this->_quote($other)));
		}

		public function bnot() {
			return new BitwiseNotUnaryOperandNode($this);
		}

		private function _quote($other) {
			return Node::buildQuoted($other);
		}
	}