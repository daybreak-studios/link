<?php
	namespace DaybreakStudios\Link\AST\Traits;

	use DaybreakStudios\Link\AST\Node\OverNode;

	trait WindowPredicationsTrait {
		public function over($right = null) {
			return new OverNode($this, $right);
		}
	}