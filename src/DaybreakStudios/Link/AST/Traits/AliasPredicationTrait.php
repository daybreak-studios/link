<?php
	namespace DaybreakStudios\Link\AST\Traits;

	use DaybreakStudios\Link\AST\Node\AsNode;
	use DaybreakStudios\Link\AST\Node\SqlLiteral;

	trait AliasPredicationTrait {
		/**
		 * @param string $other
		 *
		 * @return AsNode
		 */
		public function named($other) {
			return new AsNode($this, new SqlLiteral($other));
		}
	}