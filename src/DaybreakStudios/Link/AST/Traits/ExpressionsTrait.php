<?php
	namespace DaybreakStudios\Link\AST\Traits;

	use DaybreakStudios\Link\AST\Node\AvgFunctionNode;
	use DaybreakStudios\Link\AST\Node\CountFunctionNode;
	use DaybreakStudios\Link\AST\Node\ExtractNode;
	use DaybreakStudios\Link\AST\Node\MaxFunctionNode;
	use DaybreakStudios\Link\AST\Node\MinFunctionNode;
	use DaybreakStudios\Link\AST\Node\SumFunctionNode;

	trait ExpressionsTrait {
		/**
		 * @param bool $distinct
		 *
		 * @return CountFunctionNode
		 */
		public function count($distinct = false) {
			return new CountFunctionNode([$this], $distinct);
		}

		/**
		 * @return SumFunctionNode
		 */
		public function sum() {
			return new SumFunctionNode([$this]);
		}

		/**
		 * @return MaxFunctionNode
		 */
		public function maximum() {
			return new MaxFunctionNode([$this]);
		}

		/**
		 * @return MinFunctionNode
		 */
		public function minimum() {
			return new MinFunctionNode([$this]);
		}

		/**
		 * @return AvgFunctionNode
		 */
		public function average() {
			return new AvgFunctionNode([$this]);
		}

		/**
		 * @param $field
		 *
		 * @return ExtractNode
		 */
		public function extract($field) {
			return new ExtractNode([$this], $field);
		}
	}