<?php
	namespace DaybreakStudios\Link\AST\Traits;

	use DaybreakStudios\Link\AST\Node\AndNode;
	use DaybreakStudios\Link\AST\Node\BetweenNode;
	use DaybreakStudios\Link\AST\Node\CaseNode;
	use DaybreakStudios\Link\AST\Node\ConcatInfixNode;
	use DaybreakStudios\Link\AST\Node\DoesNotMatchNode;
	use DaybreakStudios\Link\AST\Node\EqualityNode;
	use DaybreakStudios\Link\AST\Node\GreaterThanNode;
	use DaybreakStudios\Link\AST\Node\GreaterThanOrEqualNode;
	use DaybreakStudios\Link\AST\Node\GroupingNode;
	use DaybreakStudios\Link\AST\Node\InNode;
	use DaybreakStudios\Link\AST\Node\LessThanNode;
	use DaybreakStudios\Link\AST\Node\LessThanOrEqualNode;
	use DaybreakStudios\Link\AST\Node\MatchesNode;
	use DaybreakStudios\Link\AST\Node\Node;
	use DaybreakStudios\Link\AST\Node\NotEqualNode;
	use DaybreakStudios\Link\AST\Node\NotInNode;
	use DaybreakStudios\Link\AST\Node\NotRegexNode;
	use DaybreakStudios\Link\AST\Node\OrNode;
	use DaybreakStudios\Link\AST\Node\RegexNode;

	trait PredicationsTrait {
		/**
		 * @param $right
		 *
		 * @return LessThanNode
		 */
		public function lt($right) {
			return new LessThanNode($this, $this->quote($right));
		}

		/**
		 * @param $others
		 *
		 * @return GroupingNode
		 */
		public function ltAny($others) {
			return $this->buildAnyGrouping([$this, 'lt'], $others);
		}

		/**
		 * @param $others
		 *
		 * @return GroupingNode
		 */
		public function ltAll($others) {
			return $this->buildAllGrouping([$this, 'lt'], $others);
		}

		/**
		 * @param $right
		 *
		 * @return LessThanOrEqualNode
		 */
		public function lte($right) {
			return new LessThanOrEqualNode($this, $this->quote($right));
		}

		/**
		 * @param $others
		 *
		 * @return GroupingNode
		 */
		public function lteAny($others) {
			return $this->buildAnyGrouping([$this, 'lte'], $others);
		}

		/**
		 * @param $others
		 *
		 * @return GroupingNode
		 */
		public function lteAll($others) {
			return $this->buildAllGrouping([$this, 'lte'], $others);
		}

		/**
		 * @param $other
		 *
		 * @return EqualityNode
		 */
		public function eq($other) {
			return new EqualityNode($this, $this->quote($other));
		}

		/**
		 * @param $others
		 *
		 * @return GroupingNode
		 */
		public function eqAny($others) {
			return $this->buildAnyGrouping([$this, 'eq'], $others);
		}

		/**
		 * @param $others
		 *
		 * @return GroupingNode
		 */
		public function eqAll($others) {
			return $this->buildAllGrouping([$this, 'eq'], $this->quoteArray($others));
		}

		/**
		 * @param $other
		 *
		 * @return NotEqualNode
		 */
		public function neq($other) {
			return new NotEqualNode($this, $this->quote($other));
		}

		/**
		 * @param $others
		 *
		 * @return GroupingNode
		 */
		public function neqAny($others) {
			return $this->buildAnyGrouping([$this, 'neq'], $others);
		}

		/**
		 * @param $others
		 *
		 * @return GroupingNode
		 */
		public function neqAll($others) {
			return $this->buildAllGrouping([$this, 'neq',], $others);
		}

		/**
		 * @param $right
		 *
		 * @return GreaterThanNode
		 */
		public function gt($right) {
			return new GreaterThanNode($this, $this->quote($right));
		}

		/**
		 * @param $others
		 *
		 * @return GroupingNode
		 */
		public function gtAny($others) {
			return $this->buildAnyGrouping([$this, 'gt'], $others);
		}

		/**
		 * @param $others
		 *
		 * @return GroupingNode
		 */
		public function gtAll($others) {
			return $this->buildAllGrouping([$this, 'gt'], $others);
		}

		/**
		 * @param $right
		 *
		 * @return GreaterThanOrEqualNode
		 */
		public function gte($right) {
			return new GreaterThanOrEqualNode($this, $this->quote($right));
		}

		/**
		 * @param $others
		 *
		 * @return GroupingNode
		 */
		public function gteAny($others) {
			return $this->buildAnyGrouping([$this, 'gte'], $others);
		}

		/**
		 * @param $others
		 *
		 * @return GroupingNode
		 */
		public function gteAll($others) {
			return $this->buildAllGrouping([$this, 'gte'], $others);
		}

		/**
		 * @param $left
		 * @param $right
		 *
		 * @return BetweenNode
		 */
		public function between($left, $right) {
			return new BetweenNode($this, $this->quote($left)->andWhere($this->quote($right)));
		}

		/**
		 * @param $other
		 *
		 * @return InNode
		 */
		public function in($other) {
			if (is_array($other) || $other instanceof \Traversable)
				return new InNode($this, $this->quoteArray($other));

			return new InNode($this, $this->quote($other));
		}

		/**
		 * @param $others
		 *
		 * @return GroupingNode
		 */
		public function inAny($others) {
			return $this->buildAnyGrouping([$this, 'in'], $others);
		}

		/**
		 * @param $others
		 *
		 * @return GroupingNode
		 */
		public function inAll($others) {
			return $this->buildAllGrouping([$this, 'in'], $others);
		}

		/**
		 * @param $other
		 *
		 * @return NotInNode
		 */
		public function nin($other) {
			if (is_array($other) || $other instanceof \Traversable)
				return new NotInNode($this, $this->quoteArray($other));

			return new NotInNode($this, $this->quote($other));
		}

		/**
		 * @param $others
		 *
		 * @return GroupingNode
		 */
		public function ninAny($others) {
			return $this->buildAnyGrouping([$this, 'nin'], $others);
		}

		/**
		 * @param $others
		 *
		 * @return GroupingNode
		 */
		public function ninAll($others) {
			return $this->buildAllGrouping([$this, 'nin'], $others);
		}

		/**
		 * @param      $other
		 * @param null $escape
		 * @param bool $caseSensitive
		 *
		 * @return MatchesNode
		 */
		public function matches($other, $escape = null, $caseSensitive = false) {
			return new MatchesNode($this, $this->quote($other), $escape, $caseSensitive);
		}

		/**
		 * @param      $others
		 * @param null $escape
		 * @param bool $caseSensitive
		 *
		 * @return GroupingNode
		 */
		public function matchesAny($others, $escape = null, $caseSensitive = false) {
			return $this->buildAnyGrouping([$this, 'matches'], $others, $escape, $caseSensitive);
		}

		/**
		 * @param      $others
		 * @param null $escape
		 * @param bool $caseSensitive
		 *
		 * @return GroupingNode
		 */
		public function matchesAll($others, $escape = null, $caseSensitive = false) {
			return $this->buildAllGrouping([$this, 'matches'], $others, $escape, $caseSensitive);
		}

		/**
		 * @param      $other
		 * @param null $escape
		 * @param bool $caseSensitive
		 *
		 * @return DoesNotMatchNode
		 */
		public function doesNotMatch($other, $escape = null, $caseSensitive = false) {
			return new DoesNotMatchNode($this, $this->quote($other), $escape, $caseSensitive);
		}

		/**
		 * @param      $others
		 * @param null $escape
		 * @param bool $caseSensitive
		 *
		 * @return GroupingNode
		 */
		public function doesNotMatchAny($others, $escape = null, $caseSensitive = false) {
			return $this->buildAnyGrouping([$this, 'doesNotMatch'], $others, $escape, $caseSensitive);
		}

		/**
		 * @param      $others
		 * @param null $escape
		 * @param bool $caseSensitive
		 *
		 * @return GroupingNode
		 */
		public function doesNotMatchAll($others, $escape = null, $caseSensitive = false) {
			return $this->buildAllGrouping([$this, 'doesNotMatch'], $others, $escape, $caseSensitive);
		}

		/**
		 * @param      $other
		 * @param bool $caseSensitive
		 *
		 * @return RegexNode
		 */
		public function regex($other, $caseSensitive = true) {
			return new RegexNode($this, $this->quote($other), $caseSensitive);
		}

		/**
		 * @param      $other
		 * @param bool $caseSensitive
		 *
		 * @return NotRegexNode
		 */
		public function notRegex($other, $caseSensitive = true) {
			return new NotRegexNode($this, $this->quote($other), $caseSensitive);
		}

		/**
		 * @param $right
		 *
		 * @return CaseNode
		 */
		public function when($right) {
			return (new CaseNode($this))->when($this->quote($right));
		}

		/**
		 * @param $other
		 *
		 * @return ConcatInfixNode
		 */
		public function concat($other) {
			return new ConcatInfixNode($this, $other);
		}

		/**
		 * @param $other
		 *
		 * @return \DaybreakStudios\Link\AST\Node\CastedNode|\DaybreakStudios\Link\AST\Node\QuotedNode
		 */
		private function quote($other) {
			return Node::buildQuoted($other, $this);
		}

		/**
		 * @param $other
		 *
		 * @return array
		 */
		private function quoteArray($other) {
			$quoted = [];

			foreach ($other as $o)
				$quoted[] = $this->quote($o);

			return $quoted;
		}

		/**
		 * @param callable $call
		 * @param          $others
		 * @param array    ...$extras
		 *
		 * @return GroupingNode
		 */
		private function buildAnyGrouping(callable $call, $others, ... $extras) {
			$nodes = array_map(function ($expr) use ($call, $extras) {
				return call_user_func($call, $expr, ...$extras);
			}, $others);

			$composed = new OrNode(array_shift($nodes), array_shift($nodes));

			foreach ($nodes as $node)
				$composed = new OrNode($composed, $node);

			return new GroupingNode($composed);
		}

		/**
		 * @param callable $call
		 * @param          $others
		 * @param array    ...$extras
		 *
		 * @return GroupingNode
		 */
		private function buildAllGrouping(callable $call, $others, ... $extras) {
			return new GroupingNode(new AndNode(array_map(function ($expr) use ($call, $extras) {
				return call_user_func($call, $expr, ...$extras);
			}, $others)));
		}
	}