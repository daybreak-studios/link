<?php
	namespace DaybreakStudios\Link\AST\Traits;

	use DaybreakStudios\Link\AST\Node\AscendingNode;
	use DaybreakStudios\Link\AST\Node\DescendingNode;

	trait OrderPredicationsTrait {
		/**
		 * @return AscendingNode
		 */
		public function asc() {
			return new AscendingNode($this);
		}

		/**
		 * @return DescendingNode
		 */
		public function desc() {
			return new DescendingNode($this);
		}
	}