<?php
	namespace DaybreakStudios\Link\AST;

	use DaybreakStudios\Link\AST\Node\AssignmentNode;
	use DaybreakStudios\Link\AST\Node\Node;
	use DaybreakStudios\Link\AST\Node\UnqualifiedColumnNode;
	use DaybreakStudios\Link\AST\Node\UpdateStatementNode;
	use DaybreakStudios\Link\Connection\Connection;

	class UpdateManager extends TreeManager {
		/**
		 * UpdateManager constructor.
		 *
		 * @param Connection|null $connection
		 */
		public function __construct(Connection $connection = null) {
			$ast = new UpdateStatementNode();

			parent::__construct($ast, $ast, $connection);
		}

		/**
		 * @return UpdateStatementNode
		 */
		public function getTree() {
			return parent::getTree();
		}

		/**
		 * @return mixed|null
		 */
		public function getKey() {
			return $this->getTree()->getKey();
		}

		/**
		 * @param $limit
		 *
		 * @return $this
		 */
		public function limit($limit) {
			$this->getTree()->setLimit($limit);

			return $this;
		}

		/**
		 * @param $key
		 *
		 * @return $this
		 */
		public function key($key) {
			$this->getTree()->setKey(Node::buildQuoted($key));

			return $this;
		}

		/**
		 * @param array ...$orders
		 *
		 * @return $this
		 */
		public function order(... $orders) {
			$this->getTree()->setOrders($orders);

			return $this;
		}

		/**
		 * @param $table
		 *
		 * @return $this
		 */
		public function table($table) {
			$this->getTree()->setRelation($table);

			return $this;
		}

		/**
		 * @param mixed $expr
		 *
		 * @return $this
		 */
		public function where($expr) {
			$this->getTree()->addWhere($expr);

			return $this;
		}

		/**
		 * @param $values
		 *
		 * @return $this
		 */
		public function set($values) {
			if (is_string($values))
				$this->getTree()->addValue([$values]);
			else
				foreach ($values as $column => $value)
					$this->getTree()->addValue(new AssignmentNode(new UnqualifiedColumnNode($column), $value));

			return $this;
		}
	}