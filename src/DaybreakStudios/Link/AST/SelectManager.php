<?php
	namespace DaybreakStudios\Link\AST;

	use DaybreakStudios\Link\AST\Node\AndNode;
	use DaybreakStudios\Link\AST\Node\BinaryNode;
	use DaybreakStudios\Link\AST\Node\DistinctNode;
	use DaybreakStudios\Link\AST\Node\DistinctOnNode;
	use DaybreakStudios\Link\AST\Node\ExceptNode;
	use DaybreakStudios\Link\AST\Node\ExistsFunctionNode;
	use DaybreakStudios\Link\AST\Node\InnerJoinNode;
	use DaybreakStudios\Link\AST\Node\IntersectNode;
	use DaybreakStudios\Link\AST\Node\JoinNode;
	use DaybreakStudios\Link\AST\Node\LimitNode;
	use DaybreakStudios\Link\AST\Node\LockNode;
	use DaybreakStudios\Link\AST\Node\NamedWindowNode;
	use DaybreakStudios\Link\AST\Node\Node;
	use DaybreakStudios\Link\AST\Node\TableAliasNode;
	use DaybreakStudios\Link\AST\Node\OffsetNode;
	use DaybreakStudios\Link\AST\Node\OnNode;
	use DaybreakStudios\Link\AST\Node\OuterJoinNode;
	use DaybreakStudios\Link\AST\Node\SelectCoreNode;
	use DaybreakStudios\Link\AST\Node\SelectStatementNode;
	use DaybreakStudios\Link\AST\Node\SqlLiteral;
	use DaybreakStudios\Link\AST\Node\StringJoinNode;
	use DaybreakStudios\Link\AST\Node\TopNode;
	use DaybreakStudios\Link\AST\Node\UnionAllNode;
	use DaybreakStudios\Link\AST\Node\UnionNode;
	use DaybreakStudios\Link\AST\Node\WithNode;
	use DaybreakStudios\Link\AST\Node\WithRecursiveNode;

	class SelectManager extends TreeManager {
		private $table;

		/**
		 * SelectManager constructor.
		 *
		 * @param null $table
		 */
		public function __construct($table = null) {
			$ast = new SelectStatementNode();
			$cores = $ast->getCores();

			parent::__construct($ast, $cores[sizeof($cores) - 1]);

			$this->table = $table;

			$this->from($table);
		}

		/**
		 * @return SelectCoreNode
		 */
		public function getCore() {
			return parent::getCore();
		}

		/**
		 * @return SelectStatementNode
		 */
		public function getTree() {
			return parent::getTree();
		}

		/**
		 * @return mixed|null
		 */
		public function getLimit() {
			$limit = $this->getTree()->getLimit();

			if ($limit instanceof LimitNode)
				return $limit->getChild();

			return $limit;
		}

		/**
		 * @return mixed|null
		 */
		public function getOffset() {
			$offset = $this->getTree()->getOffset();

			if ($offset instanceof OffsetNode)
				return $offset->getChild();

			return $offset;
		}

		/**
		 * @param mixed $table
		 *
		 * @return $this
		 */
		public function from($table) {
			if (is_string($table))
				$table = new SqlLiteral($table);

			switch (get_class($table)) {
				case JoinNode::class:
					$this->getCore()->getSource()->addRight($table);

					break;

				default:
					$this->getCore()->getSource()->setLeft($table);
			}

			return $this;
		}

		/**
		 * @param mixed $other
		 *
		 * @return TableAliasNode
		 */
		public function named($other) {
			return $this->createTableAlias($this->grouping($this->getTree()), new SqlLiteral($other));
		}

		/**
		 * @param string|bool|SqlLiteral $locking
		 *
		 * @return $this
		 */
		public function lock($locking = 'FOR UPDATE') {
			if (is_string($locking))
				$locking = new SqlLiteral($locking);
			else if ($locking === true)
				$locking = new SqlLiteral('FOR UPDATE');

			$this->getTree()->setLock(new LockNode($locking));

			return $this;
		}

		/**
		 * @return bool
		 */
		public function isLocked() {
			return $this->getTree()->getLock() !== null;
		}

		/**
		 * @param array ...$exprs
		 *
		 * @return $this
		 */
		public function on(... $exprs) {
			/** @var BinaryNode[] $right */
			$this->getCore()->getSource()->addRight(new OnNode($this->collapse($exprs)));

			return $this;
		}

		/**
		 * @param mixed  $relation
		 * @param string $klass
		 *
		 * @return $this
		 */
		public function join($relation, $klass = InnerJoinNode::class) {
			if (!$relation)
				return $this;

			if (is_string($relation) && strlen($relation) === 0 ||
				$relation instanceof SqlLiteral && $relation->isEmpty()
			)
				throw new \RuntimeException('$relation cannot be empty');
			else if (is_string($relation) || $relation instanceof SqlLiteral)
				$klass = StringJoinNode::class;

			$this->getCore()->getSource()->addRight($this->createJoin($relation, null, $klass));

			return $this;
		}

		/**
		 * @param mixed $relation
		 *
		 * @return $this
		 */
		public function outerJoin($relation) {
			return $this->join($relation, OuterJoinNode::class);
		}

		/**
		 * @param $expr
		 *
		 * @return $this
		 */
		public function having($expr) {
			$this->getCore()->addHaving($expr);

			return $this;
		}

		/**
		 * @param $name
		 *
		 * @return NamedWindowNode
		 */
		public function window($name) {
			$this->getCore()->addWindow($window = new NamedWindowNode($name));

			return $window;
		}

		/**
		 * @param array ...$selects
		 *
		 * @return $this
		 */
		public function select(... $selects) {
			foreach ($selects as $select)
				$this->getCore()->addSelect(is_string($select) ? new SqlLiteral($select) : $select);

			return $this;
		}

		/**
		 * @return array
		 */
		public function getSelects() {
			return $this->getCore()->getSelects();
		}

		/**
		 * @param array $selects
		 *
		 * @return $this
		 */
		public function setSelects(array $selects) {
			$this->getCore()->setSelects($selects);

			return $this;
		}

		/**
		 * @param bool $value
		 *
		 * @return $this
		 */
		public function distinct($value = true) {
			if ($value)
				$this->getCore()->setSetQuantifier(new DistinctNode());
			else
				$this->getCore()->setSetQuantifier(null);

			return $this;
		}

		/**
		 * @param mixed $value
		 *
		 * @return $this
		 */
		public function distinctOn($value) {
			if ($value)
				$this->getCore()->setSetQuantifier(new DistinctOnNode($value));
			else
				$this->getCore()->setSetQuantifier(null);

			return $this;
		}

		/**
		 * @param array ...$exprs
		 *
		 * @return $this
		 */
		public function order(... $exprs) {
			foreach ($exprs as $expr)
				$this->getTree()->addOrder(is_string($expr) ? new SqlLiteral($expr) : $expr);

			return $this;
		}

		/**
		 * @return array
		 */
		public function getOrders() {
			return $this->getTree()->getOrders();
		}

		/**
		 * @param array $orders
		 *
		 * @return $this
		 */
		public function setOrders(array $orders) {
			$this->getTree()->setOrders($orders);

			return $this;
		}

		/**
		 * @param TreeManager $other
		 * @param string      $klass
		 *
		 * @return UnionAllNode|UnionNode
		 * @internal param $operation
		 */
		public function union(TreeManager $other, $klass = UnionNode::class) {
			return new $klass($this->getTree(), $other->getTree());
		}

		/**
		 * @param TreeManager $other
		 *
		 * @return IntersectNode
		 */
		public function intersect(TreeManager $other) {
			return new IntersectNode($this->getTree(), $other->getTree());
		}

		/**
		 * @param TreeManager $other
		 *
		 * @return ExceptNode
		 */
		public function except(TreeManager $other) {
			return new ExceptNode($this->getTree(), $other->getTree());
		}

		/**
		 * @param array ...$subqueries
		 *
		 * @return $this
		 */
		public function with(... $subqueries) {
			if (sizeof($subqueries) === 0)
				return $this;

			$klass = WithNode::class;

			if ($subqueries[0] === WithRecursiveNode::class)
				$klass = array_shift($subqueries);

			$this->getTree()->setWith(new $klass($subqueries));

			return $this;
		}

		/**
		 * @return ExistsFunctionNode
		 */
		public function exists() {
			return new ExistsFunctionNode($this->getTree());
		}

		/**
		 * @param int|null $limit
		 *
		 * @return $this
		 */
		public function limit($limit) {
			if ($limit) {
				$this->getTree()->setLimit(new LimitNode(Node::buildQuoted($limit)));
				$this->getCore()->setTop(new TopNode(Node::buildQuoted($limit)));
			} else {
				$this->getTree()->setLimit(null);
				$this->getCore()->setTop(null);
			}

			return $this;
		}

		/**
		 * @param int|null $offset
		 *
		 * @return $this
		 */
		public function offset($offset) {
			$this->getTree()->setOffset($offset ? new OffsetNode(Node::buildQuoted($offset)) : null);

			return $this;
		}

		/**
		 * @param array $exprs
		 *
		 * @return AndNode
		 */
		private function collapse(array $exprs) {
			$exprs = array_filter($exprs, function ($expr) {
				return $expr !== null;
			});

			$exprs = array_map(function ($expr) {
				if (is_string($expr))
					return $this->createRawSql($expr);

				return $expr;
			}, $exprs);

			if (sizeof($exprs) === 1)
				return $exprs[0];

			return $this->createAnd($exprs);
		}
	}