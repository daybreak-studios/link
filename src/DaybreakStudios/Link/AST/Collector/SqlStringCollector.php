<?php
	namespace DaybreakStudios\Link\AST\Collector;

	class SqlStringCollector extends PlainStringCollector {
		private $bindIndex = 1;

		/**
		 * @param callable $bind
		 *
		 * @return $this
		 */
		public function addBind(callable $bind) {
			call_user_func($bind, ++$this->bindIndex);

			return $this;
		}
	}