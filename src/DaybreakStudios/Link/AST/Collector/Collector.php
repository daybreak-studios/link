<?php
	namespace DaybreakStudios\Link\AST\Collector;

	interface Collector {
		/**
		 * @param string $value
		 *
		 * @return Collector
		 */
		public function append($value);

		/**
		 * @return string
		 */
		public function getValue();
	}