<?php
	namespace DaybreakStudios\Link\AST\Collector;

	class PlainStringCollector implements Collector {
		private $string = '';

		/**
		 * @return string
		 */
		public function getValue() {
			return $this->string;
		}

		/**
		 * @param string $value
		 *
		 * @return Collector
		 */
		public function append($value) {
			$this->string .= $value;

			return $this;
		}
	}