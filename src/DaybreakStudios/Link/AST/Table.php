<?php
	namespace DaybreakStudios\Link\AST;

	use DaybreakStudios\Link\AST\Attribute\Attribute;
	use DaybreakStudios\Link\AST\Node\AllColumnsNode;
	use DaybreakStudios\Link\AST\Node\GroupingNode;
	use DaybreakStudios\Link\AST\Node\InnerJoinNode;
	use DaybreakStudios\Link\AST\Node\NamedFunctionNode;
	use DaybreakStudios\Link\AST\Node\OuterJoinNode;
	use DaybreakStudios\Link\AST\Node\SqlLiteral;
	use DaybreakStudios\Link\AST\Node\StringJoinNode;

	class Table implements \ArrayAccess {
		private $name;
		private $alias;
		private $schema;
		private $select;

		/**
		 * Table constructor.
		 *
		 * @param      $name
		 * @param null $alias
		 * @param null $schema
		 */
		public function __construct($name, $alias = null, $schema = null) {
			$this->name = $name;
			$this->alias = $alias;
			$this->schema = $schema;
			$this->select = new SelectManager($this);
		}

		/**
		 * @return null
		 */
		public function getAlias() {
			return $this->alias;
		}

		/**
		 * @param $alias
		 *
		 * @return $this
		 */
		public function setAlias($alias) {
			$this->alias = $alias;

			return $this;
		}

		/**
		 * @return string
		 */
		public function getName() {
			return $this->name;
		}

		/**
		 * @return null
		 */
		public function getSchema() {
			return $this->schema;
		}

		/**
		 * @return SelectManager
		 */
		public function from() {
			return $this->select;
		}

		/**
		 * @return AllColumnsNode
		 */
		public function all() {
			return new AllColumnsNode($this);
		}

		/**
		 * @param mixed $relation
		 * @param string $klass
		 *
		 * @return SelectManager
		 */
		public function join($relation, $klass = InnerJoinNode::class) {
			if (!$relation)
				return $this->from();

			if (is_string($relation) && strlen($relation) === 0 ||
				$relation instanceof SqlLiteral && $relation->isEmpty()
			)
				throw new \InvalidArgumentException('$relation cannot be empty');
			else if (is_string($relation) || $relation instanceof SqlLiteral)
				$klass = StringJoinNode::class;

			return $this->from()->join($relation, $klass);
		}

		/**
		 * @param mixed $relation
		 *
		 * @return SelectManager
		 */
		public function outerJoin($relation) {
			return $this->join($relation, OuterJoinNode::class);
		}

		/**
		 * @param array ...$columns
		 *
		 * @return SelectManager
		 */
		//public function group(... $columns) {
		//	return $this->from()->group(...$columns);
		//}

		/**
		 * @param array ...$orders
		 *
		 * @return SelectManager
		 */
		public function order(... $orders) {
			return $this->from()->order(...$orders);
		}

		/**
		 * @param mixed $condition
		 *
		 * @return SelectManager
		 */
		public function where($condition) {
			return $this->from()->where($condition);
		}

		/**
		 * @param array ...$things
		 *
		 * @return SelectManager
		 */
		public function select(... $things) {
			return $this->from()->select(...$things);
		}

		/**
		 * @param int|null $amount
		 *
		 * @return SelectManager
		 */
		public function take($amount) {
			return $this->from()->limit($amount);
		}

		/**
		 * @param int|null $amount
		 *
		 * @return SelectManager
		 */
		public function skip($amount) {
			return $this->from()->offset($amount);
		}

		/**
		 * @param mixed $expr
		 *
		 * @return SelectManager
		 */
		public function having($expr) {
			return $this->from()->having($expr);
		}

		/**
		 * @param $function
		 *
		 * @return NamedFunctionNode
		 */
		public static function call($function) {
			return new NamedFunctionNode($function, []);
		}

		/**
		 * @param $expr
		 *
		 * @return GroupingNode
		 */
		public static function group($expr) {
			return new GroupingNode($expr);
		}

		public function offsetGet($offset) {
			return new Attribute($this, $offset);
		}

		public function offsetExists($offset) {
			return true;
		}

		public function offsetSet($offset, $value) {
			throw new \BadMethodCallException('Table does not implement offsetSet');
		}

		public function offsetUnset($offset) {
			throw new \BadMethodCallException('Table does not implement offsetUnset');
		}
	}