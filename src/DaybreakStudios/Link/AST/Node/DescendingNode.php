<?php
	namespace DaybreakStudios\Link\AST\Node;

	class DescendingNode extends OrderingNode {
		public function getDirection() {
			return OrderingNode::ORDER_DESC;
		}

		/**
		 * @return AscendingNode
		 */
		public function reverse() {
			return new AscendingNode(parent::getChild());
		}

		public function isAscending() {
			return false;
		}

		public function isDescending() {
			return true;
		}
	}