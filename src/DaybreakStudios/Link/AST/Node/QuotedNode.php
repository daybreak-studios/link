<?php
	namespace DaybreakStudios\Link\AST\Node;

	class QuotedNode extends UnaryNode {
		public function isNull() {
			return parent::getChild() === null;
		}
	}