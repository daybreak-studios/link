<?php
	namespace DaybreakStudios\Link\AST\Node;

	class ValuesNode extends BinaryNode{
		/**
		 * ValuesNode constructor.
		 *
		 * @param       $exprs
		 * @param array $columns
		 */
		public function __construct($exprs, array $columns = []) {
			parent::__construct($exprs, $columns);
		}

		/**
		 * @return array
		 */
		public function getRight() {
			return parent::getRight();
		}

		/**
		 * @return mixed
		 */
		public function getExpressions() {
			return parent::getLeft();
		}

		/**
		 * @return array
		 */
		public function getColumns() {
			return $this->getRight();
		}
	}