<?php
	namespace DaybreakStudios\Link\AST\Node;

	use DaybreakStudios\Link\AST\Attribute\Attribute;

	class TableAliasNode extends BinaryNode implements \ArrayAccess {
		/**
		 * @return mixed
		 */
		public function getName() {
			return parent::getRight();
		}

		/**
		 * @return mixed
		 */
		public function getRelation() {
			return parent::getLeft();
		}

		public function offsetGet($offset) {
			return new Attribute($this, $offset);
		}

		public function offsetExists($offset) {
			return true;
		}

		public function offsetUnset($offset) {
			throw new \BadMethodCallException('TableAliasNode does not implement offsetUnset');
		}

		public function offsetSet($offset, $value) {
			throw new \BadMethodCallException('TableAliasNode does not implement offsetSet');
		}
	}