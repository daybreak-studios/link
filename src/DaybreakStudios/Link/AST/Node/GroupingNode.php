<?php
	namespace DaybreakStudios\Link\AST\Node;

	use DaybreakStudios\Link\AST\Traits\AliasPredicationTrait;
	use DaybreakStudios\Link\AST\Traits\PredicationsTrait;

	class GroupingNode extends UnaryNode {
		use PredicationsTrait,
			AliasPredicationTrait;
	}