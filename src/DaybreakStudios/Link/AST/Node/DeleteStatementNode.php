<?php
	namespace DaybreakStudios\Link\AST\Node;

	use DaybreakStudios\Link\AST\Table;

	class DeleteStatementNode extends BinaryNode {
		private $limit = null;

		/**
		 * DeleteStatementNode constructor.
		 *
		 * @param null  $relation
		 * @param array $wheres
		 */
		public function __construct($relation = null, $wheres = []) {
			parent::__construct($relation, $wheres);
		}

		/**
		 * @return Table|null
		 */
		public function getRelation() {
			return $this->getLeft();
		}

		/**
		 * @param $relation
		 *
		 * @return $this
		 */
		public function setRelation($relation) {
			$this->setLeft($relation);

			return $this;
		}

		/**
		 * @return array
		 */
		public function getWheres() {
			return $this->getRight();
		}

		/**
		 * @param array $wheres
		 *
		 * @return $this
		 */
		public function setWheres(array $wheres) {
			$this->setRight($wheres);

			return $this;
		}

		/**
		 * @param $where
		 *
		 * @return DeleteStatementNode
		 */
		public function addWhere($where) {
			$wheres = $this->getWheres();
			$wheres[] = $where;

			return $this->setWheres($wheres);
		}

		/**
		 * @return mixed|null
		 */
		public function getLimit() {
			return $this->limit;
		}

		/**
		 * @param mixed|null $limit
		 *
		 * @return $this
		 */
		public function setLimit($limit) {
			$this->limit = $limit;

			return $this;
		}
	}