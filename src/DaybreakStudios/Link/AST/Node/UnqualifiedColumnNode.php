<?php
	namespace DaybreakStudios\Link\AST\Node;

	class UnqualifiedColumnNode extends UnaryNode {
		/**
		 * @return mixed
		 */
		public function getChild() {
			return parent::getChild();
		}

		/**
		 * @return mixed
		 */
		public function getRelation() {
			return $this->getChild()->getRelation();
		}

		/**
		 * @return mixed
		 */
		public function getName() {
			return $this->getChild();
		}
	}