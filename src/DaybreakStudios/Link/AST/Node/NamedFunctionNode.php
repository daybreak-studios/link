<?php
	namespace DaybreakStudios\Link\AST\Node;

	class NamedFunctionNode extends FunctionNode {
		private $name;

		/**
		 * NamedFunctionNode constructor.
		 *
		 * @param string      $name
		 * @param array       $arguments
		 * @param string|null $alias
		 */
		public function __construct($name, array $arguments, $alias = null) {
			parent::__construct($arguments, $alias);

			$this->name = $name;
		}

		/**
		 * @return string
		 */
		public function getName() {
			return $this->name;
		}

		/**
		 * @param string $name
		 *
		 * @return $this
		 */
		public function setName($name) {
			$this->name = $name;

			return $this;
		}
	}