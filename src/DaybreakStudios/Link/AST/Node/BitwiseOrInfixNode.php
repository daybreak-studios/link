<?php
	namespace DaybreakStudios\Link\AST\Node;

	class BitwiseOrInfixNode extends InfixNode {
		public function __construct($left, $right) {
			parent::__construct('|', $left, $right);
		}
	}