<?php
	namespace DaybreakStudios\Link\AST\Node;

	class CastedNode extends Node {
		private $value;
		private $attribute;

		public function __construct($value, $attribute = null) {
			$this->value = $value;
			$this->attribute = $attribute;
		}

		public function getValue() {
			return $this->value;
		}

		public function getAttribute() {
			return $this->attribute;
		}

		public function isNull() {
			return $this->value === null;
		}
	}