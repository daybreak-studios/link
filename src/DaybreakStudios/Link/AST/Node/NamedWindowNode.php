<?php
	namespace DaybreakStudios\Link\AST\Node;

	class NamedWindowNode extends WindowNode {
		private $name;

		public function __construct($name) {
			$this->name = $name;
		}

		/**
		 * @return string
		 */
		public function getName() {
			return $this->name;
		}

		/**
		 * @param string $name
		 *
		 * @return $this
		 */
		public function setName($name) {
			$this->name = $name;

			return $this;
		}
	}