<?php
	namespace DaybreakStudios\Link\AST\Node;

	class CountFunctionNode extends FunctionNode {
		private $distinct;

		/**
		 * CountFunctionNode constructor.
		 *
		 * @param array $arguments
		 * @param bool  $distinct
		 * @param null  $alias
		 */
		public function __construct(array $arguments = [], $distinct = false, $alias = null) {
			parent::__construct($arguments, $alias);

			$this->distinct = $distinct;
		}

		/**
		 * @return bool
		 */
		public function isDistinct() {
			return $this->distinct;
		}

		/**
		 * @param bool $distinct
		 *
		 * @return $this
		 */
		public function setDistinct($distinct) {
			$this->distinct = $distinct;

			return $this;
		}
	}