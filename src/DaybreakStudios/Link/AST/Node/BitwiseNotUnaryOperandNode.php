<?php
	namespace DaybreakStudios\Link\AST\Node;

	class BitwiseNotUnaryOperandNode extends UnaryOperationNode {
		public function __construct($operand) {
			parent::__construct('~', $operand);
		}
	}