<?php
	namespace DaybreakStudios\Link\AST\Node;

	use DaybreakStudios\Link\AST\Traits\AliasPredicationTrait;
	use DaybreakStudios\Link\AST\Traits\ExpressionsTrait;
	use DaybreakStudios\Link\AST\Traits\MathTrait;
	use DaybreakStudios\Link\AST\Traits\OrderPredicationsTrait;
	use DaybreakStudios\Link\AST\Traits\PredicationsTrait;

	abstract class UnaryOperationNode extends UnaryNode {
		use ExpressionsTrait,
			PredicationsTrait,
			OrderPredicationsTrait,
			AliasPredicationTrait,
			MathTrait;

		private $operator;

		/**
		 * UnaryOperationNode constructor.
		 *
		 * @param string $operator
		 * @param        $operand
		 */
		public function __construct($operator, $operand) {
			parent::__construct($operand);

			$this->operator = $operator;
		}

		/**
		 * @return string
		 */
		public function getOperator() {
			return $this->operator;
		}
	}