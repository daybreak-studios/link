<?php
	namespace DaybreakStudios\Link\AST\Node;

	class RegexNode extends BinaryNode {
		private $caseSensitive;

		/**
		 * RegexNode constructor.
		 *
		 * @param      $left
		 * @param      $right
		 * @param bool $caseSensitive
		 */
		public function __construct($left, $right, $caseSensitive = true) {
			parent::__construct($left, $right);

			$this->caseSensitive = $caseSensitive;
		}
	}