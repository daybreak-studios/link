<?php
	namespace DaybreakStudios\Link\AST\Node;

	class MultiplicationInfixNode extends InfixNode {
		public function __construct($left, $right) {
			parent::__construct('*', $left, $right);
		}
	}