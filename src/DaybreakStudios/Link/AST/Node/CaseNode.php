<?php
	namespace DaybreakStudios\Link\AST\Node;

	use DaybreakStudios\Link\AST\Traits\AliasPredicationTrait;
	use DaybreakStudios\Link\AST\Traits\OrderPredicationsTrait;
	use DaybreakStudios\Link\AST\Traits\PredicationsTrait;

	class CaseNode extends Node {
		use OrderPredicationsTrait,
			PredicationsTrait,
			AliasPredicationTrait;

		private $case;
		private $default;

		/** @var WhenNode[] */
		private $conditions = [];

		/**
		 * CaseNode constructor.
		 *
		 * @param mixed|null $case
		 * @param mixed|null $default
		 */
		public function __construct($case = null, $default = null) {
			$this->case = $case;
			$this->default = $default;
		}

		/**
		 * @return mixed|null
		 */
		public function getCase() {
			return $this->case;
		}

		/**
		 * @return mixed|null
		 */
		public function getDefault() {
			return $this->default;
		}

		/**
		 * @return array
		 */
		public function getConditions() {
			return $this->conditions;
		}

		/**
		 * @param mixed      $condition
		 * @param mixed|null $expression
		 *
		 * @return $this
		 */
		public function when($condition, $expression = null) {
			$this->conditions[] = new WhenNode(Node::buildQuoted($condition), $expression);

			return $this;
		}

		/**
		 * @param mixed $expression
		 *
		 * @return $this
		 */
		public function then($expression) {
			$this->conditions[sizeof($this->conditions) - 1]->setRight(Node::buildQuoted($expression));

			return $this;
		}

		/**
		 * @param mixed $expression
		 *
		 * @return $this
		 */
		public function otherwise($expression) {
			$this->default = new ElseNode(Node::buildQuoted($expression));

			return $this;
		}
	}