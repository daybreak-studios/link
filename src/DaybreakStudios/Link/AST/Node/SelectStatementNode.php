<?php
	namespace DaybreakStudios\Link\AST\Node;

	class SelectStatementNode extends Node {
		private $cores;

		private $orders = [];
		private $limit = null;
		private $lock = null;
		private $offset = null;
		private $with = null;

		/**
		 * SelectStatementNode constructor.
		 *
		 * @param SelectCoreNode[] $cores
		 */
		public function __construct(array $cores = []) {
			$this->cores = !empty($cores) ? $cores : [new SelectCoreNode()];
		}

		/**
		 * @return SelectCoreNode[]
		 */
		public function getCores() {
			return $this->cores;
		}

		/**
		 * @return array
		 */
		public function getOrders() {
			return $this->orders;
		}

		/**
		 * @param array $orders
		 *
		 * @return $this
		 */
		public function setOrders(array $orders) {
			$this->orders = $orders;

			return $this;
		}

		/**
		 * @param mixed $order
		 *
		 * @return $this
		 */
		public function addOrder($order) {
			$this->orders[] = $order;

			return $this;
		}

		/**
		 * @return mixed|null
		 */
		public function getLimit() {
			return $this->limit;
		}

		/**
		 * @param mixed $limit
		 *
		 * @return $this
		 */
		public function setLimit($limit) {
			$this->limit = $limit;

			return $this;
		}

		/**
		 * @return mixed|null
		 */
		public function getLock() {
			return $this->lock;
		}

		/**
		 * @param mixed $lock
		 *
		 * @return $this
		 */
		public function setLock($lock) {
			$this->lock = $lock;

			return $this;
		}

		/**
		 * @return mixed|null
		 */
		public function getOffset() {
			return $this->offset;
		}

		/**
		 * @param mixed $offset
		 *
		 * @return $this
		 */
		public function setOffset($offset) {
			$this->offset = $offset;

			return $this;
		}

		/**
		 * @return mixed|null
		 */
		public function getWith() {
			return $this->with;
		}

		/**
		 * @param mixed $with
		 *
		 * @return $this
		 */
		public function setWith($with) {
			$this->with = $with;

			return $this;
		}
	}