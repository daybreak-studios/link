<?php
	namespace DaybreakStudios\Link\AST\Node;

	class JoinSourceNode extends BinaryNode {
		/**
		 * JoinSourceNode constructor.
		 *
		 * @param       $source
		 * @param array $op
		 */
		public function __construct($source, array $op = []) {
			parent::__construct($source, $op);
		}

		/**
		 * @return array
		 */
		public function getRight() {
			return parent::getRight();
		}

		/**
		 * @param array $right
		 *
		 * @return $this
		 */
		public function setRight($right) {
			if (!is_array($right))
				throw new \InvalidArgumentException(sprintf('%s::setRight must be an array', static::class));

			return parent::setRight($right);
		}

		/**
		 * @param $other
		 *
		 * @return JoinSourceNode
		 */
		public function addRight($other) {
			$right = $this->getRight();
			$right[] = $other;

			return $this->setRight($right);
		}

		/**
		 * @return bool
		 */
		public function isEmpty() {
			return parent::getLeft() === null && sizeof(parent::getRight()) === 0;
		}
	}