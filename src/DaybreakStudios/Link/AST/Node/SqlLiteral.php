<?php
	namespace DaybreakStudios\Link\AST\Node;

	use DaybreakStudios\Link\AST\Traits\AliasPredicationTrait;
	use DaybreakStudios\Link\AST\Traits\OrderPredicationsTrait;
	use DaybreakStudios\Link\AST\Traits\PredicationsTrait;

	class SqlLiteral {
		use PredicationsTrait,
			AliasPredicationTrait,
			OrderPredicationsTrait;

		private $value;

		/**
		 * SqlLiteral constructor.
		 *
		 * @param string $value
		 */
		public function __construct($value) {
			$this->value = $value;
		}

		/**
		 * @return string
		 */
		public function getValue() {
			return $this->value;
		}

		/**
		 * @return bool
		 */
		public function isEmpty() {
			return strlen($this->value) === 0;
		}
	}