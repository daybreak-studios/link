<?php
	namespace DaybreakStudios\Link\AST\Node;

	class ExtractNode extends UnaryNode {
		private $field;

		public function __construct($child, $field) {
			parent::__construct($child);

			$this->field = $field;
		}

		/**
		 * @return mixed
		 */
		public function getField() {
			return $this->field;
		}
	}