<?php
	namespace DaybreakStudios\Link\AST\Node;

	use DaybreakStudios\Link\Entity\Entity;

	abstract class Node implements NodeInterface {
		/**
		 * @return NotNode
		 */
		public function not() {
			return new NotNode($this);
		}

		/**
		 * @param $right
		 *
		 * @return OrNode
		 */
		public function orWhere($right) {
			return new OrNode($this, $right);
		}

		/**
		 * @param $right
		 *
		 * @return AndNode
		 */
		public function andWhere($right) {
			return new AndNode([ $this, $right ]);
		}

		/**
		 * @param mixed $other
		 * @param mixed $attribute
		 *
		 * @return QuotedNode|CastedNode
		 */
		public static function buildQuoted($other, $attribute = null) {
			if (is_bool($other))
				return $other ? new TrueNode() : new FalseNode();
			else if ($other !== null && !is_object($other))
				return new QuotedNode($other);
			else if (is_object($other)) {
				if ($other instanceof Entity)
					$other = new QuotedNode($other->getId());
				else if (method_exists($other, '__toString'))
					$other = new QuotedNode((string)$other);
			}

			return $other;
		}
	}