<?php
	namespace DaybreakStudios\Link\AST\Node;

	abstract class UnaryNode extends Node {
		private $child;

		public function __construct($child) {
			$this->child = $child;
		}

		public function getChild() {
			return $this->child;
		}

		public function setChild($child) {
			$this->child = $child;

			return $this;
		}
	}