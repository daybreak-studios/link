<?php
	namespace DaybreakStudios\Link\AST\Node;

	use DaybreakStudios\Link\AST\Table;

	class DescribeStatementNode extends Node {
		private $schema;
		private $relation;
		private $wheres;

		/**
		 * DescribeStatementNode constructor.
		 *
		 * @param null  $schema
		 * @param null  $relation
		 * @param array $wheres
		 */
		public function __construct($schema = null, $relation = null, array $wheres = []) {
			$this->schema = $schema;
			$this->relation = $relation;
			$this->wheres = $wheres;
		}

		/**
		 * @return Table|TableAliasNode
		 */
		public function getRelation() {
			return $this->relation;
		}

		/**
		 * @param Table|TableAliasNode $relation
		 *
		 * @return $this
		 */
		public function setRelation($relation) {
			$this->relation = $relation;

			return $this;
		}

		/**
		 * @return null
		 */
		public function getSchema() {
			return $this->schema;
		}

		/**
		 * @param $schema
		 *
		 * @return $this
		 */
		public function setSchema($schema) {
			$this->schema = $schema;

			return $this;
		}

		/**
		 * @return array
		 */
		public function getWheres() {
			return $this->wheres;
		}

		/**
		 * @param array $wheres
		 *
		 * @return $this
		 */
		public function setWheres(array $wheres) {
			$this->wheres = $wheres;

			return $this;
		}

		/**
		 * @param $where
		 *
		 * @return $this
		 */
		public function addWhere($where) {
			$this->wheres[] = $where;

			return $this;
		}

		/**
		 * @return bool
		 */
		public function hasWheres() {
			return sizeof($this->wheres) > 0;
		}
	}