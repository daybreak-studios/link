<?php
	namespace DaybreakStudios\Link\AST\Node;

	use DaybreakStudios\Link\AST\Traits\AliasPredicationTrait;
	use DaybreakStudios\Link\AST\Traits\ExpressionsTrait;
	use DaybreakStudios\Link\AST\Traits\MathTrait;
	use DaybreakStudios\Link\AST\Traits\OrderPredicationsTrait;
	use DaybreakStudios\Link\AST\Traits\PredicationsTrait;

	abstract class InfixNode extends BinaryNode {
		use ExpressionsTrait,
			PredicationsTrait,
			OrderPredicationsTrait,
			AliasPredicationTrait,
			MathTrait;

		private $operator;

		/**
		 * InfixNode constructor.
		 *
		 * @param string $operator
		 * @param        $left
		 * @param        $right
		 */
		public function __construct($operator, $left, $right) {
			parent::__construct($left, $right);

			$this->operator = $operator;
		}

		/**
		 * @return string
		 */
		public function getOperator() {
			return $this->operator;
		}
	}