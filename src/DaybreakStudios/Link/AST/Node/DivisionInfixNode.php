<?php
	namespace DaybreakStudios\Link\AST\Node;

	class DivisionInfixNode extends InfixNode {
		public function __construct($left, $right) {
			parent::__construct('/', $left, $right);
		}
	}