<?php
	namespace DaybreakStudios\Link\AST\Node;

	class WindowNode extends Node {
		private $framing = null;
		private $orders = [];
		private $partitions = [];

		/**
		 * @return null
		 */
		public function getFraming() {
			return $this->framing;
		}

		/**
		 * @return array
		 */
		public function getOrders() {
			return $this->orders;
		}

		/**
		 * @return bool
		 */
		public function hasOrders() {
			return sizeof($this->orders) > 0;
		}

		/**
		 * @return array
		 */
		public function getPartitions() {
			return $this->partitions;
		}

		/**
		 * @return bool
		 */
		public function hasPartitions() {
			return sizeof($this->partitions) > 0;
		}

		/**
		 * @param array ...$orders
		 *
		 * @return $this
		 */
		public function order(... $orders) {
			$this->orders = array_merge($this->orders, array_map(function($order) {
				return is_string($order) ? new SqlLiteral($order) : $order;
			}, $orders));

			return $this;
		}

		/**
		 * @param array ...$partitions
		 *
		 * @return $this
		 */
		public function partition(... $partitions) {
			$this->partitions = array_merge($this->partitions, array_map(function($partition) {
				return is_string($partition) ? new SqlLiteral($partition) : $partition;
			}, $partitions));

			return $this;
		}

		/**
		 * @param mixed $expr
		 *
		 * @return mixed
		 */
		public function frame($expr) {
			$this->framing = $expr;

			return $this->framing;
		}

		/**
		 * @param mixed|null $expr
		 *
		 * @return RowsNode|null
		 */
		public function rows($expr = null) {
			if ($this->framing !== null)
				return new RowsNode($expr);

			return $this->frame(new RowsNode($expr));
		}

		/**
		 * @param mixed|null $expr
		 *
		 * @return RangeNode|null
		 */
		public function range($expr = null) {
			if ($this->framing !== null)
				return new RangeNode($expr);

			return $this->frame(new RangeNode($expr));
		}
	}