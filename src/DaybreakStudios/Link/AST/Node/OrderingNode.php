<?php
	namespace DaybreakStudios\Link\AST\Node;

	abstract class OrderingNode extends UnaryNode {
		const ORDER_ASC = 'asc';
		const ORDER_DESC = 'desc';

		/**
		 * @return string
		 */
		public abstract function getDirection();

		/**
		 * @return OrderingNode
		 */
		public abstract function reverse();

		/**
		 * @return bool
		 */
		public abstract function isAscending();

		/**
		 * @return bool
		 */
		public abstract function isDescending();
	}