<?php
	namespace DaybreakStudios\Link\AST\Node;

	use DaybreakStudios\Link\AST\Table;

	class AllColumnsNode {
		private $relation;

		/**
		 * AllColumnsNode constructor.
		 *
		 * @param Table $relation
		 */
		public function __construct(Table $relation) {
			$this->relation = $relation;
		}

		/**
		 * @return Table
		 */
		public function getRelation() {
			return $this->relation;
		}
	}