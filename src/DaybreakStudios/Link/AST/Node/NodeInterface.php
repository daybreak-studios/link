<?php
	namespace DaybreakStudios\Link\AST\Node;

	interface NodeInterface {
		/**
		 * @return NotNode
		 */
		public function not();

		/**
		 * @param $right
		 *
		 * @return OrNode
		 */
		public function orWhere($right);

		/**
		 * @param $right
		 *
		 * @return AndNode
		 */
		public function andWhere($right);
	}