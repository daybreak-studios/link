<?php
	namespace DaybreakStudios\Link\AST\Node;

	class SelectCoreNode extends Node {
		private $source;

		private $top = null;
		private $setQuantifier = null;
		private $selects = [];
		private $wheres = [];
		private $groups = [];
		private $windows = [];
		private $havings = [];

		public function __construct() {
			$this->source = new JoinSourceNode(null);
		}

		/**
		 * @return mixed
		 */
		public function getFrom() {
			return $this->source->getLeft();
		}

		/**
		 * @param mixed $from
		 *
		 * @return $this
		 */
		public function setFrom($from) {
			$this->source->setLeft($from);

			return $this;
		}

		/**
		 * @return JoinSourceNode
		 */
		public function getSource() {
			return $this->source;
		}

		/**
		 * @param JoinSourceNode $source
		 *
		 * @return $this
		 */
		public function setSource(JoinSourceNode $source) {
			$this->source = $source;

			return $this;
		}

		/**
		 * @return mixed
		 */
		public function getTop() {
			return $this->top;
		}

		/**
		 * @param mixed $top
		 *
		 * @return $this
		 */
		public function setTop($top) {
			$this->top = $top;

			return $this;
		}

		/**
		 * @return array
		 */
		public function getSelects() {
			return $this->selects;
		}

		/**
		 * @param array $selects
		 *
		 * @return $this
		 */
		public function setSelects(array $selects) {
			$this->selects = $selects;

			return $this;
		}

		/**
		 * @param mixed $select
		 *
		 * @return $this
		 */
		public function addSelect($select) {
			$this->selects[] = $select;

			return $this;
		}

		/**
		 * @return array
		 */
		public function getWheres() {
			return $this->wheres;
		}

		/**
		 * @param array $wheres
		 *
		 * @return $this
		 */
		public function setWheres(array $wheres) {
			$this->wheres = $wheres;

			return $this;
		}

		/**
		 * @param $where
		 *
		 * @return $this
		 */
		public function addWhere($where) {
			$this->wheres[] = $where;

			return $this;
		}

		/**
		 * @return array
		 */
		public function getGroups() {
			return $this->groups;
		}

		/**
		 * @param array $groups
		 *
		 * @return $this
		 */
		public function setGroups(array $groups) {
			$this->groups = $groups;

			return $this;
		}

		/**
		 * @param mixed $group
		 *
		 * @return $this
		 */
		public function addGroup($group) {
			$this->groups[] = $group;

			return $this;
		}

		/**
		 * @return array
		 */
		public function getWindows() {
			return $this->windows;
		}

		/**
		 * @param array $windows
		 *
		 * @return $this
		 */
		public function setWindows(array $windows) {
			$this->windows = $windows;

			return $this;
		}

		/**
		 * @param mixed $window
		 *
		 * @return $this
		 */
		public function addWindow($window) {
			$this->windows[] = $window;

			return $this;
		}

		/**
		 * @return array
		 */
		public function getHavings() {
			return $this->havings;
		}

		/**
		 * @param array $havings
		 *
		 * @return $this
		 */
		public function setHavings(array $havings) {
			$this->havings = $havings;

			return $this;
		}

		/**
		 * @param mixed $having
		 *
		 * @return $this
		 */
		public function addHaving($having) {
			$this->havings[] = $having;

			return $this;
		}

		/**
		 * @return mixed
		 */
		public function getSetQuantifier() {
			return $this->setQuantifier;
		}

		/**
		 * @param mixed $setQuantifier
		 *
		 * @return $this
		 */
		public function setSetQuantifier($setQuantifier) {
			$this->setQuantifier = $setQuantifier;

			return $this;
		}
	}