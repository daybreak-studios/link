<?php
	namespace DaybreakStudios\Link\AST\Node;

	class AndNode extends Node {
		private $children;

		/**
		 * AndNode constructor.
		 *
		 * @param array $children
		 */
		public function __construct(array $children) {
			$this->children = $children;
		}

		/**
		 * @return array
		 */
		public function getChildren() {
			return $this->children;
		}
	}