<?php
	namespace DaybreakStudios\Link\AST\Node;

	use DaybreakStudios\Link\AST\Traits\OrderPredicationsTrait;
	use DaybreakStudios\Link\AST\Traits\PredicationsTrait;
	use DaybreakStudios\Link\AST\Traits\WindowPredicationsTrait;

	class FunctionNode extends Node {
		use PredicationsTrait,
			WindowPredicationsTrait,
			OrderPredicationsTrait;

		private $arguments;

		private $alias = null;
		private $distinct = false;

		public function __construct(array $arguments = [], $alias = null) {
			$this->arguments = $arguments;

			if ($alias !== null)
				$this->alias = new SqlLiteral($alias);
		}

		/**
		 * @return array
		 */
		public function getArguments() {
			return $this->arguments;
		}

		/**
		 * @param array $arguments
		 *
		 * @return $this
		 */
		public function setArguments(array $arguments) {
			$this->arguments = $arguments;

			return $this;
		}

		/**
		 * @param array ...$args
		 *
		 * @return $this
		 */
		public function with(... $args) {
			foreach ($args as $arg)
				$this->addArgument(Node::buildQuoted($arg));

			return $this;
		}

		/**
		 * @param mixed $arg
		 *
		 * @return $this
		 */
		public function addArgument($arg) {
			$this->arguments[] = $arg;

			return $this;
		}

		/**
		 * @return SqlLiteral|null
		 */
		public function getAlias() {
			return $this->alias;
		}

		/**
		 * @param string $alias
		 *
		 * @return $this
		 */
		public function named($alias) {
			$this->alias = new SqlLiteral($alias);

			return $this;
		}

		/**
		 * @return bool
		 */
		public function isDistinct() {
			return $this->distinct;
		}

		/**
		 * @param bool $distinct
		 *
		 * @return $this
		 */
		public function setDistinct($distinct) {
			$this->distinct = $distinct;

			return $this;
		}
	}