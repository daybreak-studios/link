<?php
	namespace DaybreakStudios\Link\AST\Node;

	class FollowingNode extends UnaryNode {
		public function __construct($child = null) {
			parent::__construct($child);
		}
	}