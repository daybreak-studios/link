<?php
	namespace DaybreakStudios\Link\AST\Node;

	class UpdateStatementNode extends Node {
		private $key = null;
		private $limit = null;
		private $relation = null;
		private $wheres = [];
		private $values = [];
		private $orders = [];

		/**
		 * @return mixed|null
		 */
		public function getKey() {
			return $this->key;
		}

		/**
		 * @param mixed $key
		 *
		 * @return $this
		 */
		public function setKey($key) {
			$this->key = $key;

			return $this;
		}

		/**
		 * @return mixed|null
		 */
		public function getLimit() {
			return $this->limit;
		}

		/**
		 * @param mixed $limit
		 *
		 * @return $this
		 */
		public function setLimit($limit) {
			$this->limit = $limit;

			return $this;
		}

		/**
		 * @return mixed|null
		 */
		public function getRelation() {
			return $this->relation;
		}

		/**
		 * @param mixed $relation
		 *
		 * @return $this
		 */
		public function setRelation($relation) {
			$this->relation = $relation;

			return $this;
		}

		/**
		 * @return array
		 */
		public function getWheres() {
			return $this->wheres;
		}

		/**
		 * @param array $wheres
		 *
		 * @return $this
		 */
		public function setWheres(array $wheres) {
			$this->wheres = $wheres;

			return $this;
		}

		/**
		 * @param mixed $where
		 *
		 * @return $this
		 */
		public function addWhere($where) {
			$this->wheres[] = $where;

			return $this;
		}

		/**
		 * @return array
		 */
		public function getOrders() {
			return $this->orders;
		}

		/**
		 * @param array $orders
		 *
		 * @return $this
		 */
		public function setOrders(array $orders) {
			$this->orders = $orders;

			return $this;
		}

		/**
		 * @param mixed $order
		 *
		 * @return $this
		 */
		public function addOrder($order) {
			$this->orders[] = $order;

			return $this;
		}

		/**
		 * @return array
		 */
		public function getValues() {
			return $this->values;
		}

		/**
		 * @param array $values
		 *
		 * @return $this
		 */
		public function setValues(array $values) {
			$this->values = $values;

			return $this;
		}

		/**
		 * @param mixed $value
		 *
		 * @return $this
		 */
		public function addValue($value) {
			$this->values[] = $value;

			return $this;
		}
	}