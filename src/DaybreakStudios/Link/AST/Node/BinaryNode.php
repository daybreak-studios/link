<?php
	namespace DaybreakStudios\Link\AST\Node;

	class BinaryNode extends Node {
		private $left;
		private $right;

		public function __construct($left, $right) {
			$this->left = $left;
			$this->right = $right;
		}

		public function getLeft() {
			return $this->left;
		}

		public function setLeft($left) {
			$this->left = $left;

			return $this;
		}

		public function getRight() {
			return $this->right;
		}

		public function setRight($right) {
			$this->right = $right;

			return $this;
		}
	}