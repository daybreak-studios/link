<?php
	namespace DaybreakStudios\Link\AST\Node;

	use DaybreakStudios\Link\AST\Attribute\Attribute;
	use DaybreakStudios\Link\AST\Table;

	class InsertStatementNode extends Node {
		private $relation = null;
		private $columns = [];
		private $values = null;
		private $select = null;

		/**
		 * @return Table|null
		 */
		public function getRelation() {
			return $this->relation;
		}

		/**
		 * @param Table|null $relation
		 *
		 * @return $this
		 */
		public function setRelation($relation) {
			$this->relation = $relation;

			return $this;
		}

		/**
		 * @return Attribute[]
		 */
		public function getColumns() {
			return $this->columns;
		}

		/**
		 * @param Attribute[] $columns
		 *
		 * @return $this
		 */
		public function setColumns(array $columns) {
			$this->columns = $columns;

			return $this;
		}

		/**
		 * @param Attribute|UnqualifiedColumnNode $column
		 *
		 * @return $this
		 */
		public function addColumn($column) {
			$this->columns[] = $column;

			return $this;
		}

		/**
		 * @return mixed
		 */
		public function getValues() {
			return $this->values;
		}

		/**
		 * @param mixed $values
		 *
		 * @return $this
		 */
		public function setValues($values) {
			$this->values = $values;

			return $this;
		}

		/**
		 * @return mixed|null
		 */
		public function getSelect() {
			return $this->select;
		}

		/**
		 * @param mixed $select
		 *
		 * @return $this
		 */
		public function setSelect($select) {
			$this->select = $select;

			return $this;
		}
	}