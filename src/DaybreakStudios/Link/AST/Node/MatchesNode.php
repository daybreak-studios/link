<?php
	namespace DaybreakStudios\Link\AST\Node;

	class MatchesNode extends BinaryNode {
		private $caseSensitive;

		private $escape = null;

		/**
		 * MatchesNode constructor.
		 *
		 * @param             $left
		 * @param             $right
		 * @param string|null $escape
		 * @param bool        $caseSensitive
		 */
		public function __construct($left, $right, $escape = null, $caseSensitive = false) {
			parent::__construct($left, $right);

			$this->caseSensitive = $caseSensitive;

			if ($escape !== null)
				$this->escape = Node::buildQuoted($escape);
		}

		/**
		 * @return string|null
		 */
		public function getEscape() {
			return $this->escape;
		}

		/**
		 * @return bool
		 */
		public function isCaseSensitive() {
			return $this->caseSensitive;
		}

		/**
		 * @param bool $caseSensitive
		 *
		 * @return $this
		 */
		public function setCaseSensitive($caseSensitive) {
			$this->caseSensitive = $caseSensitive;

			return $this;
		}
	}