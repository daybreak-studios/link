<?php
	namespace DaybreakStudios\Link\AST\Node;

	class AscendingNode extends OrderingNode {
		public function getDirection() {
			return OrderingNode::ORDER_ASC;
		}

		/**
		 * @return DescendingNode
		 */
		public function reverse() {
			return new DescendingNode(parent::getChild());
		}

		public function isAscending() {
			return true;
		}

		public function isDescending() {
			return false;
		}
	}