<?php
	namespace DaybreakStudios\Link\AST\Node;

	class StringJoinNode extends JoinNode {
		public function __construct($left, $right = null) {
			parent::__construct($left, $right);
		}
	}