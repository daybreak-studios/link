<?php
	namespace DaybreakStudios\Link\AST\Node;

	use DaybreakStudios\Link\AST\Traits\AliasPredicationTrait;

	class OverNode extends BinaryNode {
		use AliasPredicationTrait;

		public function __construct($left, $right = null) {
			parent::__construct($left, $right);
		}

		public function getOperator() {
			return 'OVER';
		}
	}