<?php
	namespace DaybreakStudios\Link\AST\Attribute;

	use DaybreakStudios\Link\AST\Node\TableAliasNode;
	use DaybreakStudios\Link\AST\Table;
	use DaybreakStudios\Link\AST\Traits\AliasPredicationTrait;
	use DaybreakStudios\Link\AST\Traits\ExpressionsTrait;
	use DaybreakStudios\Link\AST\Traits\MathTrait;
	use DaybreakStudios\Link\AST\Traits\OrderPredicationsTrait;
	use DaybreakStudios\Link\AST\Traits\PredicationsTrait;

	class Attribute {
		use ExpressionsTrait,
			PredicationsTrait,
			AliasPredicationTrait,
			OrderPredicationsTrait,
			MathTrait;

		private $name;
		private $relation;

		/**
		 * Attribute constructor.
		 *
		 * @param        $relation
		 * @param string $name
		 */
		public function __construct($relation, $name) {
			$this->relation = $relation;
			$this->name = $name;
		}

		/**
		 * @return TableAliasNode|Table
		 */
		public function getRelation() {
			return $this->relation;
		}

		/**
		 * @param mixed $relation
		 *
		 * @return $this
		 */
		public function setRelation($relation) {
			$this->relation = $relation;

			return $this;
		}

		/**
		 * @return string
		 */
		public function getName() {
			return $this->name;
		}

		/**
		 * @param string $name
		 *
		 * @return $this
		 */
		public function setName($name) {
			$this->name = $name;

			return $this;
		}
	}