<?php
	namespace DaybreakStudios\Link\AST\Visitor;

	use DaybreakStudios\Link\AST\Attribute\Attribute;
	use DaybreakStudios\Link\AST\Collector\Collector;
	use DaybreakStudios\Link\AST\Collector\SqlStringCollector;
	use DaybreakStudios\Link\AST\Node\AllColumnsNode;
	use DaybreakStudios\Link\AST\Node\AndNode;
	use DaybreakStudios\Link\AST\Node\AscendingNode;
	use DaybreakStudios\Link\AST\Node\AsNode;
	use DaybreakStudios\Link\AST\Node\AssignmentNode;
	use DaybreakStudios\Link\AST\Node\AvgFunctionNode;
	use DaybreakStudios\Link\AST\Node\BetweenNode;
	use DaybreakStudios\Link\AST\Node\BinaryNode;
	use DaybreakStudios\Link\AST\Node\BindParamNode;
	use DaybreakStudios\Link\AST\Node\BinNode;
	use DaybreakStudios\Link\AST\Node\CaseNode;
	use DaybreakStudios\Link\AST\Node\CountFunctionNode;
	use DaybreakStudios\Link\AST\Node\CurrentRowNode;
	use DaybreakStudios\Link\AST\Node\DeleteStatementNode;
	use DaybreakStudios\Link\AST\Node\DescendingNode;
	use DaybreakStudios\Link\AST\Node\DescribeStatementNode;
	use DaybreakStudios\Link\AST\Node\DistinctNode;
	use DaybreakStudios\Link\AST\Node\DistinctOnNode;
	use DaybreakStudios\Link\AST\Node\DoesNotMatchNode;
	use DaybreakStudios\Link\AST\Node\ElseNode;
	use DaybreakStudios\Link\AST\Node\EqualityNode;
	use DaybreakStudios\Link\AST\Node\ExceptNode;
	use DaybreakStudios\Link\AST\Node\ExistsFunctionNode;
	use DaybreakStudios\Link\AST\Node\ExtractNode;
	use DaybreakStudios\Link\AST\Node\FalseNode;
	use DaybreakStudios\Link\AST\Node\FollowingNode;
	use DaybreakStudios\Link\AST\Node\FullOuterJoinNode;
	use DaybreakStudios\Link\AST\Node\FunctionNode;
	use DaybreakStudios\Link\AST\Node\GreaterThanNode;
	use DaybreakStudios\Link\AST\Node\GreaterThanOrEqualNode;
	use DaybreakStudios\Link\AST\Node\GroupingNode;
	use DaybreakStudios\Link\AST\Node\GroupNode;
	use DaybreakStudios\Link\AST\Node\InfixNode;
	use DaybreakStudios\Link\AST\Node\InnerJoinNode;
	use DaybreakStudios\Link\AST\Node\InNode;
	use DaybreakStudios\Link\AST\Node\InsertStatementNode;
	use DaybreakStudios\Link\AST\Node\IntersectNode;
	use DaybreakStudios\Link\AST\Node\JoinSourceNode;
	use DaybreakStudios\Link\AST\Node\LessThanNode;
	use DaybreakStudios\Link\AST\Node\LessThanOrEqualNode;
	use DaybreakStudios\Link\AST\Node\LimitNode;
	use DaybreakStudios\Link\AST\Node\LockNode;
	use DaybreakStudios\Link\AST\Node\MatchesNode;
	use DaybreakStudios\Link\AST\Node\MaxFunctionNode;
	use DaybreakStudios\Link\AST\Node\MinFunctionNode;
	use DaybreakStudios\Link\AST\Node\NamedFunctionNode;
	use DaybreakStudios\Link\AST\Node\NamedWindowNode;
	use DaybreakStudios\Link\AST\Node\NotEqualNode;
	use DaybreakStudios\Link\AST\Node\NotInNode;
	use DaybreakStudios\Link\AST\Node\NotRegexNode;
	use DaybreakStudios\Link\AST\Node\OffsetNode;
	use DaybreakStudios\Link\AST\Node\OnNode;
	use DaybreakStudios\Link\AST\Node\OrNode;
	use DaybreakStudios\Link\AST\Node\OuterJoinNode;
	use DaybreakStudios\Link\AST\Node\OverNode;
	use DaybreakStudios\Link\AST\Node\PrecedingNode;
	use DaybreakStudios\Link\AST\Node\QuotedNode;
	use DaybreakStudios\Link\AST\Node\RangeNode;
	use DaybreakStudios\Link\AST\Node\RegexNode;
	use DaybreakStudios\Link\AST\Node\RightOuterJoinNode;
	use DaybreakStudios\Link\AST\Node\RowsNode;
	use DaybreakStudios\Link\AST\Node\SelectCoreNode;
	use DaybreakStudios\Link\AST\Node\SelectStatementNode;
	use DaybreakStudios\Link\AST\Node\SqlLiteral;
	use DaybreakStudios\Link\AST\Node\StringJoinNode;
	use DaybreakStudios\Link\AST\Node\SumFunctionNode;
	use DaybreakStudios\Link\AST\Node\TableAliasNode;
	use DaybreakStudios\Link\AST\Node\TopNode;
	use DaybreakStudios\Link\AST\Node\TrueNode;
	use DaybreakStudios\Link\AST\Node\UnionAllNode;
	use DaybreakStudios\Link\AST\Node\UnionNode;
	use DaybreakStudios\Link\AST\Node\UnqualifiedColumnNode;
	use DaybreakStudios\Link\AST\Node\UpdateStatementNode;
	use DaybreakStudios\Link\AST\Node\ValuesNode;
	use DaybreakStudios\Link\AST\Node\WhenNode;
	use DaybreakStudios\Link\AST\Node\WindowNode;
	use DaybreakStudios\Link\AST\Node\WithNode;
	use DaybreakStudios\Link\AST\Node\WithRecursiveNode;
	use DaybreakStudios\Link\AST\SelectManager;
	use DaybreakStudios\Link\AST\Table;
	use DaybreakStudios\Link\Connection\Adapter\AdapterInterface;

	class ToSqlVisitor extends ReduceVisitor {
		private $adapter;

		public function __construct(AdapterInterface $adapter) {
			parent::__construct();

			$this->adapter = $adapter;
		}

		public function visitDescribeStatementNode(DescribeStatementNode $node, Collector $collector) {
			throw $this->createNotSupportedException(DescribeStatementNode::class);
		}

		public function visitDeleteStatementNode(DeleteStatementNode $node, Collector $collector) {
			$collector->append('DELETE FROM ');
			parent::visit($node->getRelation(), $collector);

			if (sizeof($node->getWheres())) {
				$collector->append(' WHERE ');

				$this->injectJoin($node->getWheres(), $collector, ' AND ');
			}

			return $this->maybeVisit($node->getLimit(), $collector);
		}

		public function visitUpdateStatementNode(UpdateStatementNode $node, Collector $collector) {
			$wheres = null;

			if (sizeof($node->getOrders()) === 0 && $node->getLimit() === null)
				$wheres = $node->getWheres();
			else
				$wheres = [new InNode($node->getKey(), [$this->buildSubselect($node->getKey(), $node)])];

			$collector->append('UPDATE ');
			parent::visit($node->getRelation(), $collector);

			if (sizeof($node->getValues())) {
				$collector->append(' SET ');

				$this->injectJoin($node->getValues(), $collector, ', ');
			}

			if (sizeof($wheres)) {
				$collector->append(' WHERE ');

				$this->injectJoin($wheres, $collector, ' AND ');
			}

			return $collector;
		}

		public function visitInsertStatementNode(InsertStatementNode $node, Collector $collector) {
			$collector->append('INSERT INTO ');
			parent::visit($node->getRelation(), $collector);

			if (sizeof($node->getColumns())) {
				$collector->append(' (');

				$last = sizeof($node->getColumns()) - 1;

				foreach ($node->getColumns() as $i => $column) {
					parent::visit($column, $collector);

					if ($i !== $last)
						$collector->append(', ');
				}

				$collector->append(')');
			}

			if ($node->getValues())
				$this->maybeVisit($node->getValues(), $collector);
			else if ($node->getSelect())
				$this->maybeVisit($node->getSelect(), $collector);

			return $collector;
		}

		public function visitSelectStatmentNode(SelectStatementNode $node, Collector $collector) {
			if ($node->getWith())
				parent::visit($node->getWith(), $collector)->append(' ');

			foreach ($node->getCores() as $core)
				$this->visitSelectCoreNode($core, $collector);

			if (sizeof($node->getOrders())) {
				$collector->append(' ORDER BY ');

				$last = sizeof($node->getOrders()) - 1;

				foreach ($node->getOrders() as $i => $order) {
					parent::visit($order, $collector);

					if ($i !== $last)
						$collector->append(', ');
				}
			}

			$this->visitSelectOptions($node, $collector);

			return $collector;
		}

		protected function visitSelectOptions(SelectStatementNode $node, Collector $collector) {
			$this->maybeVisit($node->getLimit(), $collector);
			$this->maybeVisit($node->getOffset(), $collector);
			$this->maybeVisit($node->getLock(), $collector);

			return $collector;
		}

		public function visitSelectCoreNode(SelectCoreNode $node, Collector $collector) {
			$collector->append('SELECT');

			$this->maybeVisit($node->getTop(), $collector);
			$this->maybeVisit($node->getSetQuantifier(), $collector);

			if (sizeof($node->getSelects())) {
				$collector->append(' ');

				$last = sizeof($node->getSelects()) - 1;

				foreach ($node->getSelects() as $i => $projection) {
					parent::visit($projection, $collector);

					if ($i !== $last)
						$collector->append(', ');
				}
			}

			if ($node->getSource() && !$node->getSource()->isEmpty()) {
				$collector->append(' FROM ');

				parent::visit($node->getSource(), $collector);
			}

			if (sizeof($node->getWheres())) {
				$collector->append(' WHERE ');

				$last = sizeof($node->getWheres()) - 1;

				foreach ($node->getWheres() as $i => $where) {
					parent::visit($where, $collector);

					if ($i !== $last)
						$collector->append(' AND ');
				}
			}

			if (sizeof($node->getGroups())) {
				$collector->append(' GROUP BY ');

				$last = sizeof($node->getGroups()) - 1;

				foreach ($node->getGroups() as $i => $group) {
					parent::visit($group, $collector);

					if ($i !== $last)
						$collector->append(', ');
				}
			}

			if (sizeof($node->getHavings())) {
				$collector->append(' HAVING ');

				$this->injectJoin($node->getHavings(), $collector, ' AND ');
			}

			if (sizeof($node->getWindows())) {
				$collector->append(' WINDOW ');

				$last = sizeof($node->getWindows()) - 1;

				foreach ($node->getWindows() as $i => $window) {
					parent::visit($window, $collector);

					if ($i !== $last)
						$collector->append(', ');
				}
			}

			return $collector;
		}

		public function visitAllColumnsNode(AllColumnsNode $node, Collector $collector) {
			return $collector
				->append($this->quoteTableName($node->getRelation()->getAlias() ?: $node->getRelation()->getName()))
				->append('.*');
		}

		public function visitValuesNode(ValuesNode $node, Collector $collector) {
			$collector->append(' VALUES (');

			$last = sizeof($node->getExpressions()) - 1;

			foreach ($node->getExpressions() as $i => $value) {
				parent::visit($value, $collector);

				if ($i !== $last)
					$collector->append(', ');
			}

			return $collector->append(')');
		}

		public function visitAssignementNode(AssignmentNode $node, Collector $collector) {
			parent::visit($node->getLeft(), $collector)->append(' = ');

			$right = $node->getRight();

			if (!(
				$right instanceof UnqualifiedColumnNode ||
				$right instanceof Attribute ||
				$right instanceof BindParamNode
			))
				$right = new QuotedNode($right);

			return parent::visit($right, $collector);
		}

		public function visitUnqualifiedColumnNode(UnqualifiedColumnNode $node, Collector $collector) {
			return $collector->append($this->quoteColumnName($node->getName()));
		}

		public function visitJoinSourceNode(JoinSourceNode $node, Collector $collector) {
			if ($node->getLeft())
				parent::visit($node->getLeft(), $collector);

			if (sizeof($node->getRight())) {
				if ($node->getLeft())
					$collector->append(' ');

				$this->injectJoin($node->getRight(), $collector, ' ');
			}

			return $collector;
		}

		public function visitInnerJoinNode(InnerJoinNode $node, Collector $collector) {
			$collector->append('INNER JOIN ');

			parent::visit($node->getLeft(), $collector);

			if ($node->getRight()) {
				$collector->append(' ');

				parent::visit($node->getRight(), $collector);
			}

			return $collector;
		}

		public function visitOuterJoinNode(OuterJoinNode $node, Collector $collector) {
			$collector->append('LEFT OUTER JOIN ');

			parent::visit($node->getLeft(), $collector);

			if ($node->getRight()) {
				$collector->append(' ');

				parent::visit($node->getRight(), $collector);
			}

			return $collector;
		}

		public function visitOnNode(OnNode $node, Collector $collector) {
			$collector->append('ON ');

			return parent::visit($node->getChild(), $collector);
		}

		public function visitOrNode(OrNode $node, Collector $collector) {
			parent::visit($node->getLeft(), $collector)->append(' OR ');
			parent::visit($node->getRight(), $collector);

			return $collector;
		}

		public function visitAndNode(AndNode $node, Collector $collector) {
			$this->injectJoin($node->getChildren(), $collector, ' AND ');

			return $collector;
		}

		public function visitSqlLiteral(SqlLiteral $literal, Collector $collector) {
			return $collector->append($literal->getValue());
		}

		public function visitTable(Table $table, Collector $collector) {
			if ($table->getSchema())
				$collector
					->append($this->quoteTableName($table->getSchema()))
					->append('.');

			if ($table->getAlias())
				return $collector
					->append($this->quoteTableName($table->getName()))
					->append(' ')
					->append($this->quoteTableName($table->getAlias()));

			return $collector->append($this->quoteTableName($table->getName()));
		}

		public function visitGroupingNode(GroupingNode $node, Collector $collector) {
			if ($node->getChild() instanceof GroupingNode)
				parent::visit($node->getChild(), $collector);
			else {
				$collector->append('(');

				parent::visit($node->getChild(), $collector)->append(')');
			}

			return $collector;
		}

		public function visitInfixNode(InfixNode $node, Collector $collector) {
			parent::visit($node->getLeft(), $collector)
				->append(' ')
				->append($node->getOperator())
				->append(' ');

			return parent::visit($node->getRight(), $collector);
		}

		public function visitAttribute(Attribute $attribute, Collector $collector) {
			$relation = $attribute->getRelation();
			$name = null;

			if ($relation instanceof TableAliasNode)
				$name = $relation->getAlias();
			else if ($relation instanceof Table) {
				if ($relation->getAlias())
					$name = $relation->getAlias();
				else
					$name = $relation->getName();
			}

			return $collector
				->append($this->quoteTableName($name))
				->append('.')
				->append($this->quoteColumnName($attribute->getName()));
		}

		public function visitAsNode(AsNode $node, Collector $collector) {
			parent::visit($node->getLeft(), $collector)->append(' AS ');

			return parent::visit($node->getRight(), $collector);
		}

		public function visitString($string, Collector $collector) {
			return $collector->append($this->quote($string));
		}

		public function visitInteger($integer, Collector $collector) {
			return $collector->append((string)$integer);
		}

		public function visitTopNode(TopNode $node, Collector $collector) {
			return $collector;
		}

		public function visitLimitNode(LimitNode $node, Collector $collector) {
			$collector->append('LIMIT ');

			return parent::visit($node->getChild(), $collector);
		}

		public function visitOffsetNode(OffsetNode $node, Collector $collector) {
			$collector->append('OFFSET ');

			return parent::visit($node->getChild(), $collector);
		}

		public function visitExistsFunctionNode(ExistsFunctionNode $node, Collector $collector) {
			$collector->append('EXISTS (');
			parent::visit($node->getArguments(), $collector)->append(')');

			if ($node->getAlias()) {
				$collector->append(' AS ');

				parent::visit($node->getAlias(), $collector);
			}

			return $collector;
		}

		public function visitMatchesNode(MatchesNode $node, Collector $collector) {
			parent::visit($node->getLeft(), $collector)->append(' LIKE ');
			parent::visit($node->getRight(), $collector);

			if ($node->getEscape()) {
				$collector->append(' ESCAPE ');

				parent::visit($node->getEscape(), $collector);
			}

			return $collector;
		}

		public function visitQuotedNode(QuotedNode $node, Collector $collector) {
			if ($node->isNull())
				return $collector->append('NULL');
			else if (!is_scalar($node->getChild()))
				return $collector;

			return $collector->append($this->quote($node->getChild()));
		}

		public function visitTrueNode(TrueNode $node, Collector $collector) {
			return $collector->append('TRUE');
		}

		public function visitFalseNode(FalseNode $node, Collector $collector) {
			return $collector->append('FALSE');
		}

		public function visitBinNode(BinNode $node, Collector $collector) {
			return parent::visit($node->getChild(), $collector);
		}

		public function visitDistinctNode(DistinctNode $node, Collector $collector) {
			return $collector->append('DISTINCT');
		}

		public function visitDistinctOnNode(DistinctOnNode $node, Collector $collector) {
			throw new \BadMethodCallException('ToSqlVisitor cannot handle DistinctOnNode');
		}

		public function visitWithNode(WithNode $node, Collector $collector) {
			$collector->append('WITH ');

			$this->injectJoin($node->getChild(), $collector, ', ');

			return $collector;
		}

		public function visitWithRecursiveNode(WithRecursiveNode $node, Collector $collector) {
			return $this->visitWithNode($node, $collector);
		}

		public function visitUnionNode(UnionNode $node, Collector $collector) {
			return $this->visitNodeWithInfixedValue($node, $collector, ' UNION ');
		}

		public function visitUnionAllNode(UnionAllNode $node, Collector $collector) {
			return $this->visitNodeWithInfixedValue($node, $collector, ' UNION ALL ');
		}

		public function visitIntersectNode(IntersectNode $node, Collector $collector) {
			return $this->visitNodeWithInfixedValue($node, $collector, ' INTERSECT ');
		}

		public function visitExceptNode(ExceptNode $node, Collector $collector) {
			return $this->visitNodeWithInfixedValue($node, $collector, ' EXCEPT ');
		}

		public function visitWindowNode(WindowNode $node, Collector $collector) {
			$collector->append('(');

			if ($node->hasPartitions()) {
				$collector->append('PARTITION BY ');

				$this->injectJoin($node->getPartitions(), $collector, ', ');
			}

			if ($node->hasOrders()) {
				if ($node->hasPartitions())
					$collector->append(' ');

				$collector->append('ORDER BY ');

				$this->injectJoin($node->getOrders(), $collector, ', ');
			}

			if ($node->getFraming()) {
				if ($node->hasPartitions() || $node->hasOrders())
					$collector->append(' ');

				parent::visit($node->getFraming(), $collector);
			}

			return $collector->append(')');
		}

		public function visitNamedWindowNode(NamedWindowNode $node, Collector $collector) {
			$collector
				->append($this->quoteColumnName($node->getName()))
				->append(' AS ');

			return $this->visitWindowNode($node, $collector);
		}

		public function visitRowsNode(RowsNode $node, Collector $collector) {
			if ($node->getChild()) {
				$collector->append('ROWS ');

				parent::visit($node->getChild(), $collector);
			} else
				$collector->append('ROWS');

			return $collector;
		}

		public function visitRangeNode(RangeNode $node, Collector $collector) {
			if ($node->getChild()) {
				$collector->append('RANGE ');

				parent::visit($node->getChild(), $collector);
			} else
				$collector->append('RANGE');

			return $collector;
		}

		public function visitPrecedingNode(PrecedingNode $node, Collector $collector) {
			if ($node->getChild())
				parent::visit($node->getChild(), $collector);
			else
				$collector->append('UNBOUNDED');

			return $collector->append(' PRECEDING');
		}

		public function visitFollowingNode(FollowingNode $node, Collector $collector) {
			if ($node->getChild())
				parent::visit($node->getChild(), $collector);
			else
				$collector->append('UNBOUNDED');

			return $collector->append(' FOLLOWING');
		}

		public function visitCurrentRowNode(CurrentRowNode $node, Collector $collector) {
			return $collector->append('CURRENT ROW');
		}

		public function visitOverNode(OverNode $node, Collector $collector) {
			if ($node->getRight() === null)
				parent::visit($node->getLeft(), $collector)->append(' OVER ()');
			else if (is_string($node->getRight()))
				parent::visit($node->getLeft(), $collector)
					->append(' OVER ')
					->append($this->quoteColumnName($node->getRight()));
			else
				$this->infixValue($node, $collector, ' OVER ');

			return $collector;
		}

		public function visitLockNode(LockNode $node, Collector $collector) {
			return parent::visit($node->getChild(), $collector);
		}

		public function visitSelectManager(SelectManager $node, Collector $collector) {
			return $collector
				->append('(')
				->append(trim($node->getSql()))
				->append(')');
		}

		public function visitAscendingNode(AscendingNode $node, Collector $collector) {
			return parent::visit($node->getChild(), $collector)->append(' ASC');
		}

		public function visitDescendingNode(DescendingNode $node, Collector $collector) {
			return parent::visit($node->getChild(), $collector)->append(' DESC');
		}

		public function visitGroupNode(GroupNode $node, Collector $collector) {
			return parent::visit($node->getChild(), $collector);
		}

		public function visitNamedFunctionNode(NamedFunctionNode $node, Collector $collector) {
			return $this->visitFunctionNode($node->getName(), $node, $collector);
		}

		public function visitExtractNode(ExtractNode $node, Collector $collector) {
			$collector
				->append('EXTRACT(')
				->append(strtoupper($node->getField()))
				->append(' FROM ');

			return parent::visit($node->getChild(), $collector)->append(')');
		}

		public function visitCountFunctionNode(CountFunctionNode $node, Collector $collector) {
			return $this->visitFunctionNode('COUNT', $node, $collector);
		}

		public function visitSumFunctionNode(SumFunctionNode $node, Collector $collector) {
			return $this->visitFunctionNode('SUM', $node, $collector);
		}

		public function visitMaxFunctionNode(MaxFunctionNode $node, Collector $collector) {
			return $this->visitFunctionNode('MAX', $node, $collector);
		}

		public function visitMinFunctionNode(MinFunctionNode $node, Collector $collector) {
			return $this->visitFunctionNode('MIN', $node, $collector);
		}

		public function visitAvgFunctionNode(AvgFunctionNode $node, Collector $collector) {
			return $this->visitFunctionNode('AVG', $node, $collector);
		}

		public function visitTableAliasNode(TableAliasNode $node, Collector $collector) {
			parent::visit($node->getRelation(), $collector)
				->append(' ')
				->append($this->quoteTableName($node->getName()));
		}

		public function visitBetweenNode(BetweenNode $node, Collector $collector) {
			return $this->infixValue($node, $collector, ' BETWEEN ');
		}

		public function visitLessThanNode(LessThanNode $node, Collector $collector) {
			return $this->infixValue($node, $collector, ' < ');
		}

		public function visitLessThanOrEqualNode(LessThanOrEqualNode $node, Collector $collector) {
			return $this->infixValue($node, $collector, ' <= ');
		}

		public function visitEqualityNode(EqualityNode $node, Collector $collector) {
			parent::visit($node->getLeft(), $collector);

			if ($node->getRight() === null)
				$collector->append(' IS NULL');
			else {
				$collector->append(' = ');

				parent::visit($node->getRight(), $collector);
			}

			return $collector;
		}

		public function visitNotEqualNode(NotEqualNode $node, Collector $collector) {
			if ($node->getRight() === null)
				parent::visit($node->getLeft(), $collector)->append(' IS NOT NULL');
			else
				$this->infixValue($node, $collector, ' != ');

			return $collector;
		}

		public function visitGreaterThanNode(GreaterThanNode $node, Collector $collector) {
			return $this->infixValue($node, $collector, ' > ');
		}

		public function visitGreaterThanOrEqualNode(GreaterThanOrEqualNode $node, Collector $collector) {
			return $this->infixValue($node, $collector, ' >= ');
		}

		public function visitDoesNotMatchNode(DoesNotMatchNode $node, Collector $collector) {
			$this->infixValue($node, $collector, ' NOT LIKE ');

			if ($node->getEscape()) {
				$collector->append(' ESCAPE ');

				parent::visit($node->getEscape(), $collector);
			}

			return $collector;
		}

		public function visitRegexNode(RegexNode $node, Collector $collector) {
			throw $this->createNotSupportedException(RegexNode::class);
		}

		public function visitNotRegexNode(NotRegexNode $node, Collector $collector) {
			throw $this->createNotSupportedException(NotRegexNode::class);
		}

		public function visitStringJoinNode(StringJoinNode $node, Collector $collector) {
			return parent::visit($node->getLeft(), $collector);
		}

		public function visitFullOuterJoinNode(FullOuterJoinNode $node, Collector $collector) {
			$collector->append('FULL OUTER JOIN ');

			return $this->infixValue($node, $collector, ' ');
		}

		public function visitRightOuterJoin(RightOuterJoinNode $node, Collector $collector) {
			$collector->append('RIGHT OUTER JOIN ');

			return $this->infixValue($node, $collector, ' ');
		}

		public function visitInNode(InNode $node, Collector $collector) {
			if (is_array($node->getRight()) && sizeof($node->getRight()) === 0)
				$collector->append('1=0');
			else {
				parent::visit($node->getLeft(), $collector)
					->append(' IN (');

				foreach ($node->getRight() as $i => $item) {
					if ($i > 0)
						$collector->append(', ');

					parent::visit($item, $collector);
				}

				$collector->append(')');
			}

			return $collector;
		}

		public function visitNotInNode(NotInNode $node, Collector $collector) {
			if (is_array($node->getRight()) && sizeof($node->getRight()) === 0)
				$collector->append('1=1');
			else {
				parent::visit($node->getLeft(), $collector)
					->append(' NOT IN (');

				foreach ($node->getRight() as $i => $item) {
					if ($i > 0)
						$collector->append(', ');

					parent::visit($item, $collector);
				}

				$collector->append(')');
			}

			return $collector;
		}

		public function visitCaseNode(CaseNode $node, Collector $collector) {
			$collector->append('CASE ');

			if ($node->getCase())
				parent::visit($node->getCase(), $collector)->append(' ');

			foreach ($node->getConditions() as $condition)
				parent::visit($condition, $collector)->append(' ');

			if ($node->getDefault())
				parent::visit($node->getDefault(), $collector)->append(' ');

			return $collector->append('END');
		}

		public function visitWhenNode(WhenNode $node, Collector $collector) {
			$collector->append('WHEN ');

			return $this->infixValue($node, $collector, 'THEN ');
		}

		public function visitElseNode(ElseNode $node, Collector $collector) {
			$collector->append('ELSE ');

			return parent::visit($node, $collector);
		}

		public function visitBindParamNode(BindParamNode $node, Collector $collector) {
			if (!($collector instanceof SqlStringCollector))
				throw new \BadMethodCallException(sprintf('%s cannot visit %s using the collector provided',
					static::class, get_class($collector)));

			return $collector->addBind(function() {
				return '?';
			});
		}

		public function visitDateTime(\DateTime $dt, Collector $collector) {
			return parent::visit(new QuotedNode($dt->format('Y-m-d H:i:s')), $collector);
		}

		protected function buildSubselect($key, UpdateStatementNode $node) {
			$stmt = new SelectStatementNode();

			$core = $stmt->getCores()[0];
			$core->setFrom($node->getRelation());
			$core->setWheres($node->getWheres());
			$core->setSelects([$key]);

			$stmt->setLimit($node->getLimit());
			$stmt->setOrders($node->getOrders());

			return $stmt;
		}

		protected function createNotSupportedException($klass) {
			return new \BadMethodCallException(sprintf('%s cannot handle %s', get_called_class(), $klass));
		}

		protected function visitFunctionNode($name, FunctionNode $node, Collector $collector) {
			$collector
				->append($name)
				->append('(');

			if ($node->isDistinct())
				$collector->append('DISTINCT ');

			$this->injectJoin($node->getArguments(), $collector, ', ');

			$collector->append(')');

			if ($node->getAlias()) {
				$collector->append(' AS ');

				parent::visit($node->getAlias(), $collector);
			}

			return $collector;
		}

		protected function visitNodeWithInfixedValue(BinaryNode $node, Collector $collector, $infix, $left = '(', $right = ')') {
			$collector->append($left);

			return $this->infixValue($node, $collector, $infix)->append($right);
		}

		protected function infixValue(BinaryNode $node, $collector, $value) {
			parent::visit($node->getLeft(), $collector)->append($value);

			return parent::visit($node->getRight(), $collector);
		}

		protected function injectJoin(array $list, Collector $collector, $joinStr) {
			$last = sizeof($list) - 1;

			foreach ($list as $i => $e)
				if ($i === $last)
					parent::visit($e, $collector);
				else
					parent::visit($e, $collector)->append($joinStr);

			return $collector;
		}

		protected function maybeVisit($thing, Collector $collector) {
			if (!$thing)
				return $collector;

			$collector->append(' ');

			return $this->visit($thing, $collector);
		}

		protected function quoteColumnName($name) {
			if ($name instanceof SqlLiteral)
				$name = $name->getValue();

			return sprintf('`%s`', preg_replace('/[^a-z0-9_-]/i', '', $name));
		}

		protected function quoteTableName($name) {
			return $this->quoteColumnName($name);
		}

		public function quote($value) {
			return $this->adapter->quote($value);
		}
	}