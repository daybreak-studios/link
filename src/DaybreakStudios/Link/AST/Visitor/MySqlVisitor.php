<?php
	namespace DaybreakStudios\Link\AST\Visitor;

	use DaybreakStudios\Link\AST\Collector\Collector;
	use DaybreakStudios\Link\AST\Node\ConcatInfixNode;
	use DaybreakStudios\Link\AST\Node\DeleteStatementNode;
	use DaybreakStudios\Link\AST\Node\DescribeStatementNode;
	use DaybreakStudios\Link\AST\Node\InsertStatementNode;
	use DaybreakStudios\Link\AST\Node\LimitNode;
	use DaybreakStudios\Link\AST\Node\SelectStatementNode;

	class MySqlVisitor extends ToSqlVisitor {
		public function visitSelectStatmentNode(SelectStatementNode $node, Collector $collector) {
			if ($node->getOffset() && !$node->getLimit())
				$node->setLimit(new LimitNode(PHP_INT_MAX));

			return parent::visitSelectStatmentNode($node, $collector);
		}

		public function visitInsertStatementNode(InsertStatementNode $node, Collector $collector) {
			$alias = $node->getRelation()->getAlias();
			$node->getRelation()->setAlias(null);

			parent::visitInsertStatementNode($node, $collector);

			$node->getRelation()->setAlias($alias);

			return $collector;
		}

		public function visitDeleteStatementNode(DeleteStatementNode $node, Collector $collector) {
			$collector->append('DELETE ');

			if ($node->getRelation()->getAlias())
				$collector
					->append($this->quoteTableName($node->getRelation()->getAlias()))
					->append(' ');

			$collector->append('FROM ');
			parent::visit($node->getRelation(), $collector);

			if (sizeof($node->getWheres())) {
				$collector->append(' WHERE ');

				$this->injectJoin($node->getWheres(), $collector, ' AND ');
			}

			return $this->maybeVisit($node->getLimit(), $collector);
		}

		public function visitDescribeStatementNode(DescribeStatementNode $node, Collector $collector) {
			$alias = $node->getRelation()->getAlias();
			$node->getRelation()->setAlias(null);

			$collector->append('SHOW COLUMNS FROM ');

			parent::visit($node->getRelation(), $collector);

			if ($node->hasWheres()) {
				$collector->append(' WHERE ');

				$this->injectJoin($node->getWheres(), $collector, ' AND ');
			}

			$node->getRelation()->setAlias($alias);

			return $collector;
		}

		public function visitConcatInfixNode(ConcatInfixNode $node, Collector $collector) {
			$collector->append('CONCAT(');

			parent::visit($node->getLeft(), $collector)->append(', ');
			parent::visit($node->getRight(), $collector)->append(')');

			return $collector;
		}
	}