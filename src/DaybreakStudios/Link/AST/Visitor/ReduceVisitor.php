<?php
	namespace DaybreakStudios\Link\AST\Visitor;

	use DaybreakStudios\Link\AST\Collector\Collector;

	class ReduceVisitor extends Visitor {
		public function visit($thing, Collector $collector) {
			$method = $this->getDispatchFor($thing);

			if ($method !== null)
				$method->invoke($this, $thing, $collector);

			return $collector;
		}

		/**
		 * @param $thing
		 *
		 * @return \ReflectionMethod|null
		 */
		public function getDispatchFor($thing) {
			if (is_object($thing))
				return parent::getDispatch(get_class($thing));

			return parent::getDispatch(ucfirst(gettype($thing)));
		}
	}