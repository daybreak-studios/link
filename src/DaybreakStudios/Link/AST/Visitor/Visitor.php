<?php
	namespace DaybreakStudios\Link\AST\Visitor;

	use DaybreakStudios\Link\AST\Collector\Collector;

	abstract class Visitor {
		private $dispatch;

		/**
		 * Visitor constructor.
		 */
		public function __construct() {
			$this->dispatch = self::buildDispatchCache(static::class);
		}

		/**
		 * @param string $klass
		 *
		 * @return \ReflectionMethod
		 */
		public function getDispatch($klass) {
			if (!isset($this->dispatch[$klass]) && $klass = get_parent_class($klass))
				return $this->getDispatch($klass);
			else if (!isset($this->dispatch[$klass]))
				return null;

			return $this->dispatch[$klass];
		}

		/**
		 * @param           $thing
		 * @param Collector $collector
		 *
		 * @return mixed
		 */
		abstract function visit($thing, Collector $collector);

		/**
		 * @param string $klass
		 *
		 * @return \ReflectionMethod[]
		 */
		public static function buildDispatchCache($klass) {
			$dispatch = [];
			$refl = new \ReflectionClass($klass);

			foreach ($refl->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
				if ($method->getName() === 'visit' || strpos($method->getName(), 'visit') !== 0)
					continue;

				$param = $method->getParameters()[0];

				if ($param->getClass() === null)
					continue;

				$dispatch[$param->getClass()->getName()] = $method;
			}

			return $dispatch;
		}
	}