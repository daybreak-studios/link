<?php
	namespace DaybreakStudios\Link\AST;

	use DaybreakStudios\Link\AST\Node\InsertStatementNode;
	use DaybreakStudios\Link\AST\Node\QuotedNode;
	use DaybreakStudios\Link\AST\Node\SqlLiteral;
	use DaybreakStudios\Link\AST\Node\UnqualifiedColumnNode;
	use DaybreakStudios\Link\AST\Node\ValuesNode;
	use DaybreakStudios\Link\Connection\Connection;

	class InsertManager extends TreeManager {
		/**
		 * InsertManager constructor.
		 *
		 * @param Connection|null $connection
		 */
		public function __construct(Connection $connection = null) {
			parent::__construct(new InsertStatementNode(), null, $connection);
		}

		/**
		 * @return InsertStatementNode
		 */
		public function getTree() {
			return parent::getTree();
		}

		/**
		 * @param $table
		 *
		 * @return $this
		 */
		public function into($table) {
			$this->getTree()->setRelation($table);

			return $this;
		}

		/**
		 * @return array|Attribute\Attribute[]
		 */
		public function columns() {
			return $this->getTree()->getColumns();
		}

		/**
		 * @param $select
		 *
		 * @return $this
		 */
		public function select($select) {
			$this->getTree()->setSelect($select);

			return $this;
		}

		/**
		 * @param $fields
		 *
		 * @return $this
		 */
		public function insert($fields) {
			if (is_string($fields)) {
				$this->getTree()->setValues(new SqlLiteral($fields));

				return $this;
			}

			$values = [];

			foreach ($fields as $column => $value) {
				$this->getTree()->addColumn(new UnqualifiedColumnNode($column));

				$values[] = new QuotedNode($value);
			}

			$this->getTree()->setValues(new ValuesNode($values, $this->getTree()->getColumns()));

			return $this;
		}
	}