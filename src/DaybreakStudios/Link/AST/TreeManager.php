<?php
	namespace DaybreakStudios\Link\AST;

	use DaybreakStudios\Link\AST\Collector\SqlStringCollector;
	use DaybreakStudios\Link\AST\Node\SelectCoreNode;
	use DaybreakStudios\Link\AST\Traits\FactoryMethodsTrait;
	use DaybreakStudios\Link\Connection\Connection;

	abstract class TreeManager {
		use FactoryMethodsTrait;

		private $ctx;
		private $ast;
		private $connection;

		private $binds = [];

		/**
		 * TreeManager constructor.
		 *
		 * @param                 $ast
		 * @param                 $ctx
		 * @param Connection|null $connection
		 */
		public function __construct($ast, $ctx = null, Connection $connection = null) {
			$this->ast = $ast;
			$this->ctx = $ctx;

			$this->connection = $connection ?: Connection::getConnection();
		}

		/**
		 * @return mixed|null
		 */
		public function getTree() {
			return $this->ast;
		}

		/**
		 * @return null
		 */
		public function getCore() {
			return $this->ctx;
		}

		/**
		 * @return Connection
		 */
		public function getConnection() {
			return $this->connection;
		}

		/**
		 * @return array
		 */
		public function getBinds() {
			return $this->binds;
		}

		/**
		 * @param array $binds
		 *
		 * @return array
		 */
		public function setBinds($binds) {
			$this->binds = $binds;

			return $this;
		}

		/**
		 * @param mixed $bind
		 *
		 * @return $this
		 */
		public function addBind($bind) {
			$this->binds[] = $bind;

			return $this;
		}

		/**
		 *
		 */
		public function getDot() {
			throw new \BadMethodCallException('TreeManager does not implement getDot');
		}

		/**
		 * @param Connection|null $connection
		 *
		 * @return string
		 */
		public function getSql(Connection $connection = null) {
			$connection = $connection ?: $this->connection;
			$collector = new SqlStringCollector();

			return $connection->getAdapter()->getVisitor()
				->visit($this->getTree(), $collector)
					->getValue();
		}

		/**
		 * @param mixed $expr
		 *
		 * @return $this
		 */
		public function where($expr) {
			if (!($this->ctx instanceof SelectCoreNode))
				throw new \UnexpectedValueException('TreeManager does not support where for non select statements');

			if ($expr instanceof TreeManager)
				$expr = $expr->getTree();

			$this->ctx->addWhere($expr);

			return $this;
		}
	}