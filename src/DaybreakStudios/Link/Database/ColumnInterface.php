<?php
	namespace DaybreakStudios\Link\Database;

	use DaybreakStudios\Link\Database\Type\TypeInterface;

	interface ColumnInterface {
		/**
		 * @return string
		 */
		public function getField();

		/**
		 * @return TypeInterface
		 */
		public function getType();

		/**
		 * @return bool
		 */
		public function isNullable();

		/**
		 * @return mixed
		 */
		public function getDefault();

		/**
		 * @return OptionsInterface
		 */
		public function getOptions();
	}