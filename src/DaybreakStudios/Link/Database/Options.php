<?php
	namespace DaybreakStudios\Link\Database;

	class Options implements OptionsInterface {
		const OPT_UNSIGNED = 'unsigned';
		const OPT_AI = 'auto_increment';

		private $options;

		/**
		 * Options constructor.
		 *
		 * @param array $options
		 */
		public function __construct(array $options) {
			$this->options = array_unique($options);
		}

		public function all() {
			return $this->options;
		}

		public function has($name) {
			return in_array($name, $this->options);
		}
	}