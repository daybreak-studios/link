<?php
	namespace DaybreakStudios\Link\Database;

	class UniqueConstraint extends Constraint {
		public function __construct($name, $columns) {
			parent::__construct(Constraint::TYPE_UNIQUE, $name, $columns);
		}
	}