<?php
	namespace DaybreakStudios\Link\Database;

	class PrimaryConstraint extends Constraint {
		/**
		 * PrimaryConstraint constructor.
		 *
		 * @param string $name
		 * @param array  $columns
		 */
		public function __construct($name, array $columns) {
			parent::__construct(Constraint::TYPE_PRIMARY, $name, $columns);
		}
	}