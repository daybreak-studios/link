<?php
	namespace DaybreakStudios\Link\Database\Reader;

	use DaybreakStudios\Link\Connection\Connection;
	use DaybreakStudios\Link\Database\ColumnInterface;
	use DaybreakStudios\Link\Database\ConstraintInterface;

	abstract class AbstractReader implements ReaderInterface {
		private $table;

		/** @var Connection */
		private $connection;

		/** @var ColumnInterface[] */
		private $columns = [];

		/** @var ConstraintInterface[] */
		private $constraints = [];

		/**
		 * AbstractReader constructor.
		 *
		 * @param string          $table
		 * @param Connection|null $connection
		 */
		public function __construct($table, Connection $connection = null) {
			$this->table = $table;
			$this->connection = $connection ?: Connection::getConnection();

			if ($this->connection === null)
				throw new \InvalidArgumentException('Could not establish connection to database');
		}

		/**
		 * @return Connection
		 */
		protected function getConnection() {
			return $this->connection;
		}

		/**
		 * @param ColumnInterface $column
		 *
		 * @return $this
		 */
		protected function addColumn(ColumnInterface $column) {
			$this->columns[$column->getField()] = $column;

			return $this;
		}

		/**
		 * @return ColumnInterface[]
		 */
		public function getColumns() {
			return $this->columns;
		}

		/**
		 * @param string $name
		 *
		 * @return ColumnInterface|null
		 */
		public function getColumn($name) {
			if (!isset($this->getColumns()[$name]))
				return null;

			return $this->getColumns()[$name];
		}

		/**
		 * @param string $name
		 *
		 * @return bool
		 */
		public function hasColumn($name) {
			return $this->getColumn($name) !== null;
		}

		/**
		 * @param ConstraintInterface $constraint
		 *
		 * @return $this
		 */
		protected function addConstraint(ConstraintInterface $constraint) {
			$this->constraints[] = $constraint;

			return $this;
		}

		/**
		 * @return ConstraintInterface[]
		 */
		public function getConstraints() {
			return $this->constraints;
		}

		/**
		 * @param string $name
		 *
		 * @return ConstraintInterface|null
		 */
		public function getConstraint($name) {
			if (!isset($this->getConstraints()[$name]))
				return null;

			return $this->getConstraints()[$name];
		}

		/**
		 * @param string $name
		 *
		 * @return bool
		 */
		public function hasConstraint($name) {
			return $this->getConstraint($name) !== null;
		}

		/**
		 * @return string
		 */
		public function getTableName() {
			return $this->table;
		}

		/**
		 * @return string
		 */
		public function getSchema() {
			return $this->connection->getSchema();
		}

		/**
		 *
		 */
		public function reset() {
			$this->columns = [];
			$this->constraints = [];
		}
	}