<?php
	namespace DaybreakStudios\Link\Database\Reader;

	use DaybreakStudios\Link\Database\ColumnInterface;
	use DaybreakStudios\Link\Database\ConstraintInterface;

	interface ReaderInterface {
		/**
		 * @return ColumnInterface[]
		 */
		public function getColumns();

		/**
		 * @param string $name
		 *
		 * @return ColumnInterface|null
		 */
		public function getColumn($name);

		/**
		 * @param string $name
		 *
		 * @return bool
		 */
		public function hasColumn($name);

		/**
		 * @return ConstraintInterface[]
		 */
		public function getConstraints();

		/**
		 * @param string $name
		 *
		 * @return ConstraintInterface|null
		 */
		public function getConstraint($name);

		/**
		 * @param string $name
		 *
		 * @return bool
		 */
		public function hasConstraint($name);

		/**
		 * @return string
		 */
		public function getTableName();

		/**
		 * Resets the reader, telling it to refresh the column and constraint lists next time they're retrieved.
		 */
		public function reset();
	}