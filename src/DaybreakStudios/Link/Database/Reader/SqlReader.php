<?php
	namespace DaybreakStudios\Link\Database\Reader;

	use DaybreakStudios\Link\AST\Table;
	use DaybreakStudios\Link\Connection\Connection;
	use DaybreakStudios\Link\Database\Column;
	use DaybreakStudios\Link\Database\ColumnInterface;
	use DaybreakStudios\Link\Database\Constraint;
	use DaybreakStudios\Link\Database\ConstraintInterface;
	use DaybreakStudios\Link\Database\ForeignKeyConstraint;
	use DaybreakStudios\Link\Database\Options;
	use DaybreakStudios\Link\Database\PrimaryConstraint;
	use DaybreakStudios\Link\Database\Type\Types;
	use DaybreakStudios\Link\Database\UniqueConstraint;

	class SqlReader extends AbstractReader {
		/**
		 * SqlReader constructor.
		 *
		 * @param string          $table
		 * @param Connection|null $connection
		 */
		public function __construct($table, Connection $connection = null) {
			parent::__construct($table, $connection);
		}

		/**
		 * @return ColumnInterface[]
		 */
		public function getColumns() {
			if (sizeof(parent::getColumns()))
				return parent::getColumns();

			$columns = new Table('COLUMNS', 'c', 'INFORMATION_SCHEMA');
			$sql = $columns
				->select(
					$columns['column_name']->named('field'),
					$columns['data_type']->named('type'),
					Table::call('IF')->with($columns['is_nullable']->neq('NO'), true, false)->named('is_nullable'),
					Table::call('IF')->with($columns['column_type']->matches('%unsigned%'), true, false)
						->named('is_unsigned'),
					$columns['character_maximum_length']->named('length'),
					$columns['numeric_precision']->named('_precision'),
					$columns['column_key']->named('ctype'),
					$columns['column_default']->named('_default'),
					$columns['extra']
				)
				->where($columns['table_schema']->eq(parent::getSchema()))
				->where($columns['table_name']->eq(parent::getTableName()))
				->getSql(parent::getConnection());

			$cols = parent::getConnection()->fetch($sql);

			foreach ($cols as $c) {
				$type = Types::getTypeFor($c->type, $c->length !== null ? $c->length : $c->_precision,
					$c->_precision ? $c->is_unsigned : null);

				$extra = new Options([]);

				if ($c->extra)
					$extra = new Options(explode(' ', $c->extra));

				parent::addColumn(new Column($c->field, $type, $extra, $c->is_nullable, $c->_default));
			}

			return parent::getColumns();
		}

		/**
		 * @return ConstraintInterface[]
		 */
		public function getConstraints() {
			if (sizeof(parent::getConstraints()))
				return parent::getConstraints();

			// We need column data to load constraints, so make sure it's been loaded before continuing
			$this->getColumns();

			$cons = new Table('TABLE_CONSTRAINTS', 'tc', 'INFORMATION_SCHEMA');
			$usages = new Table('KEY_COLUMN_USAGE', 'u', 'INFORMATION_SCHEMA');
			$sql = $cons
				->select(
					$cons['constraint_type']->named('ctype'),
					$usages['constraint_name']->named('cname'),
					$usages['column_name']->named('field'),
					$usages['referenced_table_schema']->named('target_schema'),
					$usages['referenced_table_name']->named('target_table'),
					$usages['referenced_column_name']->named('target_column')
				)
				->outerJoin($usages)
					->on($cons['constraint_name']->eq($usages['constraint_name']))
				->where($cons['table_schema']->eq(parent::getSchema()))
				->where($cons['table_name']->eq(parent::getTableName()))
				->where($usages['table_schema']->eq(parent::getSchema()))
				->where($usages['table_name']->eq(parent::getTableName()))
				->getSql(parent::getConnection());

			$constraints = parent::getConnection()->fetch($sql);

			foreach ($constraints as $i => $c) {
				switch ($ctype = strtok($c->ctype, ' ')) {
					case Constraint::TYPE_PRIMARY:
						parent::addConstraint(new PrimaryConstraint($c->cname, [parent::getColumn($c->field)]));

						break;

					case Constraint::TYPE_FOREIGN:
						parent::addConstraint(new ForeignKeyConstraint(
							$c->cname,
							[parent::getColumn($c->field)],
							$c->target_schema,
							$c->target_table,
							$c->target_column
						));

						break;

					case Constraint::TYPE_UNIQUE:
						if (parent::hasConstraint($c->cname))
							break;

						$columns = [parent::getColumn($c->field)];

						foreach ($constraints as $j => $con)
							if ($j !== $i && $con->cname === $c->cname)
								$columns[] = parent::getColumn($con->field);

						parent::addConstraint(new UniqueConstraint($c->cname, $columns));

						break;

					default:
						throw new \UnexpectedValueException(sprintf('Unknown constraint type named %s', $ctype));
				}
			}

			return parent::getConstraints();
		}
	}