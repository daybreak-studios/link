<?php
	namespace DaybreakStudios\Link\Database;

	use DaybreakStudios\Link\Database\Type\TypeInterface;

	class Column implements ColumnInterface {
		private $field;
		private $type;
		private $nullable;
		private $_default;
		private $options;

		/**
		 * Column constructor.
		 *
		 * @param                  $field
		 * @param TypeInterface    $type
		 * @param OptionsInterface $options
		 * @param bool             $nullable
		 * @param null             $_default
		 */
		public function __construct($field, TypeInterface $type, OptionsInterface $options, $nullable = false, $_default = null) {
			$this->field = $field;
			$this->type = $type;
			$this->options = $options;
			$this->nullable = $nullable;
			$this->_default = $_default;
		}

		public function getField() {
			return $this->field;
		}

		public function getType() {
			return $this->type;
		}

		public function isNullable() {
			return $this->nullable;
		}

		public function getDefault() {
			return $this->_default;
		}

		public function getOptions() {
			return $this->options;
		}
	}