<?php
	namespace DaybreakStudios\Link\Database;

	class Constraint implements ConstraintInterface {
		const TYPE_PRIMARY = 'PRIMARY';
		const TYPE_UNIQUE = 'UNIQUE';
		const TYPE_FOREIGN = 'FOREIGN';

		private $type;
		private $name;
		private $columns;

		/**
		 * Constraint constructor.
		 *
		 * @param string            $type
		 * @param string            $name
		 * @param ColumnInterface[] $columns
		 */
		public function __construct($type, $name, array $columns) {
			$this->type = $type;
			$this->name = $name;
			$this->columns = $columns;
		}

		public function getType() {
			return $this->type;
		}

		public function getName() {
			return $this->name;
		}

		public function getColumns() {
			return $this->columns;
		}
	}