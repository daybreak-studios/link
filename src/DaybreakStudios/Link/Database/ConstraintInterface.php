<?php
	namespace DaybreakStudios\Link\Database;

	interface ConstraintInterface {
		/**
		 * @return string
		 */
		public function getType();

		/**
		 * @return string
		 */
		public function getName();

		/**
		 * @return ColumnInterface[]
		 */
		public function getColumns();
	}