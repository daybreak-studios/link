<?php
	namespace DaybreakStudios\Link\Database;

	interface OptionsInterface {
		/**
		 * @return array
		 */
		public function all();

		/**
		 * @param string $name
		 *
		 * @return bool
		 */
		public function has($name);
	}