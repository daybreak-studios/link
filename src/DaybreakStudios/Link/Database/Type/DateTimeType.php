<?php
	namespace DaybreakStudios\Link\Database\Type;

	use DaybreakStudios\Link\Database\Options;

	class DateTimeType extends Type {
		const FORMAT = 'Y-m-d H:i:s';

		public function __construct() {
			parent::__construct('datetime', new Options([]), null);
		}

		public function castForDatabase($value) {
			if (!($value instanceof \DateTime))
				throw new \InvalidArgumentException($value . ' is not a valid DateTime value');

			return $value->format(self::FORMAT);
		}

		public function castForEntity($value) {
			return \DateTime::createFromFormat(self::FORMAT, $value);
		}

		public static function handles($type, $length = null, $unsigned = null) {
			return $type === 'datetime';
		}
	}