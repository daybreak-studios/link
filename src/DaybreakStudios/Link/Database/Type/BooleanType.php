<?php
	namespace DaybreakStudios\Link\Database\Type;

	use DaybreakStudios\Link\Database\Options;

	class BooleanType extends Type {
		public function __construct() {
			parent::__construct('boolean', new Options([]), null);
		}

		public function castForDatabase($value) {
			return (bool)$value;
		}

		public function castForEntity($value) {
			return (bool)$value;
		}

		public static function handles($type, $length = null, $unsigned = null) {
			return in_array($type, ['bool', 'boolean']) || $type === 'tinyint' && $length >= 1 && $length <= 3 && $unsigned === true;
		}
	}