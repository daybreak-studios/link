<?php
	namespace DaybreakStudios\Link\Database\Type;

	use DaybreakStudios\Link\Database\OptionsInterface;

	class Type implements TypeInterface {
		private $name;
		private $length;
		private $options;

		public function __construct($name, OptionsInterface $options, $length = null) {
			$this->name = $name;
			$this->options = $options;
			$this->length = $length;
		}

		public function getName() {
			return $this->name;
		}

		public function getLength() {
			return $this->length;
		}

		public function getOptions() {
			return $this->options;
		}

		public function castForDatabase($value) {
			return $value;
		}

		public function castForEntity($value) {
			return $value;
		}

		public static function handles($type, $length = null, $unsigned = null) {
			return true;
		}
	}