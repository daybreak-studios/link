<?php
	namespace DaybreakStudios\Link\Database\Type;

	use DaybreakStudios\Link\Database\Options;

	class Types {
		const TYPES = [
			DateTimeType::class,
			BooleanType::class,
			IntegerType::class,
		];

		/**
		 * @param string    $type
		 * @param int|null  $length
		 * @param bool|null $unsigned
		 *
		 * @return Type
		 */
		public static function getTypeFor($type, $length = null, $unsigned = null) {
			foreach (self::TYPES as $klass)
				if (call_user_func([$klass, 'handles'], $type, $length, $unsigned))
					return new $klass($length, $unsigned);

			$opts = [];

			if ($unsigned !== null)
				$opts[] = Options::OPT_UNSIGNED;

			return new Type($type, new Options($opts), $length);
		}
	}