<?php
	namespace DaybreakStudios\Link\Database\Type;

	use DaybreakStudios\Link\Database\Options;

	class IntegerType extends Type {
		/**
		 * IntegerType constructor.
		 *
		 * @param int  $precision
		 * @param bool $unsigned
		 */
		public function __construct($precision = 10, $unsigned = true) {
			$opts = [];

			if ($unsigned)
				$opts[] = Options::OPT_UNSIGNED;

			parent::__construct('integer', new Options($opts), $precision);
		}

		public function castForDatabase($value) {
			return $value;
		}

		public function castForEntity($value) {
			return (int)$value;
		}

		public static function handles($type, $length = null, $unsigned = null) {
			return stripos($type, 'int') !== false;
		}
	}