<?php
	namespace DaybreakStudios\Link\Database\Type;

	use DaybreakStudios\Link\Database\OptionsInterface;

	interface TypeInterface {
		/**
		 * @return string
		 */
		public function getName();

		/**
		 * @return int|null
		 */
		public function getLength();

		/**
		 * @return OptionsInterface
		 */
		public function getOptions();

		public function castForDatabase($value);
		public function castForEntity($value);

		/**
		 * @param string    $type
		 * @param int|null  $length
		 * @param bool|null $unsigned
		 *
		 * @return bool
		 */
		public static function handles($type, $length = null, $unsigned = null);
	}