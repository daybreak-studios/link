<?php
	namespace DaybreakStudios\Link\Database;

	class ForeignKeyConstraint extends Constraint {
		private $schema;
		private $table;
		private $column;

		/**
		 * ForeignKeyConstraint constructor.
		 *
		 * @param string $name
		 * @param array  $columns
		 * @param string $schema
		 * @param string $table
		 * @param string $column
		 */
		public function __construct($name, array $columns, $schema, $table, $column) {
			parent::__construct(Constraint::TYPE_FOREIGN, $name, $columns);

			$this->schema = $schema;
			$this->table = $table;
			$this->column = $column;
		}

		/**
		 * @return string
		 */
		public function getSchema() {
			return $this->schema;
		}

		/**
		 * @return string
		 */
		public function getTable() {
			return $this->table;
		}

		/**
		 * @return string
		 */
		public function getColumn() {
			return $this->column;
		}
	}