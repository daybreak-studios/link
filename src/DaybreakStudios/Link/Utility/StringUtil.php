<?php
	namespace DaybreakStudios\Link\Utility;

	class StringUtil {
		const PLURALIZERS = [
			'/(ss|sh|ch|[^aeoiu]o)$/i' => '\\1es',
			'/se$/i' => 'ses',
			'/([^aeiou])y$/i' => '\\1ies',
		];

		/**
		 * @param string $string
		 *
		 * @return string
		 */
		public static function pluralize($string) {
			$plural = $string;

			foreach (self::PLURALIZERS as $pattern => $replacement) {
				$plural = preg_replace($pattern, $replacement, $plural);

				if ($plural !== $string)
					break;
			}

			if ($plural === $string)
				$plural .= 's';

			return $plural;
		}

		/**
		 * @param string $string
		 *
		 * @return string
		 */
		public static function classify($string) {
			return str_replace(' ', '', ucwords(strtr($string, '_-', '  ')));
		}

		/**
		 * @param string $string
		 *
		 * @return string
		 */
		public static function camelize($string) {
			return lcfirst(self::classify($string));
		}

		/**
		 * @param string $string
		 *
		 * @return string
		 */
		public static function underscore($string) {
			$s = '';

			for ($i = 0, $ii = strlen($string); $i < $ii; $i++) {
				$c = $string[$i];

				if ($i > 0 && strtoupper($c) === $c)
					$c = '_' . $c;

				$s .= $c;
			}

			return strtolower($s);
		}
	}