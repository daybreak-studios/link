<?php
	namespace DaybreakStudios\Link\Utility\ClassBuilder\Comment;

	class BlockComment extends Comment {
		public function build($depth = 0) {
			$comment = str_repeat("\t", $depth) . '/**' . PHP_EOL;

			foreach (parent::getLines() as $line)
				$comment .= str_repeat("\t", $depth) . ' * ' . $line . PHP_EOL;

			return $comment . str_repeat("\t", $depth) . ' */';
		}
	}