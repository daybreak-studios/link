<?php
	namespace DaybreakStudios\Link\Utility\ClassBuilder\Comment;

	class LineComment extends Comment {
		public function build($depth = 0) {
			$comment = '';

			foreach (parent::getLines() as $line)
				$comment .= str_repeat("\t", $depth) . '// ' . $line . PHP_EOL;

			return trim($comment);
		}
	}