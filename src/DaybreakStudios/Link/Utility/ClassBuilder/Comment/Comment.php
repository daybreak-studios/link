<?php
	namespace DaybreakStudios\Link\Utility\ClassBuilder\Comment;

	abstract class Comment {
		private $lines;

		/**
		 * Comment constructor.
		 *
		 * @param string[] $lines
		 */
		public function __construct(array $lines) {
			$this->lines = $lines;
		}

		/**
		 * @return string[]
		 */
		public function getLines() {
			return $this->lines;
		}

		/**
		 * @param int $depth
		 *
		 * @return string
		 */
		abstract function build($depth = 0);
	}