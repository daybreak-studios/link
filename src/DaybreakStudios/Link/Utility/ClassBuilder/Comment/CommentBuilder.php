<?php
	namespace DaybreakStudios\Link\Utility\ClassBuilder\Comment;

	abstract class CommentBuilder {
		private $type;

		private $lines = [];

		/**
		 * CommentBuilder constructor.
		 *
		 * @param string $type
		 */
		public function __construct($type) {
			$this->type = $type;
		}

		/**
		 * @return string
		 */
		public function getType() {
			return $this->type;
		}

		/**
		 * @return string[]
		 */
		public function getLines() {
			return $this->lines;
		}

		/**
		 * @param string[] $lines
		 *
		 * @return $this
		 */
		public function setLines(array $lines) {
			$this->lines = $lines;

			return $this;
		}

		/**
		 * @param string $line
		 *
		 * @return $this
		 */
		public function addLine($line) {
			$this->lines[] = $line;

			return $this;
		}

		/**
		 * @param string      $type
		 * @param string|null $value
		 *
		 * @return $this
		 */
		public function addDocLine($type, $value = null) {
			$this->lines[] = trim('@' . $type . ' ' . $value);

			return $this;
		}

		/**
		 * @param string    $variable
		 * @param \string[] ...$types
		 *
		 * @return CommentBuilder
		 */
		public function addDocParam($variable, ... $types) {
			return $this->addDocLine('param', implode('|', $types) . ' $' . $variable);
		}

		/**
		 * @param string $method
		 * @param bool   $static
		 * @param array  $returns
		 * @param array  $params
		 *
		 * @return CommentBuilder
		 */
		public function addDocMethod($method, $static = false, array $returns = [], array $params = []) {
			$parameters = [];

			foreach ($params as $variable => $type) {
				if ($type)
					$type = (strpos($type, '\\') > 0 ? '\\' . $type : $type) . ' ';

				$parameters[] = $type . '$' . $variable;
			}

			foreach ($returns as $i => $return)
				if (strpos($return, '\\') > 0)
					$returns[$i] = '\\' . $return;

			return $this->addDocLine('method',
				($static ? ' static ' : '') .
				implode('|', $returns) . ' ' .
				$method . '(' . implode(', ', $parameters) . ')');
		}

		/**
		 * @return Comment
		 */
		public function build() {
			$klass = $this->type;

			return new $klass($this->getLines());
		}
	}