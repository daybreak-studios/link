<?php
	namespace DaybreakStudios\Link\Utility\ClassBuilder\Comment;

	class BlockCommentBuilder extends CommentBuilder {
		public static function create() {
			return new BlockCommentBuilder(BlockComment::class);
		}
	}