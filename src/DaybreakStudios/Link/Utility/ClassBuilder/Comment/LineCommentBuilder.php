<?php
	namespace DaybreakStudios\Link\Utility\ClassBuilder\Comment;

	class LineCommentBuilder extends CommentBuilder {
		public static function create() {
			return new LineCommentBuilder(LineComment::class);
		}
	}