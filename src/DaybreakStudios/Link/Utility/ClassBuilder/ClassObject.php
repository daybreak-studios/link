<?php
	namespace DaybreakStudios\Link\Utility\ClassBuilder;

	use DaybreakStudios\Link\Utility\ClassBuilder\Comment\Comment;
	use DaybreakStudios\Link\Utility\ClassBuilder\Method\Method;
	use DaybreakStudios\Link\Utility\ClassBuilder\Property\Property;

	class ClassObject {
		const TYPE_ABSTRACT = 'abstract';
		const TYPE_INTERFACE = 'interface';

		private $name;
		private $type;
		private $namespace;
		private $extends;
		private $implements;
		private $methods;
		private $properties;
		private $comment;

		/**
		 * ClassObject constructor.
		 *
		 * @param string      $name
		 * @param string|null $type
		 * @param string|null $namespace
		 * @param string|null $extends
		 * @param array       $implements
		 * @param Method[]    $methods
		 * @param Property[]  $properties
		 * @param Comment     $comment
		 */
		public function __construct(
			$name,
			$type = null,
			$namespace = null,
			$extends = null,
			array $implements = [],
			array $methods = [],
			array $properties = [],
			Comment $comment = null
		) {
			$this->name = $name;
			$this->type = $type;
			$this->namespace = $namespace;
			$this->extends = $extends;
			$this->implements = $implements;
			$this->methods = $methods;
			$this->properties = $properties;
			$this->comment = $comment;
		}

		/**
		 * @return string
		 */
		public function getName() {
			return $this->name;
		}

		/**
		 * @return string|null
		 */
		public function getType() {
			return $this->type;
		}

		/**
		 * @return string|null
		 */
		public function getNamespace() {
			return $this->namespace;
		}

		/**
		 * @return string|null
		 */
		public function getExtends() {
			return $this->extends;
		}

		/**
		 * @return array
		 */
		public function getImplements() {
			return $this->implements;
		}

		/**
		 * @return Method[]
		 */
		public function getMethods() {
			return $this->methods;
		}

		/**
		 * @return Property[]
		 */
		public function getProperties() {
			return $this->properties;
		}

		/**
		 * @return Comment|null
		 */
		public function getComment() {
			return $this->comment;
		}

		/**
		 * @param int $depth
		 *
		 * @return string
		 */
		public function build($depth = 0) {
			$klass = '';

			if ($this->getNamespace())
				$klass .= $this->tab($depth) . 'namespace ' . $this->getNamespace() . ';' . PHP_EOL . PHP_EOL;

			if ($this->getComment())
				$klass .= $this->getComment()->build($depth) . PHP_EOL;

			$klass .= $this->tab($depth) . ($this->getType() ?: 'class') . ' ' . $this->getName();

			if ($this->getExtends())
				$klass .= ' extends ' . $this->getExtends();

			if ($this->getImplements())
				$klass .= ' implements ' . implode(', ', $this->getImplements());

			$klass .= ' {' . PHP_EOL;

			foreach ($this->getProperties() as $property)
				$klass .= $property->build($depth + 1) . PHP_EOL;

			if (sizeof($this->getProperties()))
				$klass .= PHP_EOL;

			foreach ($this->getMethods() as $i => $method) {
				if ($i > 0)
					$klass .= PHP_EOL;

				$klass .= $method->build($depth + 1) . PHP_EOL;
			}

			return $klass . $this->tab($depth) . '}';
		}

		/**
		 * @param int $depth
		 *
		 * @return mixed
		 */
		private function tab($depth) {
			return str_repeat("\t", $depth);
		}
	}