<?php
	namespace DaybreakStudios\Link\Utility\ClassBuilder;

	class MemberAccess {
		const ACCESS_PUBLIC = 1;
		const ACCESS_PRIVATE = 2;
		const ACCESS_PROTECTED = 4;
		const ACCESS_STATIC = 8;

		const MAP = [
			self::ACCESS_PUBLIC => 'public',
			self::ACCESS_PRIVATE => 'private',
			self::ACCESS_PROTECTED => 'protected',
			self::ACCESS_STATIC => 'static',
		];

		/**
		 * @param int $access
		 *
		 * @return string
		 * @throws \InvalidArgumentException if $access could not be converted
		 */
		public static function convert($access) {
			$str = '';

			foreach (self::MAP as $level => $name)
				if ($access & $level) {
					$str .= $name;

					break;
				}

			if (strlen($str) === 0)
				throw new \InvalidArgumentException('Cannot convert ' . $access . ' to access string');

			return $str . ($access & self::ACCESS_STATIC ? ' static' : '');
		}
	}