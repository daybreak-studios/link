<?php
	namespace DaybreakStudios\Link\Utility\ClassBuilder\Property;

	use DaybreakStudios\Link\Utility\ClassBuilder\MemberAccess;

	class Property {
		const NO_DEFAULT_VALUE = '0000000ThisPropertyHasNoDefaultValue0000000';

		private $name;
		private $type;
		private $default;
		private $access;

		/**
		 * Property constructor.
		 *
		 * @param string   $name
		 * @param null     $type
		 * @param string   $default
		 * @param int|null $access
		 */
		public function __construct($name, $type = null, $default = self::NO_DEFAULT_VALUE, $access = null) {
			$this->name = $name;
			$this->type = $type;
			$this->default = $default;
			$this->access = $access;
		}

		/**
		 * @return string
		 */
		public function getName() {
			return $this->name;
		}

		/**
		 * @return mixed
		 */
		public function getType() {
			return $this->type;
		}

		/**
		 * @return mixed
		 */
		public function getDefault() {
			return $this->default;
		}

		/**
		 * @return bool
		 */
		public function hasDefault() {
			return $this->getDefault() !== self::NO_DEFAULT_VALUE;
		}

		/**
		 * @return int|null
		 */
		public function getAccess() {
			return $this->access;
		}

		/**
		 * @param int $depth
		 *
		 * @return string
		 */
		public function build($depth = 0) {
			$prop = str_repeat("\t", $depth);

			if ($this->getAccess())
				$prop .= MemberAccess::convert($this->getAccess()) . ' ';
			else if ($this->getType())
				$prop .= $this->getType() . ' ';

			$prop .= '$' . $this->getName();

			if ($this->hasDefault()) {
				$default = $this->getDefault();

				if (is_string($default) && !defined($default))
					$default = '"' . addslashes($default) . '"';
				else if (is_bool($default))
					$default = $default ? 'true' : 'false';

				$prop .= ' = ' . $default;
			}

			if ($this->getAccess())
				$prop .= ';';

			return $prop;
		}
	}