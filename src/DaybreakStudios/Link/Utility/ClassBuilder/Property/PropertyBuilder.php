<?php
	namespace DaybreakStudios\Link\Utility\ClassBuilder\Property;

	class PropertyBuilder {
		private $name;
		private $access;

		private $type = null;
		private $default = Property::NO_DEFAULT_VALUE;

		/**
		 * PropertyBuilder constructor.
		 *
		 * @param string   $name
		 * @param int|null $access
		 */
		protected function __construct($name, $access = null) {
			$this->name = $name;
			$this->access = $access;
		}

		/**
		 * @return string
		 */
		public function getName() {
			return $this->name;
		}

		/**
		 * @return string|null
		 */
		public function getType() {
			return $this->type;
		}

		/**
		 * @return bool
		 */
		public function hasDefault() {
			return $this->getDefault() !== Property::NO_DEFAULT_VALUE;
		}

		/**
		 * @return mixed
		 */
		public function getDefault() {
			return $this->default;
		}

		/**
		 * @return int|null
		 */
		public function getAccess() {
			return $this->access;
		}

		/**
		 * @param $type
		 *
		 * @return $this
		 */
		public function typed($type) {
			$this->type = strpos($type, '\\') > 0 ? '\\' . $type : $type;

			return $this;
		}

		/**
		 * @param $default
		 *
		 * @return $this
		 */
		public function defaultTo($default) {
			$this->default = $default;

			return $this;
		}

		/**
		 * @return Property
		 */
		public function build() {
			return new Property($this->getName(), $this->getType(), $this->getDefault(), $this->getAccess());
		}

		/**
		 * @param string   $name
		 * @param int|null $access
		 *
		 * @return PropertyBuilder
		 */
		public static function create($name, $access = null) {
			return new PropertyBuilder($name, $access);
		}
	}