<?php
	namespace DaybreakStudios\Link\Utility\ClassBuilder\Method;

	use DaybreakStudios\Link\Utility\ClassBuilder\Property\Property;

	class MethodBuilder {
		private $name;
		private $abstract;
		private $access;

		private $signature = [];
		private $returns = null;
		private $code = null;

		/**
		 * MethodBuilder constructor.
		 *
		 * @param string $name
		 * @param int    $access
		 * @param bool   $abstract
		 */
		protected function __construct($name, $access, $abstract = false) {
			$this->name = $name;
			$this->abstract = $abstract;
			$this->access = $access;
		}

		/**
		 * @return string
		 */
		public function getName() {
			return $this->name;
		}

		/**
		 * @return Property[]
		 */
		public function getSignature() {
			return $this->signature;
		}

		/**
		 * @return string|null
		 */
		public function getReturnType() {
			return $this->returns;
		}

		/**
		 * @return bool
		 */
		public function isAbstract() {
			return $this->abstract;
		}

		/**
		 * @return int
		 */
		public function getAccess() {
			return $this->access;
		}

		/**
		 * @return string|null
		 */
		public function getReturns() {
			return $this->returns;
		}

		/**
		 * @return string|null
		 */
		public function getCode() {
			return $this->code;
		}

		/**
		 * @param Property[] $properties
		 *
		 * @return $this
		 */
		public function accepts(Property... $properties) {
			foreach ($properties as $property)
				$this->signature[] = $property;

			return $this;
		}

		/**
		 * @param string|null $type
		 *
		 * @return $this
		 */
		public function returns($type = null) {
			$this->returns = $type;

			return $this;
		}

		/**
		 * @param string|\Closure|null $code
		 *
		 * @return $this
		 */
		public function executes($code) {
			if ($code instanceof \Closure)
				$code = $this->convertClosure($code);

			$this->code = $code;

			return $this;
		}

		/**
		 * @param \Closure $closure
		 *
		 * @return string
		 */
		private function convertClosure(\Closure $closure) {
			$refl = new \ReflectionFunction($closure);

			$file = new \SplFileObject($refl->getFileName());
			$file->seek($refl->getStartLine() - 1);

			$code = '';
			$depth = null;

			for ($i = $refl->getStartLine(), $ii = $refl->getEndLine() - 1; $i < $ii; $i++) {
				if ($file->eof())
					throw new \RuntimeException('Unexpected EOF when parsing closure source');

				$line = $file->fgets();

				if ($depth === null)
					$depth = strlen($line) - strlen(ltrim($line));

				if (sizeof($line) > 0)
					$line = substr($line, $depth);

				$code .= rtrim($line) . PHP_EOL;
			}

			return trim($code);
		}

		/**
		 * @return Method
		 */
		public function build() {
			return new Method($this->getName(), $this->getAccess(), $this->isAbstract(), $this->getSignature(),
				$this->getCode(), $this->getReturns());
		}

		/**
		 * @param string $name
		 * @param int    $access
		 * @param bool   $abstract
		 *
		 * @return MethodBuilder
		 */
		public static function create($name, $access, $abstract = false) {
			return new MethodBuilder($name, $access, $abstract);
		}
	}