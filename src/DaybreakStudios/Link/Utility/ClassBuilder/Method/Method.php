<?php
	namespace DaybreakStudios\Link\Utility\ClassBuilder\Method;

	use DaybreakStudios\Link\Utility\ClassBuilder\MemberAccess;
	use DaybreakStudios\Link\Utility\ClassBuilder\Property\Property;

	class Method {
		private $name;
		private $access;
		private $abstract;
		private $signature;
		private $returns;
		private $code;

		/**
		 * Method constructor.
		 *
		 * @param string      $name
		 * @param int         $access
		 * @param bool        $abstract
		 * @param array       $signature
		 * @param string|null $code
		 * @param string|null $returns
		 */
		public function __construct($name, $access, $abstract = false, array $signature = [], $code = null, $returns = null) {
			$this->name = $name;
			$this->access = $access;
			$this->abstract = $abstract;
			$this->signature = $signature;
			$this->code = $code;
			$this->returns = $returns;
		}

		/**
		 * @return string
		 */
		public function getName() {
			return $this->name;
		}

		/**
		 * @return int
		 */
		public function getAccess() {
			return $this->access;
		}

		/**
		 * @return bool
		 */
		public function isAbstract() {
			return $this->abstract;
		}

		/**
		 * @return Property[]
		 */
		public function getSignature() {
			return $this->signature;
		}

		/**
		 * @return string|null
		 */
		public function getReturns() {
			return $this->returns;
		}

		/**
		 * @return string|null
		 */
		public function getCode() {
			return $this->code;
		}

		/**
		 * @param int $depth
		 *
		 * @return string
		 */
		public function build($depth = 0) {
			$method = $this->tab($depth) . MemberAccess::convert($this->access) . ' ';

			if ($this->isAbstract())
				$method .= 'abstract ';

			$method .= 'function ' . $this->getName() . '(';
			$parameters = [];

			foreach ($this->getSignature() as $property) {
				$p = '';

				if ($property->getType())
					$p .= $property->getType() . ' ';

				$p .= '$' . $property->getName();

				if ($property->hasDefault()) {
					$default = $property->getDefault();

					if (is_string($default) && !defined($default))
						$default = '"' . addslashes($default) . '"';

					$p .= ' = ' . $default;
				}

				$parameters[] = $p;
			}

			$method .= implode(', ', $parameters) . ')';

			if ($this->getReturns())
				$method .= ': ' . $this->getReturns();

			if (!$this->isAbstract()) {
				if (!$this->getCode())
					throw new \InvalidArgumentException('A method not declared abstract MUST execute code');

				$method .= ' { ' . PHP_EOL;

				$lines = explode("\n", $this->getCode());

				foreach ($lines as $i => $line)
					if (strlen($line) > 0)
						$lines[$i] = $this->tab($depth + 1) . $line;

				$method .= implode("\n", $lines) . PHP_EOL .
					$this->tab($depth) . '}';
			} else
				$method .= ';';

			return $method;
		}

		/**
		 * @param int $depth
		 *
		 * @return mixed
		 */
		private function tab($depth) {
			return str_repeat("\t", $depth);
		}
	}