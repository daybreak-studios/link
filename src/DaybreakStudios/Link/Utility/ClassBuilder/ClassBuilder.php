<?php
	namespace DaybreakStudios\Link\Utility\ClassBuilder;

	use DaybreakStudios\Link\Utility\ClassBuilder\Comment\Comment;
	use DaybreakStudios\Link\Utility\ClassBuilder\Method\Method;
	use DaybreakStudios\Link\Utility\ClassBuilder\Property\Property;

	class ClassBuilder {
		private $namespace;
		private $name;
		private $type;

		private $extends = null;
		private $implements = [];
		private $methods = [];
		private $properties = [];
		private $comment = null;

		/**
		 * ClassBuilder constructor.
		 *
		 * @param string      $name
		 * @param string|null $type
		 * @param string|null $namespace
		 */
		protected function __construct($name, $type = null, $namespace = null) {
			$this->name = $name;
			$this->type = $type;
			$this->namespace = $namespace;
		}

		/**
		 * @return string
		 */
		public function getName() {
			return $this->name;
		}

		/**
		 * @return string|null
		 */
		public function getNamespace() {
			return $this->namespace;
		}

		/**
		 * @return string|null
		 */
		public function getExtends() {
			return $this->extends;
		}

		/**
		 * @return string[]
		 */
		public function getImplements() {
			return $this->implements;
		}

		/**
		 * @return string[]
		 */
		public function getMethods() {
			return $this->methods;
		}

		/**
		 * @return string[]
		 */
		public function getProperties() {
			return $this->properties;
		}

		/**
		 * @return Comment|null
		 */
		public function getComment() {
			return $this->comment;
		}

		/**
		 * @return string|null
		 */
		public function getType() {
			return $this->type;
		}

		/**
		 * @param string[] $klasses
		 *
		 * @return $this
		 */
		public function implement(... $klasses) {
			$this->implements = array_map(function($klass) {
				return '\\' . $klass;
			}, $klasses);

			return $this;
		}

		/**
		 * @param string|null $klass
		 *
		 * @return $this
		 */
		public function extend($klass = null) {
			$this->extends = $klass ? '\\' . $klass : null;

			return $this;
		}

		/**
		 * @param Method[] $methods
		 *
		 * @return $this
		 */
		public function setMethods(array $methods) {
			$this->methods = $methods;

			return $this;
		}

		/**
		 * @param Method $method
		 *
		 * @return $this
		 */
		public function addMethod(Method $method) {
			$this->methods[] = $method;

			return $this;
		}

		/**
		 * @param Property[] $properties
		 *
		 * @return $this
		 */
		public function setProperties(array $properties) {
			$this->properties = $properties;

			return $this;
		}

		/**
		 * @param Property $property
		 *
		 * @return $this
		 */
		public function addProperty(Property $property) {
			$this->properties[] = $property;

			return $this;
		}

		/**
		 * @param Comment|null $comment
		 *
		 * @return $this
		 */
		public function comment(Comment $comment = null) {
			$this->comment = $comment;

			return $this;
		}

		/**
		 * @return ClassObject
		 */
		public function build() {
			return new ClassObject($this->getName(), $this->getType(), $this->getNamespace(), $this->getExtends(),
				$this->getImplements(), $this->getMethods(), $this->getProperties(), $this->comment);
		}

		/**
		 * @param string      $name
		 * @param string|null $type
		 * @param string|null $namespace
		 *
		 * @return ClassBuilder
		 */
		public static function create($name, $type = null, $namespace = null) {
			return new ClassBuilder($name, $type, $namespace);
		}
	}