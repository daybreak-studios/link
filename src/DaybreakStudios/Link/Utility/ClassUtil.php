<?php
	namespace DaybreakStudios\Link\Utility;

	class ClassUtil {
		/**
		 * @param string $klass
		 *
		 * @return string
		 */
		public static function getNamespace($klass) {
			if (strpos($klass, '\\') === false)
				return '\\';

			return substr($klass, 0, strrpos($klass, '\\'));
		}

		/**
		 * @param string $klass
		 *
		 * @return mixed
		 */
		public static function getClassName($klass) {
			if (strpos($klass, '\\') === false)
				return $klass;

			return substr($klass, strrpos($klass, '\\') + 1);
		}

		/**
		 * @param mixed $object
		 *
		 * @return string
		 */
		public static function getType($object) {
			if (!is_object($object))
				return gettype($object);

			return get_class($object);
		}
	}