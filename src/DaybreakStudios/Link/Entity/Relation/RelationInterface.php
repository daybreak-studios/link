<?php
	namespace DaybreakStudios\Link\Entity\Relation;

	interface RelationInterface {
		/**
		 * @return string
		 */
		public function getEntityClass();

		/**
		 * @return array
		 */
		public function getConditions();

		/**
		 * @return string|null
		 */
		public function getColumn();

		/**
		 * @return string|null
		 */
		public function getTargetColumn();

		/**
		 * @return bool
		 */
		public function isOwner();
	}