<?php
	namespace DaybreakStudios\Link\Entity\Relation;

	abstract class AbstractRelation implements RelationInterface {
		private $entity;
		private $column;
		private $target;
		private $conditions;
		private $owned;

		/**
		 * AbstractRelation constructor.
		 *
		 * @param string      $entity
		 * @param array       $conditions
		 * @param bool        $owned
		 * @param string|null $column
		 * @param string|null $target
		 */
		public function __construct($entity, array $conditions, $owned, $column = null, $target = null) {
			$this->entity = $entity;
			$this->column = $column;
			$this->target = $target;
			$this->conditions = $conditions;
			$this->owned = $owned;
		}

		/**
		 * @return string
		 */
		public function getEntityClass() {
			return $this->entity;
		}

		/**
		 * @return string
		 */
		public function getColumn() {
			return $this->column;
		}

		/**
		 * @return string|null
		 */
		public function getTargetColumn() {
			return $this->target;
		}

		/**
		 * @return array
		 */
		public function getConditions() {
			return $this->conditions;
		}

		/**
		 * @return bool
		 */
		public function isOwner() {
			return $this->owned;
		}
	}