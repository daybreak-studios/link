<?php
	namespace DaybreakStudios\Link\Entity;

	use DaybreakStudios\Link\AST\Table;
	use DaybreakStudios\Link\Database\ColumnInterface;
	use DaybreakStudios\Link\Database\Reader\SqlReader;
	use DaybreakStudios\Link\Entity\Relation\RelationInterface;
	use DaybreakStudios\Link\Utility\StringUtil;

	class EntityMetadata {
		private $klass;
		private $entity;
		private $table;
		private $reader;

		private $relations = [];

		/**
		 * EntityMetadata constructor.
		 *
		 * @param string $klass
		 */
		public function __construct($klass) {
			$this->klass = $klass;
			$this->entity = substr($klass, strrpos($klass, '\\') + 1);
			$this->table = strtolower(StringUtil::pluralize($this->entity));

			if (EntitiesConfiguration::instance()->getConnection() === null)
				throw new \BadMethodCallException('Could not establish database connection');

			$this->reader = new SqlReader($this->table, EntitiesConfiguration::getConnection());
		}

		/**
		 * @return string
		 */
		public function getEntityClass() {
			return $this->klass;
		}

		/**
		 * @return string
		 */
		public function getEntityName() {
			return $this->entity;
		}

		/**
		 * @return Table
		 */
		public function getTable() {
			return new Table(strtolower($this->table), null,
				EntitiesConfiguration::getConnection()->getSchema());
		}

		/**
		 * @return string
		 */
		public function getTableName() {
			return $this->table;
		}

		/**
		 * @return ColumnInterface[]
		 */
		public function getColumns() {
			return $this->reader->getColumns();
		}

		/**
		 * @param string $name
		 *
		 * @return ColumnInterface|null
		 */
		public function getColumn($name) {
			return $this->reader->getColumn($name);
		}

		/**
		 * @param string $name
		 *
		 * @return bool
		 */
		public function hasColumn($name) {
			return $this->reader->hasColumn($name);
		}

		/**
		 * @param string            $column
		 * @param RelationInterface $relation
		 *
		 * @return $this
		 */
		public function addRelation($column, RelationInterface $relation) {
			$this->relations[$column] = $relation;

			return $this;
		}

		/**
		 * @return RelationInterface[]
		 */
		public function getRelations() {
			return $this->relations;
		}

		/**
		 * @param string $column
		 *
		 * @return RelationInterface|null
		 */
		public function getRelation($column) {
			if (!isset($this->getRelations()[$column]))
				return null;

			return $this->getRelations()[$column];
		}

		/**
		 * @param string $column
		 *
		 * @return bool
		 */
		public function isRelation($column) {
			return $this->getRelation($column) !== null;
		}

		/**
		 *
		 */
		public function reset() {
			$this->reader = new SqlReader($this->table, EntitiesConfiguration::getConnection());
		}
	}