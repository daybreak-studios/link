<?php
	namespace DaybreakStudios\Link\Entity;

	use DaybreakStudios\Link\AST\DeleteManager;
	use DaybreakStudios\Link\AST\InsertManager;
	use DaybreakStudios\Link\AST\Node\OrderingNode;
	use DaybreakStudios\Link\AST\UpdateManager;
	use DaybreakStudios\Link\Connection\Connection;
	use DaybreakStudios\Link\Entity\Collection\AttributeCollection;
	use DaybreakStudios\Link\Entity\Collection\RelationCollection;
	use DaybreakStudios\Link\Entity\Exception\ConcurrentModificationException;
	use DaybreakStudios\Link\Entity\Exception\EntityDestroyedException;
	use DaybreakStudios\Link\Entity\Exception\EntityDetachedException;
	use DaybreakStudios\Link\Entity\Exception\InvalidEntityException;
	use DaybreakStudios\Link\Entity\Exception\MissingConnectionException;
	use DaybreakStudios\Link\Entity\Exception\PartialEntityException;
	use DaybreakStudios\Link\Entity\Relation\ManyRelation;
	use DaybreakStudios\Link\Entity\Relation\OneRelation;
	use DaybreakStudios\Link\Entity\Relation\RelationInterface;
	use DaybreakStudios\Link\Utility\ClassUtil;
	use DaybreakStudios\Link\Utility\StringUtil;

	abstract class Entity implements EntityInterface, \Serializable {
		const STATE_NEW = 1;
		const STATE_PERSISTED = 2;
		const STATE_DESTROYED = 3;
		const STATE_DETACHED = 4;

		private $state;
		private $columns;
		private $relations;

		public function __construct(array $data = []) {
			static::__load();

			$this->state = self::STATE_NEW;

			$md = EntityMetadataRegistry::get(static::class);
			$attributes = [];

			foreach ($md->getColumns() as $column) {
				if ($md->isRelation($column->getField()))
					continue;

				$key = StringUtil::camelize($column->getField());
				$value = $column->getDefault();

				if (array_key_exists($key, $data))
					$value = $data[$key];

				$attributes[$key] = $value;
			}

			$this->columns = new AttributeCollection($this, $attributes);

			$relations = [];

			foreach ($md->getRelations() as $relation) {
				$key = StringUtil::camelize($relation->getColumn());
				$value = $relation;

				if (array_key_exists($key, $data)) {
					$value = $data[$key];

					if ($relation instanceof ManyRelation) {
						if ($value === null)
							$value = [];
						else if (!is_array($value))
							throw new \InvalidArgumentException('Many relations must be set using null or an array ' .
								'of ' . EntityInterface::class);

						foreach ($value as $item)
							if (!($item instanceof EntityInterface))
								throw new \InvalidArgumentException('Many relations must be set using null or an ' .
									'array of ' . EntityInterface::class);
					} else if ($value !== null && !($value instanceof EntityInterface))
						throw new \InvalidArgumentException('One relations must be seting using null or an instance ' .
							'of ' . EntityInterface::class);
				}

				$relations[$key] = $value;
			}

			$this->relations = new RelationCollection($this, $relations);
		}

		/**
		 * @return int|string|null
		 */
		public function getId() {
			if (!$this->has('id'))
				return null;

			return $this->get('id');
		}

		/**
		 * @param string $key
		 *
		 * @return mixed
		 * @throws PartialEntityException
		 */
		protected function get($key) {
			if ($this->relations->has($key)) {
				$relation = $this->relations->get($key);

				if ($relation instanceof RelationInterface) {
					$method = 'findOneBy';
					$conditions = $relation->getConditions();

					if ($relation instanceof ManyRelation) {
						$method = 'findBy';

						$conditions[$relation->getTargetColumn()] = $this->getId();
					}

					$relation = call_user_func([$relation->getEntityClass(), $method], $conditions);

					$this->relations->put($key, $relation);

					// Since we just lazy-loaded the field, we don't want it to appear in our change set during `save`,
					// unless something else modifies it following this call.
					$this->relations->sync($key);
				}

				return $relation;
			}

			if (!$this->columns->has($key))
				throw new \BadMethodCallException('No field exists named ' . $key);

			$value = $this->columns->get($key);

			if ($value instanceof PartialEntityException)
				throw $value;

			return $value;
		}

		/**
		 * @param string $key
		 * @param mixed  $value
		 *
		 * @return $this
		 * @throws EntityDestroyedException
		 */
		protected function set($key, $value) {
			if ($this->isDestroyed())
				throw new EntityDestroyedException();

			if ($this->relations->has($key)) {
				$relation = EntityMetadataRegistry::get(static::class)->getRelation($key);

				if ($relation instanceof ManyRelation) {
					if ($value === null)
						$value = [];
					else if ($value instanceof EntityInterface)
						$value = [$value];
					else if (is_array($value)) {
						$keep = [];

						foreach ($value as $item) {
							if (is_a($item, $relation->getEntityClass())) {
								$keep[] = $item;

								continue;
							} else if ($item === null)
								continue;

							throw new \InvalidArgumentException(sprintf('%s is not a valid type for %s',
								ClassUtil::getType($item), $key));
						}

						$value = $keep;
					} else
						throw new \InvalidArgumentException(sprintf('%s is not a valid type for %s',
							ClassUtil::getType($value), $key));
				} else if ($relation instanceof OneRelation) {
					if ($value !== null && (!is_object($value) || !is_a($value, $relation->getEntityClass())))
						throw new \InvalidArgumentException(sprintf('%s must be an instance of %s, %s given',
							$key, $relation->getEntityClass(), ClassUtil::getType($value)));
				}

				$this->relations->put($key, $value);

				return $this;
			}

			if (!$this->columns->has($key))
				throw new \BadMethodCallException('No field exists named' . $key);

			$this->columns->put($key, $value);

			return $this;
		}

		/**
		 * @param string $key
		 *
		 * @return bool
		 */
		protected function has($key) {
			return $this->columns->has($key);
		}

		public function isNewRecord() {
			return $this->state === self::STATE_NEW;
		}

		public function isPersisted() {
			return $this->state === self::STATE_PERSISTED;
		}

		public function isDestroyed() {
			return $this->state === self::STATE_DESTROYED;
		}

		public function reload() {
			if (!$this->state === self::STATE_PERSISTED)
				throw new \BadMethodCallException('Cannot reload an entity that has not been persisted');

			$conn = EntitiesConfiguration::getConnection();

			if ($conn === null)
				throw new MissingConnectionException();

			EntitiesConfiguration::getCache()->evict($this);

			$md = EntityMetadataRegistry::get(static::class);
			$md->reset();

			$table = EntityMetadataRegistry::get(static::class)->getTable();
			$sql = $table
				->select('*')
				->where($table['id']->eq($this->getId()))
				->getSql($conn);

			$data = $conn->fetch($sql);

			if (sizeof($data) === 0)
				throw ConcurrentModificationException::deleted(static::class, $this->getId());

			$data = (array)$data[0];
			$columns = [];

			foreach ($md->getColumns() as $column) {
				if ($md->isRelation($column->getField()))
					continue;

				$name = $column->getField();
				$field = StringUtil::camelize($name);

				if (!array_key_exists($name, $data)) {
					$columns[$field] = new PartialEntityException(static::class, $field);

					continue;
				}

				$columns[$field] = $column->getType()->castForEntity($data[$name]);
			}

			$this->columns = new AttributeCollection($this, $columns);

			$rels = [];

			foreach ($md->getRelations() as $relation)
				$rels[StringUtil::camelize($relation->getColumn())] = $relation;

			$this->relations = new RelationCollection($this, $rels);
		}

		public function save() {
			if ($this->state === self::STATE_DESTROYED)
				throw new EntityDestroyedException();
			else if ($this->state === self::STATE_DETACHED)
				throw new EntityDetachedException();

			$md = EntityMetadataRegistry::get(static::class);
			$conn = EntitiesConfiguration::getConnection();

			if ($conn === null)
				throw new MissingConnectionException();

			foreach ($this->getRelatedEntities() as $relation)
				$relation->save();

			$changes = array_merge($this->columns->getChanges(), $this->relations->getChanges());

			if (!$changes)
				return;

			$_changes = [];

			foreach ($changes as $k => $v) {
				if ($k === 'id')
					continue;

				$column = StringUtil::underscore($k);

				$_changes[$column] = $md->getColumn($column)->getType()->castForDatabase($v);
			}

			$changes = $_changes;

			$table = $md->getTable();
			$sql = null;

			if ($this->state === self::STATE_NEW)
				$sql = (new InsertManager($conn))
					->into($table)
					->insert($changes)
					->getSql();
			else
				$sql = (new UpdateManager($conn))
					->table($table)
					->set($changes)
					->where($table['id']->eq($this->getId()))
					->getSql();

			$result = $conn->alter($sql);

			if ($this->state === self::STATE_NEW) {
				$this->columns->put('id', $result);

				$this->state = self::STATE_PERSISTED;
			}

			$this->columns->sync();
			$this->relations->sync();
		}

		public function destroy() {
			if ($this->state !== self::STATE_PERSISTED)
				throw new \BadMethodCallException('Cannot destroy an entity that is not persisted');

			$md = EntityMetadataRegistry::get(static::class);

			foreach ($md->getRelations() as $key => $relation) {
				if (!$relation->isOwner())
					continue;

				/** @var EntityInterface|EntityInterface[]|null $ent */
				$ent = $this->get($key);

				if ($ent === null)
					continue;

				if (is_array($ent)) {
					foreach ($ent as $e)
						$e->destroy();
				} else
					$ent->destroy();
			}

			$conn = EntitiesConfiguration::getConnection();

			if ($conn === null)
				throw new MissingConnectionException();

			$table = $md->getTable();
			$sql = (new DeleteManager($conn))
				->from($table)
				->where($table['id']->eq($this->getId()))
				->getSql();

			$conn->alter($sql);

			$this->state = self::STATE_DESTROYED;
		}

		public function detach() {
			if ($this->state !== self::STATE_PERSISTED)
				return false;

			EntitiesConfiguration::getCache()->evict($this);

			$this->state = self::STATE_DETACHED;

			return true;
		}

		public function __call($name, array $arguments) {
			$verb = substr($name, 0, 3);

			if (strpos($verb, 'is') === 0)
				$verb = 'get';

			if (!in_array($verb, ['get', 'set']))
				throw new \BadMethodCallException(sprintf('No method named %s exists in %s', $name, static::class));

			$field = lcfirst(substr($name, 3));

			return call_user_func([$this, $verb], $field, ...$arguments);
		}

		/**
		 * @return EntityInterface[]
		 */
		private function getRelatedEntities() {
			$relations = [];

			foreach ($this->relations as $relation) {
				if ($relation === null || $relation instanceof RelationInterface)
					continue;

				if (is_array($relation))
					foreach ($relation as $entity)
						$relations[] = $entity;
				else
					$relations[] = $relation;
			}

			return $relations;
		}

		/**
		 * @param array    $cond
		 * @param array    $order
		 * @param int|null $limit
		 * @param int|null $offset
		 *
		 * @return static[]
		 * @throws InvalidEntityException if the select query does not contain an `id` column
		 * @throws \InvalidArgumentException if any value in $order is not "ASC" or "DESC" (case insensitive)
		 */
		public static function findBy(array $cond, array $order = [], $limit = null, $offset = null) {
			static::__load();

			$conn = EntitiesConfiguration::getConnection();

			if ($conn === null)
				throw new MissingConnectionException();

			$table = EntityMetadataRegistry::get(static::class)->getTable();
			$table
				->select('*')
				->limit($limit)
				->offset($offset);

			foreach ($cond as $column => $value)
				$table->where($table[$column]->eq($value));

			foreach ($order as $column => $dir) {
				$_dir = strtolower($dir);

				if ($_dir === OrderingNode::ORDER_ASC)
					$table->order($table[$column]->asc());
				else if ($_dir === OrderingNode::ORDER_DESC)
					$table->order($table[$column]->desc());
				else
					throw new \InvalidArgumentException(sprintf('%s is not a recognized order direction', $dir));
			}

			$entities = [];

			foreach ($conn->fetch($table->from()->getSql($conn)) as $columns) {
				if (!isset($columns->id))
					throw new InvalidEntityException(static::class);

				$entity = EntitiesConfiguration::getCache()->get(static::class, $columns->id);

				if ($entity === null) {
					$entity = EntitiesConfiguration::getHydrator()->hydrate(static::class, (array)$columns);

					EntitiesConfiguration::getCache()->add($entity);
				}

				$entities[] = $entity;
			}

			return $entities;
		}

		/**
		 * @param array    $cond
		 * @param array    $order
		 * @param int|null $offset
		 *
		 * @return static
		 * @throws InvalidEntityException if the select query does not contain an `id` column
		 * @throws \InvalidArgumentException if any value in $order is not "ASC" or "DESC" (case insensitive)
		 */
		public static function findOneBy(array $cond, array $order = [], $offset = null) {
			static::__load();

			$find = static::findBy($cond, $order, 1, $offset);

			if (sizeof($find) === 0)
				return null;

			return $find[0];
		}

		/**
		 * @param int|string|null $id
		 *
		 * @return static
		 * @throws InvalidEntityException if the select query does not contain an `id` column
		 */
		public static function find($id) {
			return static::findOneBy([
				'id' => $id,
			]);
		}

		protected static function __init() {}

		/**
		 * @param string      $klass
		 * @param string      $name
		 * @param array       $conds
		 * @param string|null $col    the column on the entity that is used in the relationship; an empty string
		 *                            (default) indicates that the column should be inferred, while null indicates that
		 *                            the entity does not have a column for the relationship
		 * @param string|null $target the column on the target entity that is used in the relationship; the rules for
		 *                            this argument are the same as those for $col
		 */
		protected static function hasOne($klass, $name, array $conds = [], $col = '', $target = '') {
			static::addOneRelation($name, $klass, false, $conds, $col, $target);
		}

		/**
		 * @param string      $klass
		 * @param string      $name
		 * @param array       $conds
		 * @param string|null $col    the column on the entity that is used in the relationship; an empty string
		 *                            (default) indicates that the column should be inferred, while null indicates that
		 *                            the entity does not have a column for the relationship
		 * @param string|null $target the column on the target entity that is used in the relationship; the rules for
		 *                            this argument are the same as those for $col
		 */
		protected static function ownsOne($klass, $name, array $conds = [], $col = '', $target = '') {
			static::addOneRelation($name, $klass, true, $conds, $col, $target);
		}

		/**
		 * @param string      $klass
		 * @param string      $name
		 * @param array       $conds
		 * @param string|null $target
		 */
		protected static function hasMany($klass, $name, array $conds = [], $target = null) {
			static::addManyRelation($name, $klass, false, $conds, $target);
		}

		/**
		 * @param string      $klass
		 * @param string      $name
		 * @param array       $conds
		 * @param string|null $target
		 */
		protected static function ownsMany($klass, $name, array $conds = [], $target = null) {
			static::addManyRelation($name, $klass, true, $conds, $target);
		}

		/**
		 * @param string $name
		 * @param string $entity
		 * @param bool   $owned
		 * @param array  $conds
		 * @param string $col
		 * @param string $target
		 */
		private static function addOneRelation($name, $entity, $owned, array $conds = [], $col = '', $target = '') {
			if ($col !== null && strlen($col) === 0)
				$col = StringUtil::underscore(ClassUtil::getClassName($entity));

			if ($target !== null && strlen($target) === 0)
				$target = StringUtil::underscore(ClassUtil::getClassName(static::class));

			EntityMetadataRegistry::get(static::class)
				->addRelation($name, new OneRelation($entity, $conds, $owned, $col, $target));
		}

		/**
		 * @param string      $name
		 * @param string      $entity
		 * @param bool        $owned
		 * @param array       $conds
		 * @param string|null $target
		 */
		private static function addManyRelation($name, $entity, $owned, array $conds = [], $target = null) {
			if (!$target)
				$target = StringUtil::underscore(ClassUtil::getClassName(static::class));

			EntityMetadataRegistry::get(static::class)
				->addRelation($name, new ManyRelation($entity, $conds, $owned, null, $target));
		}

		/**
		 * @internal 
		 */
		public static function __load() {
			if (EntityMetadataRegistry::has(static::class))
				return;

			EntityMetadataRegistry::add(new EntityMetadata(static::class));

			static::__init();
		}

		public function __toString() {
			return EntityMetadataRegistry::get(static::class)->getEntityName() . '#' . $this->getId();
		}

		public function serialize() {
			$relations = [];

			if ($this->isNewRecord())
				foreach ($this->relations as $key => $relation)
					if ($relation instanceof Entity)
						$relations[$key] = $relation->getId();
					else if (is_array($relation)) {
						$relations[$key] = [];

						/** @var Entity $child */
						foreach ($relation as $child)
							$relations[$key][] = $child->getId();
					} else
						$relations[$key] = null;

			return serialize((object)[
				'state' => $this->state,
				'columns' => $this->columns->toArray(),
				'relations' => $relations,
			]);
		}

		public function unserialize($serialized) {
			static::__load();

			$input = unserialize($serialized);

			$this->state = $input->state;

			$this->columns = new AttributeCollection($this, $input->columns);
			$this->relations = new RelationCollection($this);

			if ($input->state === self::STATE_PERSISTED) {
				$this->columns->put('id', $input->columns['id']);

				$this->reload();

				return;
			}

			$md = EntityMetadataRegistry::get(static::class);

			foreach ($input->relations as $key => $relation) {
				$value = $relation;

				if (is_int($value))
					$value = call_user_func([$md->getRelation($key)->getEntityClass(), 'find'], $value);
				else if (is_array($value)) {
					$children = [];

					foreach ($value as $item)
						$children[] = call_user_func([$md->getRelation($key)->getEntityClass(), 'find'], $item);

					$value = $children;
				}

				$this->relations->put($key, $value);
			}

			$this->relations->sync();
		}

		public function __get($name) {
			if (!$this->__isset($name))
				throw new \Exception('No property exists named ' . $name);

			return $this->get($name);
		}

		public function __isset($name) {
			return $this->columns->has($name) || $this->relations->has($name);
		}
	}