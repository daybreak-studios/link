<?php
	namespace DaybreakStudios\Link\Entity\Cache;

	use DaybreakStudios\Link\Entity\EntityInterface;

	interface EntityCacheInterface {
		/**
		 * @param EntityInterface $entity
		 *
		 * @return mixed
		 */
		public function add(EntityInterface $entity);

		/**
		 * @param string $klass
		 * @param        $id
		 *
		 * @return mixed
		 */
		public function get($klass, $id);

		/**
		 * @param string $klass
		 * @param        $id
		 *
		 * @return bool
		 */
		public function has($klass, $id);

		/**
		 * @param EntityInterface $entity
		 *
		 * @return bool
		 */
		public function contains(EntityInterface $entity);

		/**
		 * @param EntityInterface $entity
		 *
		 * @return mixed
		 */
		public function evict(EntityInterface $entity);

		/**
		 * @param string $klass
		 * @param        $id
		 *
		 * @return mixed
		 */
		public function remove($klass, $id);
	}