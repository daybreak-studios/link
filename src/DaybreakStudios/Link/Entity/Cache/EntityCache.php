<?php
	namespace DaybreakStudios\Link\Entity\Cache;

	use DaybreakStudios\Link\Entity\Entity;
	use DaybreakStudios\Link\Entity\EntityInterface;

	class EntityCache implements EntityCacheInterface {
		/** @var Entity[] */
		protected $cache = [];

		/**
		 * @param EntityInterface $entity
		 *
		 * @return $this
		 */
		public function add(EntityInterface $entity) {
			$this->cache[$this->getKeyFor($entity)] = $entity;

			return $this;
		}

		/**
		 * @param string     $klass
		 * @param int|string $id
		 *
		 * @return bool
		 */
		public function has($klass, $id) {
			return isset($this->cache[$this->assembleKey($klass, $id)]);
		}

		/**
		 * @param EntityInterface $entity
		 *
		 * @return bool
		 */
		public function contains(EntityInterface $entity) {
			return $this->has(get_class($entity), $entity->getId());
		}

		/**
		 * @param string     $klass
		 * @param int|string $id
		 *
		 * @return null
		 */
		public function get($klass, $id) {
			if (!$this->has($klass, $id))
				return null;

			return $this->cache[$this->assembleKey($klass, $id)];
		}

		/**
		 * @param string    $klass
		 * @param int|string $id
		 *
		 * @return $this
		 */
		public function remove($klass, $id) {
			if (!$this->has($klass, $id))
				return $this;

			unset($this->cache[$this->assembleKey($klass, $id)]);

			return $this;
		}

		/**
		 * @param EntityInterface $entity
		 *
		 * @return $this
		 */
		public function evict(EntityInterface $entity) {
			return $this->remove(get_class($entity), $entity->getId());
		}

		/**
		 * @return $this
		 */
		public function clear() {
			$this->cache = [];

			return $this;
		}

		/**
		 * @param EntityInterface $entity
		 *
		 * @return string
		 */
		protected function getKeyFor(EntityInterface $entity) {
			return $this->assembleKey(get_class($entity), $entity->getId());
		}

		/**
		 * @param string     $klass
		 * @param int|string $id
		 *
		 * @return string
		 */
		protected function assembleKey($klass, $id) {
			return sprintf('%s#%d', $klass, $id);
		}
	}