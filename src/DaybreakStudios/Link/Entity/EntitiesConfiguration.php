<?php
	namespace DaybreakStudios\Link\Entity;

	use DaybreakStudios\Link\Connection\Connection;
	use DaybreakStudios\Link\Entity\Cache\EntityCache;
	use DaybreakStudios\Link\Entity\Cache\EntityCacheInterface;
	use DaybreakStudios\Link\Entity\Hydrator\ConstructorlessHydrator;
	use DaybreakStudios\Link\Entity\Hydrator\HydratorInterface;

	class EntitiesConfiguration {
		private static $instance = null;

		private $hydrator;
		private $connection;
		private $cache;

		private function __construct() {
			$this->hydrator = new ConstructorlessHydrator();
			$this->connection = Connection::getConnection();
			$this->cache = new EntityCache();
		}

		/**
		 * @return HydratorInterface
		 */
		public static function getHydrator() {
			return self::instance()->hydrator;
		}

		/**
		 * @param HydratorInterface $hydrator
		 *
		 * @return $this
		 */
		public function setHydrator(HydratorInterface $hydrator) {
			$this->hydrator = $hydrator;

			return $this;
		}

		/**
		 * @return Connection|null
		 */
		public static function getConnection() {
			return self::instance()->connection;
		}

		/**
		 * @param Connection $connection
		 *
		 * @return $this
		 */
		public function setConnection(Connection $connection) {
			$this->connection = $connection;

			return $this;
		}

		/**
		 * @return EntityCacheInterface
		 */
		public static function getCache() {
			return self::instance()->cache;
		}

		/**
		 * @param EntityCacheInterface $cache
		 *
		 * @return $this
		 */
		public function setCache(EntityCacheInterface $cache) {
			$this->cache = $cache;

			return $this;
		}

		/**
		 * @return EntitiesConfiguration
		 */
		public static function instance() {
			if (self::$instance === null)
				self::$instance = new EntitiesConfiguration();

			return self::$instance;
		}
	}