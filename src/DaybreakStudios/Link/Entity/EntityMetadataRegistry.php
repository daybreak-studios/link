<?php
	namespace DaybreakStudios\Link\Entity;

	class EntityMetadataRegistry {
		private static $metadatas = [];

		/**
		 * @param string $klass
		 *
		 * @return bool
		 */
		public static function has($klass) {
			return isset(self::$metadatas[$klass]);
		}

		public static function add(EntityMetadata $metadata) {
			if (self::has($metadata->getEntityClass()))
				throw new \InvalidArgumentException(sprintf('Metadata for %s collides with another entry. To ' .
					'override existing entries, %s::set.', $metadata->getEntityClass(), self::class));

			self::set($metadata);
		}

		/**
		 * @param string $klass
		 * @param bool   $autoload
		 *
		 * @return EntityMetadata|null
		 */
		public static function get($klass, $autoload = true) {
			if ($autoload && !isset(self::$metadatas[$klass]))
				call_user_func([$klass, '__load']);

			if (!self::has($klass))
				return null;

			return self::$metadatas[$klass];
		}

		public static function set(EntityMetadata $metadata) {
			self::$metadatas[$metadata->getEntityClass()] = $metadata;
		}
	}