<?php

	namespace DaybreakStudios\Link\Entity\Hydrator;

	use DaybreakStudios\Link\Entity\EntityInterface;

	interface HydratorInterface {
		/**
		 * @param string $klass
		 * @param array  $data
		 *
		 * @return EntityInterface
		 */
		public function hydrate($klass, array $data);
	}