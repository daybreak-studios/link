<?php
	namespace DaybreakStudios\Link\Entity\Hydrator;

	use DaybreakStudios\Link\Entity\Entity;
	use DaybreakStudios\Link\Utility\StringUtil;

	/**
	 * Class ReflectionHydrator
	 *
	 * @package DaybreakStudios\Link\Entity\Hydrator
	 *
	 * This hydrator is intended as a base class for any hydrator that utilizes reflection. Not only does it contain
	 * some utility methods for accessing and modifying private and protected fields, it also implements a basic
	 * `hydrate` method that creates a new instance of the class without calling it's constructor, then calls the
	 * `__load` static method for that class. This should lay the foundation for the rest of the object to be
	 * hydrated via reflection.
	 */
	class ReflectionHydrator extends AbstractHydrator {
		/**
		 * Creates a new instance of the class named by $klass, and invokes it's `__load` method.
		 *
		 * @param string $klass
		 * @param array  $data
		 *
		 * @return Entity
		 */
		public function hydrate($klass, array $data) {
			parent::typeCheck($klass);

			$refl = new \ReflectionClass($klass);
			$inst = $refl->newInstanceWithoutConstructor();

			$this->invokePrivateMethod($refl, '__load');
			$this->setPrivateProperty($refl, 'state', $inst, Entity::STATE_PERSISTED);

			return $inst;
		}

		/**
		 * @param \ReflectionClass $refl
		 * @param string           $property
		 * @param                  $object
		 * @param                  $value
		 *
		 * @return $this
		 */
		protected function setPrivateProperty(\ReflectionClass $refl, $property, $object, $value) {
			$property = $this->findProperty($refl, $property);

			$property->setAccessible(true);
			$property->setValue($object, $value);
			$property->setAccessible(false);

			return $this;
		}

		/**
		 * @param \ReflectionClass $refl
		 * @param string           $property
		 * @param                  $object
		 *
		 * @return mixed
		 */
		protected function getPrivateProperty(\ReflectionClass $refl, $property, $object) {
			$property = $this->findProperty($refl, $property);

			$property->setAccessible(true);

			$value = $property->getValue($object);

			$property->setAccessible(false);

			return $value;
		}

		/**
		 * @param \ReflectionClass $refl
		 * @param string           $method
		 * @param null             $object
		 * @param array            ...$args
		 *
		 * @return mixed
		 */
		protected function invokePrivateMethod(\ReflectionClass $refl, $method, $object = null, ... $args) {
			$method = $this->findMethod($refl, $method);

			$method->setAccessible(true);

			$ret = $method->invoke($object, ...$args);

			$method->setAccessible(false);

			return $ret;
		}

		/**
		 * @param \ReflectionClass $refl
		 * @param string           $name
		 *
		 * @return \ReflectionProperty
		 */
		protected function findProperty(\ReflectionClass $refl, $name) {
			return $this->findInReflectionClass($refl, 'Property', $name);
		}

		/**
		 * @param \ReflectionClass $refl
		 * @param string           $name
		 *
		 * @return \ReflectionMethod
		 */
		protected function findMethod(\ReflectionClass $refl, $name) {
			return $this->findInReflectionClass($refl, 'Method', $name);
		}

		/**
		 * @param \ReflectionClass $refl
		 * @param string           $type
		 * @param string           $name
		 *
		 * @return mixed
		 */
		private function findInReflectionClass(\ReflectionClass $refl, $type, $name) {
			$_ = StringUtil::classify($type);
			$has = 'has' . $_;
			$get = 'get' . $_;

			if (!method_exists($refl, $has) || !method_exists($refl, $get))
				throw new \InvalidArgumentException(sprintf('Cannot find by %s in ReflectionClass', $type));

			$ref = new \ReflectionClass($refl->getName());

			while (!call_user_func([$ref, $has], $name)) {
				if ($ref = $ref->getParentClass())
					continue;

				throw new \InvalidArgumentException(sprintf('Property %s does not exist in %s', $name,
					$refl->getName()));
			}

			return call_user_func([$ref, $get], $name);
		}
	}