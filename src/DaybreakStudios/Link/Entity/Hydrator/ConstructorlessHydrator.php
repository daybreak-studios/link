<?php
	namespace DaybreakStudios\Link\Entity\Hydrator;

	use DaybreakStudios\Link\Entity\Collection\AttributeCollection;
	use DaybreakStudios\Link\Entity\Collection\RelationCollection;
	use DaybreakStudios\Link\Entity\EntityMetadataRegistry;
	use DaybreakStudios\Link\Entity\Exception\PartialEntityException;
	use DaybreakStudios\Link\Utility\StringUtil;

	class ConstructorlessHydrator extends ReflectionHydrator {
		public function hydrate($klass, array $data) {
			$entity = parent::hydrate($klass, $data);
			$refl = new \ReflectionClass($entity);
			$md = EntityMetadataRegistry::get($klass);

			$columns = [];

			foreach ($md->getColumns() as $column) {
				if ($md->isRelation($column->getField()))
					continue;

				$name = $column->getField();
				$field = StringUtil::camelize($name);

				if (!array_key_exists($name, $data)) {
					$columns[$field] = new PartialEntityException($klass, $field);

					continue;
				}

				$columns[$field] = $column->getType()->castForEntity($data[$name]);
			}

			parent::setPrivateProperty($refl, 'columns', $entity, new AttributeCollection($entity, $columns));

			$relations = [];

			foreach ($md->getRelations() as $name => $relation)
				$relations[$name] = $relation;

			parent::setPrivateProperty($refl, 'relations', $entity, new RelationCollection($entity, $relations));

			return $entity;
		}
	}