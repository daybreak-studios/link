<?php
	namespace DaybreakStudios\Link\Entity\Hydrator;

	use DaybreakStudios\Link\Entity\EntityInterface;

	abstract class AbstractHydrator implements HydratorInterface {
		/**
		 * Convenience method for checking if a fully-qualified class name is a child of EntityInterface.
		 *
		 * @param string $klass
		 * @throws \InvalidArgumentException if $klass is not a child class of EntityInterface
		 */
		protected function typeCheck($klass) {
			if (!is_a($klass, EntityInterface::class, true))
				throw new \InvalidArgumentException(sprintf('%s is not a child of %s', $klass, EntityInterface::class));
		}
	}