<?php
	namespace DaybreakStudios\Link\Entity\Exception;

	class NoEntityFoundException extends \Exception {}