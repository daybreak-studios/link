<?php
	namespace DaybreakStudios\Link\Entity\Exception;

	use Exception;

	class EntityDestroyedException extends \Exception {
		public function __construct() {
			parent::__construct('Entity is destroyed');
		}
	}