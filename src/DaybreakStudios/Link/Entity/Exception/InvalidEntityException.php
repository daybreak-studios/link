<?php
	namespace DaybreakStudios\Link\Entity\Exception;

	use Exception;

	class InvalidEntityException extends \Exception {
		/**
		 * InvalidEntityException constructor.
		 *
		 * @param string $klass
		 */
		public function __construct($klass) {
			parent::__construct(sprintf('Table named by %s does not have an ID column', $klass));
		}
	}