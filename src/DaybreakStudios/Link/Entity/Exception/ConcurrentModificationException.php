<?php
	namespace DaybreakStudios\Link\Entity\Exception;

	class ConcurrentModificationException extends \Exception {
		/**
		 * @param string     $klass
		 * @param int|string $id
		 *
		 * @return static
		 */
		public static function deleted($klass, $id) {
			return new static(sprintf('Row represented by %s#%s was deleted by another process during runtime',
				$klass, $id));
		}

		/**
		 * @param string $klass
		 *
		 * @return static
		 */
		public static function altered($klass) {
			return new static(sprintf('The table represented by %s was altered during runtime', $klass));
		}
	}