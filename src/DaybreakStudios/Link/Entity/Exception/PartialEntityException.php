<?php
	namespace DaybreakStudios\Link\Entity\Exception;

	class PartialEntityException extends \Exception {
		/**
		 * PartialEntityException constructor.
		 *
		 * @param string $klass
		 * @param string $field
		 */
		public function __construct($klass, $field) {
			parent::__construct(sprintf('Cannot read %s; object is a partial entity of %s', $field, $klass));
		}
	}