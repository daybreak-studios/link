<?php
	namespace DaybreakStudios\Link\Entity\Exception;

	class MissingConnectionException extends \Exception {
		public function __construct() {
			parent::__construct('No connection found. Make sure you\'ve set your connection using ' .
				'EntitiesConfiguration::instance()->setConnection($conn)');
		}
	}