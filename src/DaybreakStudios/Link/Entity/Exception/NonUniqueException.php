<?php
	namespace DaybreakStudios\Link\Entity\Exception;

	class NonUniqueException extends \Exception {}