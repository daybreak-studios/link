<?php
	namespace DaybreakStudios\Link\Entity\Exception;

	use Exception;

	class EntityDetachedException extends \Exception {
		public function __construct() {
			parent::__construct('Entity is detached');
		}
	}