<?php
	namespace DaybreakStudios\Link\Entity;

	interface EntityInterface {
		/**
		 * @return int|null
		 */
		public function getId();

		public function reload();
		public function save();
		public function destroy();

		/**
		 * @return bool
		 */
		public function detach();

		/**
		 * @return bool
		 */
		public function isNewRecord();

		/**
		 * @return bool
		 */
		public function isPersisted();

		/**
		 * @return bool
		 */
		public function isDestroyed();
	}