# Entity Module
For a basic getting started guide to the Entity module, please read over the Entity Module section of Link's README,
located in the root of the repository.

## Override Magic Methods
Link assumes that your getters and setters for an entity follow generally accepted conventions. However, in some cases,
it may make sense to break those conventions, either because you need a custom implementation, or perhaps you just want
to do some additional validation on those fields. When that happens, all you need to do is define the method in your
entity class.

```php
<?php
    use DaybreakStudios\Link\Entity\Entity;
    
    /**
     * @method string getFirstName()
     * @method string getLastName()
     * @method $this  setLastName(string $lastName)
     * @method bool   isDeleted()   
     * @method $this  setDeleted(bool $deleted)
     */
    class User extends Entity {
        public function setFirstName(string $firstName) {
            return parent::set('first_name', trim($firstName) ?: null);
        }
    }
```

The base `Entity` class provides two methods by which you can get and set any field in your entity: `get` and `set`.

The `get` method takes two arguments: the name of the column to retrieve, and a second optional argument that defines
the default return value to use if the column is empty. If no default value is specified, `null` is returned.

The `set` method takes two arguments as well: the name of the column to set, and the new value of the column.

## Relationships
One of the best features of entities in other libraries is assigning relationships between different entities, allowing
you to retrieve related information without having to write additional queries, or request specific entities yourself.
You can do that in Link by implementing a static `__init` method, and using two different methods provided by the base
`Entity` class: `hasOne` and `hasMany`. Let's say you have two entities, `User` and `Post`, and a post already in your
database with an ID of 10.

```php
<?php
    use DaybreakStudios\Link\Entity\Entity;
    
    /**
     * @method string getFirstName()
     * @method $this  setLastName()
     * @method string getLastName()
     * @method $this  setLastName(string $lastName)
     * @method bool   isDeleted()   
     * @method $this  setDeleted(bool $deleted)
     * @method Post[] getPosts()
     */
    class User extends Entity {
        protected static function __init() {
            static::hasMany(Post::class);
        }
    }
    
    /**
     * @method User      getPostedBy()
     * @method $this     setPostedBy(User $user)
     * @method \DateTime getPostedOn()
     * @method $this     setPostedOn(\DateTime $dt)
     * @method string    getContent()
     * @method $this     setContent(string $content)
     */
    class Post extends Entity {
        protected static function __init() {
            static::hasOne(User::class, [], 'posted_by'); 
        }
    }
```

Both `hasOne` and `hasMany` accept several arguments. The first argument defines the entity class that represents the
related entity. The second argument (the empty array we passed to `hasOne`) will be described in the next section, and
is optional in both cases. The third argument, which is also optional in calls, is used to set the field name of the
relationship on the entity. If it is not set, it is assumed to be the singular form of the entity name in the case of
`hasOne`, and the plural form of the entity in the case of `hasMany`. In the example above, the `User` entity would now
have a field named `posts` since it called `hasMany`, and the `Post` entity would now have a field named `posted_by`
since it called `hasOne` and specified the name of the field in the call.

## Conditional Relationships
Relationships may also include conditions when relating to other entities. This allows you to do things like exclude
entities that might be considered unavailable (i.e. flagged as deleted in the database, but kept for data retention
purposes).

```php
<?php
    use DaybreakStudios\Link\Entity\Entity;
    
    /** [...] */
    class Post extends Entity {}
    
    /** [...] */
    class User extends Entity {
        protected static function __init() {
            static::hasMany(Post::class, [
                'deleted' => false,
            ]);
        }
    }
```

By including the conditions array, you let Link know to ONLY populate that field with entities that match your
conditions. You're safe to keep deleted posts in the database, but they won't be loaded when you call
`$user->getPosts()`.