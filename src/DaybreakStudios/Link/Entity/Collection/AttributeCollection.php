<?php
	namespace DaybreakStudios\Link\Entity\Collection;

	use DaybreakStudios\Link\Collection\ArrayCollection;
	use DaybreakStudios\Link\Entity\Entity;

	class AttributeCollection extends ArrayCollection {
		protected $entity;
		protected $original;

		/**
		 * AttributeCollection constructor.
		 *
		 * @param Entity $entity
		 * @param array  $data
		 */
		public function __construct(Entity $entity, array $data = []) {
			parent::__construct($data);

			$this->entity = $entity;
			$this->original = $data;
		}

		/**
		 * @return Entity
		 */
		public function getEntity() {
			return $this->entity;
		}

		/**
		 * @return array
		 */
		public function getChanges() {
			$changes = [];

			if ($this->entity->isNewRecord())
				$changes = $this->data;
			else
				foreach ($this->data as $key => $value) {
					if (!array_key_exists($key, $this->original) || $this->original[$key] === $value)
						continue;

					$changes[$key] = $value;
				}

			return $changes;
		}

		/**
		 * @param string|null $field
		 */
		public function sync($field = null) {
			if ($field !== null)
				$this->original[$field] = $this->data[$field];

			$this->original = $this->data;
		}
	}