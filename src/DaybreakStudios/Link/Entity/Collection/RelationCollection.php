<?php
	namespace DaybreakStudios\Link\Entity\Collection;

	use DaybreakStudios\Link\Entity\EntityInterface;
	use DaybreakStudios\Link\Entity\EntityMetadataRegistry;
	use DaybreakStudios\Link\Utility\StringUtil;

	class RelationCollection extends AttributeCollection {
		public function getChanges() {
			$md = EntityMetadataRegistry::get(get_class(parent::getEntity()));

			$changes = [];

			foreach (parent::getChanges() as $key => $value) {
				$column = StringUtil::underscore($key);
				$relation = $md->getRelation($column);

				// If the column isn't a relation, or the relation isn't defined by a column on the current entity, we
				// can skip it
				if ($relation === null || $md->getColumn($column) === null)
					continue;

				if ($value instanceof EntityInterface)
					$value = $value->getId();

				$changes[$key] = $value;
			}

			return $changes;
		}
	}