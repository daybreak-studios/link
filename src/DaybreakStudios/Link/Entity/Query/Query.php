<?php
	namespace DaybreakStudios\Link\Entity\Query;

	use DaybreakStudios\Link\AST\DeleteManager;
	use DaybreakStudios\Link\AST\Node\AndNode;
	use DaybreakStudios\Link\AST\Node\GroupingNode;
	use DaybreakStudios\Link\AST\Node\NodeInterface;
	use DaybreakStudios\Link\AST\Node\OrNode;
	use DaybreakStudios\Link\AST\SelectManager;
	use DaybreakStudios\Link\AST\Table;
	use DaybreakStudios\Link\AST\UpdateManager;
	use DaybreakStudios\Link\Connection\Connection;
	use DaybreakStudios\Link\Entity\EntitiesConfiguration;

	class Query {
		private $table;
		private $conn;

		private $page = 1;
		private $pageSize = null;

		/** @var UpdateManager|SelectManager|DeleteManager|null */
		private $manager = null;

		/** @var NodeInterface|null */
		private $lastExpr = null;

		public function __construct(EntityTable $table, Connection $conn = null) {
			$this->table = $table;
			$this->conn = $conn ?: EntitiesConfiguration::getConnection();
		}

		public function reset() {
			$this->page = 1;
			$this->pageSize = null;

			return $this;
		}

		/**
		 * @param mixed $size
		 *
		 * @return $this
		 */
		public function setPageSize($size) {
			$size = (int)$size;

			$this->reset();
			$this->pageSize = $size > 0 ? $size : null;

			return $this;
		}

		/**
		 * @return int|null
		 */
		public function getPageSize() {
			return $this->pageSize;
		}

		/**
		 * @param int|null $page
		 *
		 * @return $this
		 */
		public function setPage($page) {
			$this->page = (int)$page >= 1 ? (int)$page : 1;

			return $this;
		}

		/**
		 * @return int
		 */
		public function getPage() {
			return $this->page;
		}

		/**
		 * @return array
		 */
		public function fetch() {
			if ($this->getPageSize())
				$this->manager->limit($this->getPageSize());

			if ($this->manager instanceof SelectManager)
				$this->manager->offset(($this->getPage() - 1) * $this->getPageSize());

			$sql = $this->manager->getSql($this->conn);

			return $this->conn->fetch($sql);
		}

		/**
		 * @return array
		 */
		public function next() {
			if (!$this->getPageSize())
				return $this->fetch();

			$result = $this->fetch();

			$this->setPage($this->getPage() + 1);

			return $result;
		}

		/**
		 * @param array ...$items
		 *
		 * @return $this
		 */
		public function select(... $items) {
			$this->lastExpr = null;

			$this->manager = new SelectManager($this->table);
			$this->manager->select(... $items);

			return $this;
		}

		/**
		 * @return $this
		 */
		public function update() {
			$this->lastExpr = null;

			$this->manager = new UpdateManager($this->conn);
			$this->manager->table($this->table);

			return $this;
		}

		/**
		 * @return $this
		 */
		public function delete() {
			$this->lastExpr = null;

			$this->manager = new DeleteManager($this->conn);
			$this->manager->from($this->table);

			return $this;
		}

		/**
		 * @param NodeInterface $expr
		 *
		 * @return Query
		 */
		public function where(NodeInterface $expr) {
			return $this->andWhere($expr);
		}

		/**
		 * @param NodeInterface $expr
		 *
		 * @return $this
		 */
		public function andWhere(NodeInterface $expr) {
			$this->manager->where($this->lastExpr = $expr);

			return $this;
		}

		/**
		 * @param NodeInterface $expr
		 *
		 * @return $this|Query
		 */
		public function orWhere(NodeInterface $expr) {
			if ($this->lastExpr === null)
				return $this->where($expr);

			$wheres = $this->manager->getCore()->getWheres();
			$last = sizeof($wheres) - 1;

			$wheres[$last] = new OrNode($wheres[$last], $expr);

			$this->manager->getCore()->setWheres($wheres);

			return $this;
		}

		/**
		 * @param NodeInterface[] ...$exprs
		 *
		 * @return GroupingNode
		 */
		public function orX(NodeInterface... $exprs) {
			return $this->buildXNode(OrNode::class, ...$exprs);
		}

		/**
		 * @param NodeInterface[] ...$exprs
		 *
		 * @return GroupingNode
		 */
		public function andX(NodeInterface... $exprs) {
			return $this->buildXNode(AndNode::class, ...$exprs);
		}

		/**
		 * @param string          $class
		 * @param NodeInterface[] ...$exprs
		 *
		 * @return GroupingNode
		 */
		protected function buildXNode($class, NodeInterface... $exprs) {
			if (sizeof($exprs) < 2)
				throw new \InvalidArgumentException('You must provide at least two arguments');

			$or = new $class($exprs[0], $exprs[1]);

			for ($i = 2, $ii = sizeof($exprs); $i < $ii; $i++)
				$or = new $class($or, $exprs[$i]);

			return Table::group($or);
		}
	}