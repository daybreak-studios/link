<?php
	namespace DaybreakStudios\Link\Entity\Query;

	use DaybreakStudios\Link\AST\Table;
	use DaybreakStudios\Link\Entity\EntityMetadataRegistry;
	use DaybreakStudios\Link\Utility\StringUtil;

	class EntityTable extends Table {
		public function __construct($entity, $alias = null, $schema = null) {
			$md = EntityMetadataRegistry::get($entity);

			if ($md === null)
				throw new \InvalidArgumentException($entity . ' is not a valid entity');

			$table = $md->getTable();

			parent::__construct($table->getName(), $alias ?: $table->getAlias(), $schema ?: $table->getSchema());
		}

		public function offsetGet($offset) {
			return parent::offsetGet(StringUtil::underscore($offset));
		}
	}