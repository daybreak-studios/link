<?php
	require file_exists(__DIR__ . '/../vendor/autoload.php') ?
		__DIR__ . '/../vendor/autoload.php' :
		__DIR__ . '/../../../autoload.php';

	$helpers = null;

	foreach (['', 'config'] as $subpath) {
		$path = getcwd() . DIRECTORY_SEPARATOR . $subpath . DIRECTORY_SEPARATOR . 'cli-config.php';

		if (file_exists($path))
			$helpers = require $path;
	}

	if ($helpers === null) {
		echo \DaybreakStudios\Link\Console\Messages::MISSING_CONFIG;

		exit(1);
	} else if (!($helpers instanceof \Symfony\Component\Console\Helper\HelperSet)) {
		printf(\DaybreakStudios\Link\Console\Messages::INVALID_CONFIG_RETURN,
			\Symfony\Component\Console\Helper\HelperSet::class,
			is_object($helpers) ? get_class($helpers) : gettype($helpers));

		exit(1);
	}

	$app = new \DaybreakStudios\Link\Console\Application('Link', '1.0.0');

	$app->setHelperSet($helpers);
	$app->add(new \DaybreakStudios\Link\Console\Command\GenerateEntityCommand());

	$app->run();