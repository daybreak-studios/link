<?php
	require_once __DIR__ . '/vendor/autoload.php';

	$adapter = new \DaybreakStudios\Link\Connection\Adapter\MySqlPdoAdapter('link_demo', '127.0.0.1', 'root', '');
	$connection = \DaybreakStudios\Link\Connection\Connection::create($adapter);

	return new \Symfony\Component\Console\Helper\HelperSet([
		'connection' => new \DaybreakStudios\Link\Console\Helper\ConnectionHelper($connection),
		'destination' => new \DaybreakStudios\Link\Console\Helper\DestinationHelper(__DIR__ . '/entities'),
	]);